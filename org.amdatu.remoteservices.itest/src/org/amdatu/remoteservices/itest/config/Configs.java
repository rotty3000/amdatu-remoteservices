package org.amdatu.remoteservices.itest.config;

public class Configs {

    public static Config[] configs(Config... configs) {
        return configs;
    }

    public static BundlesConfig bundlesConfig(String... bundlePaths) {
        return new BundlesConfig(bundlePaths);
    }

    public static FrameworkConfig frameworkConfig(String name) {
        return new FrameworkConfig(name);
    }
}
