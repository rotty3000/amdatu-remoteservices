/*
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.itest.junit.rsa;

import static org.amdatu.remoteservices.itest.config.Configs.configs;
import static org.amdatu.remoteservices.itest.config.Configs.frameworkConfig;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.SERVICE_EXPORTED_INTERFACES;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.SERVICE_IMPORTED;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.List;
import java.util.Map;

import org.amdatu.remoteservices.itest.config.Config;
import org.amdatu.remoteservices.itest.config.FrameworkConfig;
import org.amdatu.remoteservices.itest.junit.RemoteServicesTestBase;
import org.amdatu.remoteservices.itest.util.FrameworkContext;
import org.amdatu.remoteservices.remoteserviceadmin.itest.api.EchoInterface;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.http.HttpService;
import org.osgi.service.log.LogService;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.ExportRegistration;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdmin;

/**
 * Abstract test base for testing RSA functionality.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public abstract class AbstractRemoteServiceAdminTest extends RemoteServicesTestBase {

    // FIXME copy here because the original is not visible
    public static final String ARS_CONFIG_TYPE = ".ars";

    protected volatile ServiceReference<RemoteServiceAdmin> m_remoteServiceAdminReference;
    protected volatile RemoteServiceAdmin m_remoteServiceAdmin;

    protected static boolean stringListEquals(List<String> left, List<String> right) {
        if (left.size() != right.size()) {
            return false;
        }
        if (left.size() == 0) {
            return true;
        }
        List<String> copyOfLeft = new ArrayList<String>(left);
        List<String> copyOfRight = new ArrayList<String>(right);
        Collections.sort(copyOfLeft);
        Collections.sort(copyOfRight);
        for (int i = 0; i < left.size(); i++) {
            if (!copyOfLeft.get(i).equals(copyOfRight.get(i))) {
                return false;
            }
        }
        return true;
    }

    protected static List<String> strings(String... values) {
        List<String> list = new ArrayList<String>();
        for (String value : values) {
            list.add(value);
        }
        return list;
    }

    protected void assertExportRegistrationFails(Dictionary<String, Object> serviceProperties,
        Map<String, Object> extraProperies, Class<?> exceptionClass, Object serviceInstance) throws Exception {
        assertExportRegistrationFails(serviceProperties, extraProperies, exceptionClass, serviceInstance,
            EchoInterface.class);
    }

    protected void assertExportRegistrationFails(Dictionary<String, Object> serviceProperties,
        Map<String, Object> extraProperies, Class<?> exceptionClass, Object serviceInstance, Class<?>... interfaces)
        throws Exception {

        String[] ifaces = new String[interfaces.length];
        for (int i = 0; i < ifaces.length; i++) {
            ifaces[i] = interfaces[i].getName();
        }

        ServiceRegistration<?> serviceRegistration =
            getChildBundleContext().registerService(ifaces, serviceInstance, serviceProperties);

        Collection<ExportRegistration> exportRegistrations = null;
        Exception exception = null;
        try {
            exportRegistrations =
                m_remoteServiceAdmin.exportService(serviceRegistration.getReference(), extraProperies);
        }
        catch (Exception e) {
            exception = e;
        }
        finally {
            serviceRegistration.unregister();
        }

        if (exception != null) {
            if (exceptionClass == null || exception.getClass() != exceptionClass) {
                fail("Export resulted in unexpected exception of type " + exceptionClass.getName());
            }
        }
        else {
            if (exceptionClass != null) {
                fail("Export did not throw expected exception of type " + exceptionClass.getName());
            }
            assertTrue("Expected no export registration", exportRegistrations.isEmpty());
        }
    }

    protected void assertExportRegistrationSucceeds(Dictionary<String, Object> serviceProperties,
        Map<String, Object> extraProperies, List<String> expectedConfigTypes, List<String> expectedIntents,
        Object serviceInstance) throws Exception {
        assertExportRegistrationSucceeds(serviceProperties, extraProperies, expectedConfigTypes, expectedIntents,
            serviceInstance, EchoInterface.class);
    }

    protected void assertExportRegistrationSucceeds(Dictionary<String, Object> serviceProperties,
        Map<String, Object> extraProperies, List<String> expectedConfigTypes, List<String> expectedIntents,
        Object serviceInstance, Class<?>... interfaces) throws Exception {

        String[] ifaces = new String[interfaces.length];
        for (int i = 0; i < ifaces.length; i++) {
            ifaces[i] = interfaces[i].getName();
        }

        ServiceRegistration<?> serviceRegistration =
            getChildBundleContext().registerService(ifaces, serviceInstance, serviceProperties);

        Collection<ExportRegistration> exportRegistrations =
            m_remoteServiceAdmin.exportService(serviceRegistration.getReference(), extraProperies);

        try {
            assertTrue("Expected one or more export registration", exportRegistrations.size() > 0);

            for (ExportRegistration exportRegistration : exportRegistrations) {
                EndpointDescription endpointDescription = exportRegistration.getExportReference().getExportedEndpoint();

                if (expectedConfigTypes != null) {
                    assertTrue(stringListEquals(expectedConfigTypes, endpointDescription.getConfigurationTypes()));
                }
                if (expectedIntents != null) {
                    assertTrue(stringListEquals(expectedIntents, endpointDescription.getIntents()));
                }
            }

            for (Class<?> iface : interfaces) {
                assertEquals(1, countExportedServices(iface));
            }
        }
        finally {
            for (ExportRegistration exportRegistration : exportRegistrations) {
                exportRegistration.close();
            }
            serviceRegistration.unregister();
        }
    }

    @Override
    protected void cleanupTest() throws Exception {
        m_remoteServiceAdmin = null;
        if (m_remoteServiceAdminReference != null) {
            getChildBundleContext().ungetService(m_remoteServiceAdminReference);
            m_remoteServiceAdminReference = null;
        }
    }

    @Override
    protected Config[] configureFramework(FrameworkContext parent) throws Exception {

        // FIXME The RSA requires this property to be set. Should it not get it from the HTTP service or at least use a default?
        System.setProperty("host", "localhost");

        BundleContext parentBC = getParentContext().getBundleContext();
        String systemPackages = parentBC.getProperty("itest.systempackages");
        String defaultBundles = parentBC.getProperty("itest.bundles.default");
        String topologyManagerBundles = parentBC.getProperty("itest.bundles.remoteserviceadmin");

        parent.setLogLevel(LogService.LOG_DEBUG);
        parent.setServiceTimout(30000);

        FrameworkConfig child1 = frameworkConfig("CHILD1")
            .logLevel(LogService.LOG_DEBUG)
            .serviceTimeout(10000)
            .frameworkProperty(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, systemPackages)
            .frameworkProperty("org.osgi.service.http.port", "8089")
            .bundlePaths(defaultBundles, topologyManagerBundles);

        return configs(child1);
    }

    @Override
    protected void configureServices() throws Exception {
        m_remoteServiceAdminReference =
            getChildBundleContext().getServiceReference(RemoteServiceAdmin.class);
        m_remoteServiceAdmin = getChildBundleContext().getService(m_remoteServiceAdminReference);

        // Await HttpService because Felix HTTP Jetty initializes async causing race issues as RSA uses whiteboard
        getChildContext("CHILD1").getService(HttpService.class);
    }

    protected int countImportedServices(Class<?> type) throws Exception {
        ServiceReference<?>[] serviceReferences =
            getChildBundleContext().getServiceReferences(type.getName(), "(" + SERVICE_IMPORTED + "=*)");
        if (serviceReferences == null) {
            return 0;
        }
        return serviceReferences.length;
    }

    protected int countExportedServices(Class<?> type) throws Exception {
        ServiceReference<?>[] serviceReferences =
            getChildBundleContext().getServiceReferences(type.getName(), "(" + SERVICE_EXPORTED_INTERFACES + "=*)");
        if (serviceReferences == null) {
            return 0;
        }
        return serviceReferences.length;
    }

    protected Object createInstance(String typeName) throws Exception {
        Bundle b = getChildBundleContext().getBundle();
        Class<?> type = b.loadClass(typeName);
        return type.newInstance();
    }

    protected final BundleContext getChildBundleContext() {
        return getChildContext("CHILD1").getBundleContext();
    }

    protected final void logDebug(String message) {
        getChildContext("CHILD1").getLogService().log(LogService.LOG_DEBUG, message);
    }
}
