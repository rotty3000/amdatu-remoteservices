package org.amdatu.remoteservices.itest.junit.full;

import static org.amdatu.remoteservices.itest.config.Configs.configs;
import static org.amdatu.remoteservices.itest.config.Configs.frameworkConfig;

import org.amdatu.remoteservices.itest.config.Config;
import org.amdatu.remoteservices.itest.config.FrameworkConfig;
import org.amdatu.remoteservices.itest.util.FrameworkContext;
import org.osgi.framework.Constants;
import org.osgi.service.http.HttpService;
import org.osgi.service.log.LogService;

public class BonjourFullIntegrationTest extends AbstractFullIntegrationTest {

    @Override
    protected Config[] configureFramework(FrameworkContext parent) throws Exception {

        String systemPackages = getParentContext().getBundleContext().getProperty("itest.systempackages");
        String host = getParentContext().getBundleContext().getProperty("itest.host");

        String defaultBundles = getParentContext().getBundleContext().getProperty("itest.bundles.default");
        String remoteServiceAdminBundles =
            getParentContext().getBundleContext().getProperty("itest.bundles.remoteserviceadmin");
        String topologyManagerBundles =
            getParentContext().getBundleContext().getProperty("itest.bundles.topologymanager");
        String discoveryBundles = getParentContext().getBundleContext().getProperty("itest.bundles.bonjourdiscovery");

        parent.setLogLevel(LogService.LOG_WARNING);
        parent.setServiceTimout(30000);

        FrameworkConfig child1 = frameworkConfig("CHILD1")
            .logLevel(LogService.LOG_DEBUG)
            .serviceTimeout(30000)
            .frameworkProperty("host", host)
            .frameworkProperty("felix.cm.loglevel", "1")
            .frameworkProperty("org.osgi.service.http.port", "8081")
            .frameworkProperty(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, systemPackages)
            .bundlePaths(defaultBundles, remoteServiceAdminBundles, topologyManagerBundles, discoveryBundles);

        FrameworkConfig child2 = frameworkConfig("CHILD2")
            .logLevel(LogService.LOG_DEBUG)
            .serviceTimeout(30000)
            .frameworkProperty("host", host)
            .frameworkProperty("felix.cm.loglevel", "1")
            .frameworkProperty("org.osgi.service.http.port", "8082")
            .frameworkProperty(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, systemPackages)
            .bundlePaths(defaultBundles, remoteServiceAdminBundles, topologyManagerBundles, discoveryBundles);

        return configs(child1, child2);
    }

    @Override
    protected void configureServices() throws Exception {
        // Await HttpService because Felix HTTP Jetty initializes async causing race issues as RSA uses whiteboard
        getChildContext("CHILD1").getService(HttpService.class);
        getChildContext("CHILD2").getService(HttpService.class);
    }

    @Override
    protected void cleanupTest() throws Exception {
    }

}
