package org.amdatu.remoteservices.itest.junit.full;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.remoteservices.itest.junit.RemoteServicesTestBase;
import org.amdatu.remoteservices.remoteserviceadmin.itest.api.EchoInterface;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.log.LogService;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

/**
 * Abstract test for full integration with an RemoteServiceAdmin, a TopologyManager and a Discovery
 * implementation. Concrete implementation must provision from frameworks.
 * 
 */
public abstract class AbstractFullIntegrationTest extends RemoteServicesTestBase {

    public void testSimpleServiceRegistrationBeingImported() throws Exception {

        getParentContext().getLogService().log(LogService.LOG_INFO, "Registering local service");
        EchoInterface localEchoService = mock(EchoInterface.class);
        when(localEchoService.echo("Amdatu")).thenReturn("Hello Amdatu");

        Dictionary<String, Object> localProperties = new Hashtable<String, Object>();
        localProperties.put(RemoteConstants.SERVICE_EXPORTED_INTERFACES, EchoInterface.class.getName());

        ServiceRegistration<?> localServiceRegistration =
            getChildContext("CHILD1").getBundleContext().registerService(EchoInterface.class.getName(),
                localEchoService, localProperties);
        ServiceReference<?> localServiceReference = localServiceRegistration.getReference();
        assertNotNull(localServiceReference);

        getParentContext().getLogService().log(LogService.LOG_INFO, "Invoking imported remote service");

        EchoInterface client = getChildContext("CHILD2").getService(EchoInterface.class);
        assertNotNull("no proxy for service A found!", client);

        assertEquals("Hello Amdatu", client.echo("Amdatu"));
    }
}
