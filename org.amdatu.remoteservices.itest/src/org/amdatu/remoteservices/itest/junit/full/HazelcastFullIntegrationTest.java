package org.amdatu.remoteservices.itest.junit.full;

import static org.amdatu.remoteservices.itest.config.Configs.configs;
import static org.amdatu.remoteservices.itest.config.Configs.frameworkConfig;

import org.amdatu.remoteservices.itest.config.Config;
import org.amdatu.remoteservices.itest.config.FrameworkConfig;
import org.amdatu.remoteservices.itest.util.FrameworkContext;
import org.osgi.framework.Constants;
import org.osgi.service.http.HttpService;
import org.osgi.service.log.LogService;

public class HazelcastFullIntegrationTest extends AbstractFullIntegrationTest {

    @Override
    protected Config[] configureFramework(FrameworkContext parent) throws Exception {

        parent.setLogLevel(LogService.LOG_DEBUG);
        parent.setServiceTimout(30000);

        String systemPackages = getParentContext().getBundleContext().getProperty("itest.systempackages");
        String host = getParentContext().getBundleContext().getProperty("itest.host");

        String defaultBundles = getParentContext().getBundleContext().getProperty("itest.bundles.default");
        String remoteServiceAdminBundles =
            getParentContext().getBundleContext().getProperty("itest.bundles.remoteserviceadmin");
        String topologyManagerBundles =
            getParentContext().getBundleContext().getProperty("itest.bundles.topologymanager");
        String discoveryBundles = getParentContext().getBundleContext().getProperty("itest.bundles.hazelcastdiscovery");

        FrameworkConfig child1 = frameworkConfig("CHILD1")
            .logLevel(LogService.LOG_DEBUG)
            .serviceTimeout(30000)
            .frameworkProperty("host", host)
            .frameworkProperty("felix.cm.loglevel", "1")
            .frameworkProperty("org.osgi.service.http.port", "8081")
            .frameworkProperty(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, systemPackages)
            .bundlePaths(defaultBundles, remoteServiceAdminBundles, topologyManagerBundles, discoveryBundles);

        FrameworkConfig child2 = frameworkConfig("CHILD2")
            .logLevel(LogService.LOG_DEBUG)
            .serviceTimeout(30000)
            .frameworkProperty("felix.cm.loglevel", "1")
            .frameworkProperty("org.osgi.service.http.port", "8082")
            .frameworkProperty(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, systemPackages)
            .bundlePaths(defaultBundles, remoteServiceAdminBundles, topologyManagerBundles, discoveryBundles);

        return configs(child1, child2);
    }

    @Override
    protected void configureServices() throws Exception {
        // Await HttpService because Felix HTTP Jetty initializes async causing race issues as RSA uses whiteboard
        getChildContext("CHILD1").getService(HttpService.class);
        getChildContext("CHILD2").getService(HttpService.class);
    }

    @Override
    protected void cleanupTest() throws Exception {
    }

// @Override
// public void testSimpleServiceRegistrationBeingImported() throws Exception {
// getChildContext("CHILD1").getLogService().log(LogService.LOG_WARNING,
// "Hazelcast test disabled because it triggers classloading deadlocks");
// assertTrue(true);
// }
}
