package org.amdatu.remoteservices.itest.junit.topo;

import static org.amdatu.remoteservices.itest.config.Configs.configs;
import static org.amdatu.remoteservices.itest.config.Configs.frameworkConfig;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.SERVICE_EXPORTED_INTERFACES;

import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;

import org.amdatu.remoteservices.itest.config.Config;
import org.amdatu.remoteservices.itest.config.FrameworkConfig;
import org.amdatu.remoteservices.itest.junit.RemoteServicesTestBase;
import org.amdatu.remoteservices.itest.util.FrameworkContext;
import org.amdatu.remoteservices.remoteserviceadmin.itest.api.EchoInterface;
import org.amdatu.remoteservices.topologymanager.impl.TopologyManagerImpl;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.log.LogService;
import org.osgi.service.remoteserviceadmin.ExportRegistration;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdmin;

/**
 * Testing {@link TopologyManagerImpl} implementation in isolation.
 * 
 */
public class TopologyManagerTest extends RemoteServicesTestBase {

    @Override
    protected Config[] configureFramework(FrameworkContext parent) throws Exception {

        String systemPackages =
            getParentContext().getBundleContext().getProperty("itest.systempackages");
        String defaultBundles =
            getParentContext().getBundleContext().getProperty("itest.bundles.default");
        String topologyManagerBundles =
            getParentContext().getBundleContext().getProperty("itest.bundles.topologymanager");

        parent.setLogLevel(LogService.LOG_DEBUG);
        parent.setServiceTimout(30000);

        FrameworkConfig child1 = frameworkConfig("CHILD1")
            .logLevel(LogService.LOG_DEBUG)
            .serviceTimeout(10000)
            .frameworkProperty(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, systemPackages)
            .bundlePaths(defaultBundles, topologyManagerBundles);

        return configs(child1);
    }

    @Override
    protected void configureServices() throws Exception {

    }

    @Override
    protected void cleanupTest() throws Exception {
    }

    /**
     * Test that combines several tests that can safely run sequentially in one framework.
     * This is just more efficient then starting a fresh framework for every test.
     * 
     * @throws Exception
     */
    public void testTopologyMananger() throws Exception {
        assertTopologyManagerRaceOK();
        assertTopologyManagerCallsNewRSA();
    }

    /**
     * AMDATURS-37 - Check that an Export Registration is properly closed when the Service Registration is
     * unregistered immediately after its export.
     */
    @SuppressWarnings("unchecked")
    public void assertTopologyManagerRaceOK() throws Exception {

        RemoteServiceAdmin remoteServiceAdmin = mock(RemoteServiceAdmin.class);
        ExportRegistration exportRegistration = mock(ExportRegistration.class);
        Collection<ExportRegistration> registrations = Collections.singletonList(exportRegistration);
        when(remoteServiceAdmin.exportService(notNull(ServiceReference.class), any(Map.class))).thenReturn(
            registrations);

        ServiceRegistration<?> rsaRegistration =
            getChildContext("CHILD1").getBundleContext().registerService(RemoteServiceAdmin.class.getName(),
                remoteServiceAdmin, null);

        Dictionary<String, Object> localProperties = new Hashtable<String, Object>();
        localProperties.put(SERVICE_EXPORTED_INTERFACES, EchoInterface.class.getName());
        ServiceRegistration<?> svcRegistration =
            getChildContext("CHILD1").getBundleContext().registerService(EchoInterface.class.getName(),
                mock(EchoInterface.class), localProperties);
        ServiceReference<?> svcReference = svcRegistration.getReference();

        // close immediately
        svcRegistration.unregister();

        try {
            verify(remoteServiceAdmin).exportService(svcReference, null);
            verify(exportRegistration).close();
        }
        finally {
            rsaRegistration.unregister();
        }
    }

    /**
     * Tests that a topology manager calls a newly registered RSA to export services.
     */
    public void assertTopologyManagerCallsNewRSA() throws Exception {
        Dictionary<String, Object> localProperties = new Hashtable<String, Object>();
        localProperties.put(SERVICE_EXPORTED_INTERFACES, EchoInterface.class.getName());
        
        EchoInterface localEchoService = mock(EchoInterface.class);
        
        ServiceRegistration<?> svcRegistration =
            getChildContext("CHILD1").getBundleContext().registerService(EchoInterface.class.getName(),
                localEchoService,
                localProperties);
        ServiceReference<?> localServiceReference = svcRegistration.getReference();

        RemoteServiceAdmin remoteServiceAdmin = mock(RemoteServiceAdmin.class);
        ServiceRegistration<?> rsaRegistration =
            getChildContext("CHILD1").getBundleContext().registerService(RemoteServiceAdmin.class.getName(),
                remoteServiceAdmin, null);

        try {
            verify(remoteServiceAdmin).exportService(localServiceReference, null);
        }
        finally {
            svcRegistration.unregister();
            rsaRegistration.unregister();
        }
    }

}
