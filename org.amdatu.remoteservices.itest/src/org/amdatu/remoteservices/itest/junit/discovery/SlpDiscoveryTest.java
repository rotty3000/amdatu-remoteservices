/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.itest.junit.discovery;

import static org.amdatu.remoteservices.discovery.slp.SlpUtil.createServiceAgent;
import static org.amdatu.remoteservices.discovery.slp.SlpUtil.createServiceInfo;
import static org.amdatu.remoteservices.discovery.slp.SlpUtil.createUserAgentClient;
import static org.amdatu.remoteservices.discovery.slp.SlpUtil.getInet4Addresses;
import static org.amdatu.remoteservices.itest.config.Configs.configs;
import static org.amdatu.remoteservices.itest.config.Configs.frameworkConfig;
import static org.amdatu.remoteservices.itest.util.ITestUtil.createEndpointDescription;

import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.amdatu.remoteservices.itest.config.Config;
import org.amdatu.remoteservices.itest.config.FrameworkConfig;
import org.amdatu.remoteservices.itest.util.BlockingEndpointListener;
import org.amdatu.remoteservices.itest.util.FrameworkContext;
import org.livetribe.slp.Scopes;
import org.livetribe.slp.ServiceInfo;
import org.livetribe.slp.ServiceType;
import org.livetribe.slp.sa.ServiceAgent;
import org.livetribe.slp.settings.Keys;
import org.livetribe.slp.ua.UserAgentClient;
import org.osgi.framework.Constants;
import org.osgi.service.log.LogService;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.EndpointEvent;
import org.osgi.service.remoteserviceadmin.EndpointEventListener;

/**
 * Tests SLP/Livetribe discovery.
 */
public class SlpDiscoveryTest extends AbstractDiscoveryTest<ServiceInfo> {

    private ServiceAgent m_serviceAgent;

    @Override
    protected Config[] configureFramework(FrameworkContext parent) throws Exception {

        String systemPackages = getParentContext().getBundleContext().getProperty("itest.systempackages");
        String host = getParentContext().getBundleContext().getProperty("itest.host");

        String defaultBundles = getParentContext().getBundleContext().getProperty("itest.bundles.default");
        String discoveryBundles = getParentContext().getBundleContext().getProperty("itest.bundles.slpdiscovery");

        parent.setLogLevel(LogService.LOG_DEBUG);
        parent.setServiceTimout(30000);

        FrameworkConfig child1 = frameworkConfig("CHILD1")
            .logLevel(LogService.LOG_DEBUG)
            .serviceTimeout(10000)
            .frameworkProperty("host", host)
            .frameworkProperty(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, systemPackages)
            .bundlePaths(defaultBundles, discoveryBundles);

        return configs(child1);
    }

    @Override
    protected void configureServices() throws Exception {
        getChildContext("CHILD1").configure("org.amdatu.remoteservices.discovery.slp", "ars.slp.poll.interval", "1",
            Keys.PORT_KEY.getKey(), "8091", Keys.UA_UNICAST_PREFER_TCP.getKey(), "true");
        Dictionary<String, Object> configuration = new Hashtable<String, Object>();
        configuration.put(Keys.PORT_KEY.getKey(), 8091);
        m_serviceAgent = createServiceAgent(this.getClass().getClassLoader(), configuration);
        m_serviceAgent.start();

    }

    @Override
    protected void cleanupTest() throws Exception {
        if (m_serviceAgent != null && m_serviceAgent.isRunning()) {
            m_serviceAgent.stop();
        }
        m_serviceAgent = null;
    }

    @Override
    protected ServiceInfo publishEndpoint(EndpointDescription endpointDescription) throws Exception {
        ServiceInfo serviceInfo = createServiceInfo(endpointDescription, "1270.0.0.1", 8080);
        getParentContext().getLogService().log(LogService.LOG_DEBUG,
            "Registering SLP service : " + serviceInfo.getServiceURL());
        registerService(serviceInfo);
        return serviceInfo;
    }

    @Override
    protected void revokeEndpoint(ServiceInfo endpointRegistration) throws Exception {
        deregisterService(endpointRegistration);

    }

    private void registerService(ServiceInfo info) {
        if (m_serviceAgent.isRunning()) {
            m_serviceAgent.register(info);
        }
    }

    private void deregisterService(ServiceInfo info) {
        if (m_serviceAgent.isRunning()) {
            m_serviceAgent.deregister(info.getServiceURL(), info.getLanguage());
        }
    }

    /*
     * Slp specific tests
     */

    /**
     * Tests that a service registration and de-registration is reflected in an SLP registration.
     * 
     * @throws Exception on failure
     */
    public void testEndpointListener() throws Exception {

        EndpointDescription endpoint = createEndpointDescription("q123", "interface", 42l);

        Dictionary<String, Object> configuration = new Hashtable<String, Object>();
        configuration.put(Keys.PORT_KEY.getKey(), 8091);
        UserAgentClient uac = createUserAgentClient(this.getClass().getClassLoader(), configuration);

        getParentContext().getLogService().log(LogService.LOG_DEBUG,
            "Invoking discovery endpointAdded with decription" + endpoint);

        EndpointEventListener listener =
            getChildContext("CHILD1").getService(EndpointEventListener.class, "(discovery.type=slp)");
        listener.endpointChanged(new EndpointEvent(EndpointEvent.ADDED, endpoint), "(" + Constants.OBJECTCLASS
            + "=interface)");

        List<ServiceInfo> services =
            uac.findServices(new ServiceType("service:osgi.remote:http"), null, Scopes.NONE, null);
        List<Inet4Address> inet4Addresses = getInet4Addresses();
        if (services.size() != inet4Addresses.size()) {
            fail("Not the correct number of expected services: expected: " + inet4Addresses.size() + " actual: "
                + services.size());
        }

        for (ServiceInfo serviceInfo : new ArrayList<ServiceInfo>(services)) {
            for (Inet4Address inet4Address : inet4Addresses) {
                ServiceInfo createServiceInfo =
                    createServiceInfo(endpoint, inet4Address.getHostAddress(), 8080);
                if (serviceInfo.getServiceURL().equals(createServiceInfo.getServiceURL())) {
                    services.remove(serviceInfo);
                }
            }
        }
        if (!services.isEmpty()) {
            fail("Unexpected services discovered");
        }

        getParentContext().getLogService().log(LogService.LOG_DEBUG,
            "Invoking discovery endpointRemoved with decription" + endpoint);
        listener.endpointChanged(new EndpointEvent(EndpointEvent.REMOVED, endpoint), "(" + Constants.OBJECTCLASS
            + "=interface)");
        services = uac.findServices(new ServiceType("service:osgi.remote:http"), null, Scopes.NONE, null);
        if (services.size() != 0) {
            fail("Unexpected services discovered");
        }
    }

    /**
     * Tests the SLPDiscoveryService with a configuration update. This also implicitly tests the start behavior.
     * 
     * @throws Exception
     */
    public void testUpdateConfiguration() throws Exception {

        EndpointDescription endpointDescription = createEndpointDescription("q123", "interface2", 42l);
        ServiceInfo info = createServiceInfo(endpointDescription, "localhost", 8080);

        BlockingEndpointListener block =
            new BlockingEndpointListener(getChildContext("CHILD1").getBundleContext(), endpointDescription);
        block.register();

        try {
            registerService(info);
            if (!block.awaitAdded(10, TimeUnit.SECONDS)) {
                fail("Endpoint not added");
            }

            // Update the configuration with changed port. After the update the old service should be removed, since
            // it has been registered on a different port
            block.reset();
            getChildContext("CHILD1").configure("org.amdatu.remoteservices.discovery.slp", "ars.slp.poll.interval",
                "1",
                Keys.PORT_KEY.getKey(), "4242", Keys.UA_UNICAST_PREFER_TCP.getKey(), "true");
            if (!block.awaitRemoved(10, TimeUnit.SECONDS)) {
                fail("Endpoint not removed");
            }

            // Update the configuration back to the old port and Verify the service is found again
            block.reset();
            getChildContext("CHILD1").configure("org.amdatu.remoteservices.discovery.slp", "ars.slp.poll.interval",
                "1",
                Keys.PORT_KEY.getKey(), "8091", Keys.UA_UNICAST_PREFER_TCP.getKey(), "true");
            if (!block.awaitAdded(10, TimeUnit.SECONDS)) {
                fail("Endpoint not added");
            }

            // Make sure our registered service is removed
            block.reset();
            deregisterService(info);
            if (!block.awaitRemoved(10, TimeUnit.SECONDS)) {
                fail("Endpoint not removed");
            }
        }
        finally {
            block.unregister();
        }
    }
}
