/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.itest.junit.discovery;

import static org.amdatu.remoteservices.discovery.bonjour.BonjourUtil.createServiceInfo;
import static org.amdatu.remoteservices.itest.config.Configs.configs;
import static org.amdatu.remoteservices.itest.config.Configs.frameworkConfig;
import static org.amdatu.remoteservices.itest.util.ITestUtil.createEndpointDescription;

import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;

import org.amdatu.remoteservices.itest.config.Config;
import org.amdatu.remoteservices.itest.config.FrameworkConfig;
import org.amdatu.remoteservices.itest.util.BlockingEndpointListener;
import org.amdatu.remoteservices.itest.util.FrameworkContext;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.log.LogService;
import org.osgi.service.remoteserviceadmin.EndpointDescription;

/**
 * Tests mDNS discovery.
 */
public class BonjourDiscoveryITest extends AbstractDiscoveryTest<ServiceInfo> implements ServiceListener {

    private ServiceInfo m_si;
    private JmDNS m_jmDNS;

    @Override
    protected Config[] configureFramework(FrameworkContext parent) throws Exception {

        String systemPackages = getParentContext().getBundleContext().getProperty("itest.systempackages");
        String host = getParentContext().getBundleContext().getProperty("itest.host");

        String defaultBundles = getParentContext().getBundleContext().getProperty("itest.bundles.default");
        String discoveryBundles = getParentContext().getBundleContext().getProperty("itest.bundles.bonjourdiscovery");

        parent.setLogLevel(LogService.LOG_DEBUG);
        parent.setServiceTimout(30000);

        FrameworkConfig child1 = frameworkConfig("CHILD1")
            .logLevel(LogService.LOG_DEBUG)
            .serviceTimeout(10000)
            .frameworkProperty("host", host)
            .frameworkProperty(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, systemPackages)
            .bundlePaths(defaultBundles, discoveryBundles);

        return configs(child1);
    }

    @Override
    protected void configureServices() throws Exception {
        String host = getParentContext().getBundleContext().getProperty("host");
        InetAddress ip = null;
        if (host != null) {
            ip = InetAddress.getByName(host);
        }
        m_jmDNS = JmDNS.create(ip, "BonjourDiscoveryTest");
        m_jmDNS.addServiceListener("_http._tcp.local.", this);
    }

    @Override
    protected void cleanupTest() throws Exception {
        if (m_si != null) {
            m_jmDNS.removeServiceListener("_http._tcp.local.", this);
            m_jmDNS.unregisterAllServices();
        }
        m_jmDNS = null;
    }

    @Override
    public void serviceAdded(ServiceEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void serviceRemoved(ServiceEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void serviceResolved(ServiceEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    protected ServiceInfo publishEndpoint(EndpointDescription endpointDescription) throws Exception {
        ServiceInfo serviceInfo = createServiceInfo(endpointDescription);
        logDebug("Registering mDNS service : " + serviceInfo);
        m_jmDNS.registerService(serviceInfo);
        return serviceInfo;
    }

    @Override
    protected void revokeEndpoint(ServiceInfo endpointRegistration) throws Exception {
        m_jmDNS.unregisterService(endpointRegistration);
    }

    /*
     * Bonjour specific tests
     */

    /**
     * Remote Service Admin 122.6.2
     * <br/><br/>
     * An Endpoint Listener is allowed to modify the set of filters in its scope through a service property
     * modification. This modification must result in new and/or existing Endpoint Descriptions to be added,
     * however, existing Endpoints that are no longer in scope are not required to be explicitly removed by
     * the their sources. It is the responsibility for the Endpoint Listener to remove these orphaned Endpoint
     * Description from its view.
     * <br/><br/>
     * TODO This test should apply to all Discovery implementations
     * 
     * @throws Exception on failure
     */
    // FIXME move this test up
    public void testBuggyServiceDiscovery() throws Exception {

        EndpointDescription endpointDescription = createEndpointDescription("q123", "interface1", 42l, "8888");
        ServiceInfo serviceInfo = createServiceInfo(endpointDescription);

        BlockingEndpointListener block1 =
            new BlockingEndpointListener(getBundleContext(), endpointDescription, "(" + Constants.OBJECTCLASS
                + "=interface1)");
        block1.register();

        BlockingEndpointListener block2 =
            new BlockingEndpointListener(getBundleContext(), endpointDescription, "(" + Constants.OBJECTCLASS
                + "=interface2)");
        block2.register();

        try {
            logDebug("Registering mDNS service : " + serviceInfo);
            m_jmDNS.registerService(serviceInfo);
            if (!block1.awaitAdded(30, TimeUnit.SECONDS)) {
                logError("Did not receive added callback with matching endpointdescription within 10 seconds");
                fail("Did not receive added callback with matching endpointdescription within 10 seconds");
            }
            logDebug("Received added callback for matching endpointdesciption!");

            assertEquals("Received a callback for desciption with non-matching scope", 0, block2.getAddedCount());

            block2.changeScopeFilter("(" + Constants.OBJECTCLASS + "=interface1)");
            if (!block2.awaitAdded(10, TimeUnit.SECONDS)) {
                logError("Did not receive added callback with matching endpointdescription within 10 seconds");
                fail("Did not receive added callback with matching endpointdescription within 10 seconds");
            }
        }
        finally {
            block1.unregister();
            block2.unregister();
        }
    }

    // FIXME temporary helpers
    private BundleContext getBundleContext() {
        return getChildContext("CHILD1").getBundleContext();
    }

    private void logDebug(String message) {
        getChildContext("CHILD1").getLogService().log(LogService.LOG_DEBUG, message);
    }

    private void logError(String message) {
        getChildContext("CHILD1").getLogService().log(LogService.LOG_WARNING, message);
    }
}
