/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.itest.junit.discovery;

import static org.amdatu.remoteservices.itest.config.Configs.configs;
import static org.amdatu.remoteservices.itest.config.Configs.frameworkConfig;
import static org.amdatu.remoteservices.itest.util.ITestUtil.createEndpointDescription;

import java.util.concurrent.TimeUnit;

import org.amdatu.remoteservices.discovery.hazelcast.EndpointData;
import org.amdatu.remoteservices.discovery.hazelcast.HazelcastConstants;
import org.amdatu.remoteservices.discovery.hazelcast.LeaseData;
import org.amdatu.remoteservices.discovery.hazelcast.LeaseEvent;
import org.amdatu.remoteservices.itest.config.Config;
import org.amdatu.remoteservices.itest.config.FrameworkConfig;
import org.amdatu.remoteservices.itest.util.BlockingEndpointListener;
import org.amdatu.remoteservices.itest.util.FrameworkContext;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.log.LogService;
import org.osgi.service.remoteserviceadmin.EndpointDescription;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ITopic;
import com.hazelcast.core.Member;

/**
 * Tests the Hazelcast based discovery implementation.
 */
public class HazelcastDiscoveryTest extends AbstractDiscoveryTest<EndpointData> {

    private HazelcastInstance m_hazelcast;
    private Member m_member;
    private IMap<String, EndpointData> m_endpointData;
    private IMap<String, LeaseData> m_leaseData;
    private ITopic<LeaseEvent> m_leaseTopic;

    @Override
    protected Config[] configureFramework(FrameworkContext parent) throws Exception {

        String systemPackages = getParentContext().getBundleContext().getProperty("itest.systempackages");
        String host = getParentContext().getBundleContext().getProperty("itest.host");

        String defaultBundles = getParentContext().getBundleContext().getProperty("itest.bundles.default");
        String discoveryBundles = getParentContext().getBundleContext().getProperty("itest.bundles.hazelcastdiscovery");

        parent.setLogLevel(LogService.LOG_DEBUG);
        parent.setServiceTimout(30000);

        FrameworkConfig child1 = frameworkConfig("CHILD1")
            .logLevel(LogService.LOG_DEBUG)
            .serviceTimeout(10000)
            .frameworkProperty("host", host)
            .frameworkProperty(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, systemPackages)
            .bundlePaths(defaultBundles, discoveryBundles);

        return configs(child1);
    }

    @Override
    protected void configureServices() throws Exception {
        m_hazelcast = createHazelcastInstance(this.getClass().getClassLoader(), 5051);
        m_member = m_hazelcast.getCluster().getLocalMember();
        m_endpointData = m_hazelcast.getMap(HazelcastConstants.ENDPOINTS_MAP);
        m_leaseData = m_hazelcast.getMap(HazelcastConstants.LEASES_MAP);
        m_leaseTopic = m_hazelcast.getTopic(HazelcastConstants.LEASE_TOPIC);

    }

    @Override
    protected void cleanupTest() throws Exception {
        m_hazelcast.getLifecycleService().shutdown();
    }

    @Override
    protected EndpointData publishEndpoint(EndpointDescription endpointDescription) throws Exception {
        EndpointData endpointData = new EndpointData(endpointDescription.getProperties(), m_member);
        LeaseData leaseData = new LeaseData(endpointData.getId(), 1000);
        m_leaseData.put(endpointData.getId(), leaseData);
        m_endpointData.put(endpointData.getId(), endpointData);
        return endpointData;
    }

    @Override
    protected void revokeEndpoint(EndpointData endpointRegistration) throws Exception {
        m_endpointData.remove(endpointRegistration.getId(), endpointRegistration);
    }

    /*
     * Hazelcast specific tests
     */

    public void testDiscoveryLeases() throws Exception {

        EndpointDescription endpointDescription = createEndpointDescription("q123", "interface2", 42l);
        EndpointData endpointData = new EndpointData(endpointDescription.getProperties(), m_member);
        LeaseData leaseData = new LeaseData(endpointData.getId(), 1000);
        BlockingEndpointListener block = new BlockingEndpointListener(getBundleContext(), endpointDescription);
        block.register();

        try {
            logDebug("Publishing endpoint data");
            m_leaseData.put(endpointData.getId(), leaseData);
            m_endpointData.put(endpointData.getId(), endpointData);

            logDebug("Awaiting discovery callback");
            if (!block.awaitAdded(30, TimeUnit.SECONDS)) {
                logError("Did not receive callback in time");
                fail("Did not receive callback in time");
            }
            logDebug("Received added callback!");

            for (int i = 0; i < 10; i++) {
                Thread.sleep(1000);
                logDebug("Updating endpoint lease");
                m_leaseTopic.publish(new LeaseEvent(endpointData.getId(), leaseData.getDuration()));
            }

            logDebug("Now the lease should expire..");

            block.reset();
            logDebug("Awaiting discovery callback");
            if (!block.awaitRemoved(30, TimeUnit.SECONDS)) {
                logError("Did not receive removed callback with matching endpointdescription within 10 seconds");
                fail("Did not receive removed callback with matching endpointdescription within 10 seconds");
            }
            logDebug("Received removed callback for matching endpointdesciption!");

        }
        finally {
            block.unregister();
        }
    }

    private static HazelcastInstance createHazelcastInstance(ClassLoader cl, int port) {
        com.hazelcast.config.Config config = new com.hazelcast.config.Config();
        config.setClassLoader(cl);
        config.setProperty("hazelcast.logging.type", "none");
// config.setPort(port);
        return Hazelcast.newHazelcastInstance(config);
    }

    private BundleContext getBundleContext() {
        return getChildContext("CHILD1").getBundleContext();
    }

    private void logDebug(String message) {
        getChildContext("CHILD1").getLogService().log(LogService.LOG_DEBUG, message);
    }

    private void logError(String message) {
        getChildContext("CHILD1").getLogService().log(LogService.LOG_WARNING, message);
    }
}
