/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.itest.util;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

/**
 * Collection of generic itest utility methods.
 */
public class ITestUtil {

    /**
     * Create a description from the specified arguments.
     * 
     * @param frameworkUUID the frameworkUUID
     * @param interfaceClass the interface class
     * @param serviceId the service id
     * @return the description
     */
    public static EndpointDescription createEndpointDescription(String frameworkUUID, String interfaceClass,
        long serviceId) {
        Hashtable<String, Object> descriptionProps = new Hashtable<String, Object>();
        descriptionProps.put(Constants.OBJECTCLASS, new String[] { interfaceClass });
        descriptionProps.put(RemoteConstants.ENDPOINT_ID, frameworkUUID + "/" + interfaceClass);
        descriptionProps.put(RemoteConstants.ENDPOINT_SERVICE_ID, serviceId);
        descriptionProps.put(RemoteConstants.ENDPOINT_FRAMEWORK_UUID, frameworkUUID);
        descriptionProps.put(RemoteConstants.SERVICE_IMPORTED_CONFIGS, new String[] { "ARS" });
        EndpointDescription endpointDescription = new EndpointDescription(descriptionProps);
        return endpointDescription;
    }

    // FIXME temporary overloaded method for Bonjour.. we need to get the props straight.
    public static EndpointDescription createEndpointDescription(String frameworkUUID, String interfaceClass,
        long serviceId, String port) {
        Hashtable<String, Object> descriptionProps = new Hashtable<String, Object>();
        descriptionProps.put(Constants.OBJECTCLASS, new String[] { interfaceClass });
        descriptionProps.put(RemoteConstants.ENDPOINT_ID, frameworkUUID + "/" + interfaceClass);
        descriptionProps.put(RemoteConstants.ENDPOINT_SERVICE_ID, serviceId);
        descriptionProps.put(RemoteConstants.ENDPOINT_FRAMEWORK_UUID, frameworkUUID);
        descriptionProps.put(RemoteConstants.SERVICE_IMPORTED_CONFIGS, new String[] { "ARS" });
        descriptionProps.put(".ars.port", port);
        EndpointDescription endpointDescription = new EndpointDescription(descriptionProps);
        return endpointDescription;
    }

    /**
     * Creates a Properties object from a list of key-value pairs, e.g.
     * <pre>
     * properties("key", "value", "key2", "value2");
     * </pre>
     */
    public static Dictionary<String, Object> properties(String... values) {
        Dictionary<String, Object> props = new Hashtable<String, Object>();
        for (int i = 0; i < values.length; i += 2) {
            props.put(values[i], values[i + 1]);
        }
        return props;
    }

    /**
     * Retrieve the list if IPv4 addresses of the current environment excluding loopback devices.
     * 
     * @return list of addresses
     * @throws SocketException if lookup fails
     */
    public static List<Inet4Address> getInet4Addresses() throws SocketException {
        List<Inet4Address> addresses = new ArrayList<Inet4Address>();
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface networkInterface = (NetworkInterface) interfaces.nextElement();
            if (networkInterface.isLoopback()) {
                continue;
            }
            Enumeration<InetAddress> adresses = networkInterface.getInetAddresses();
            while (adresses.hasMoreElements()) {
                InetAddress ip = (InetAddress) adresses.nextElement();
                if (ip instanceof Inet4Address) {
                    addresses.add((Inet4Address) ip);
                }
            }
        }
        return addresses;
    }

    /**
     * Join an iterable of strings into a single using the specified delimiter.
     * 
     * @param iterable the subject
     * @param delimiter the delimiter
     * @return the join
     */
    public static String join(Iterable<? extends CharSequence> iterable, String delimiter) {
        Iterator<? extends CharSequence> iter = iterable.iterator();
        if (!iter.hasNext()) {
            return "";
        }
        StringBuilder buffer = new StringBuilder(iter.next());
        while (iter.hasNext()) {
            buffer.append(delimiter).append(iter.next());
        }
        return buffer.toString();
    }

    public static String join(String separator, String... parts) {
        StringBuilder builder = new StringBuilder();
        for (String part : parts) {
            if (builder.length() > 0) {
                builder.append(separator);
            }
            builder.append(part);
        }
        return builder.toString();
    }

    /**
     * Retrieve the framework UUID form the specified context.
     * 
     * @param bctx the context
     * @return the UUID
     */
    public static String getFrameworkUUID(BundleContext bctx) {
        synchronized ("org.osgi.framework.uuid") {
            String uuid = bctx.getProperty("org.osgi.framework.uuid");
            if (uuid == null) {
                uuid = UUID.randomUUID().toString();
                System.setProperty("org.osgi.framework.uuid", uuid);
            }
            return uuid;
        }
    }

    private ITestUtil() {
    }
}
