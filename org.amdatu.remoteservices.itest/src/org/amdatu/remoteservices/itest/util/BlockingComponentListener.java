/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.itest.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.ComponentDependencyDeclaration;
import org.apache.felix.dm.ComponentStateListener;

/**
 * Self-registering utility that internally uses {@link ComponentStateListener} to monitor component state. It
 * can be used to await the start of a number of components.
 */
public final class BlockingComponentListener {

    private final ComponentStateListener m_listener = new InternalComponentStateListener();
    private final List<Component> m_components = new ArrayList<Component>();
    
    private volatile CountDownLatch m_startedLatch;

    public BlockingComponentListener(Component[] components) {
        m_components.addAll(Arrays.asList(components));
        m_startedLatch = new CountDownLatch(m_components.size());
        for (Component component : m_components) {
            component.addStateListener(m_listener);
        }
    }

    public boolean awaitStarted(long timeout, TimeUnit unit) {
        try {
            return m_startedLatch.await(timeout, unit);
        }
        catch (InterruptedException e) {
            return false;
        }
    }

    /**
     * Resets the block to be used again.
     */
    public synchronized void reset() {
        m_startedLatch = new CountDownLatch(m_components.size());
    }

    @SuppressWarnings("unchecked")
    public synchronized String toLogMessage() {
        StringBuilder result = new StringBuilder();
        for (Component component : m_components) {
            result.append(component).append('\n');
            for (ComponentDependencyDeclaration dependency : (List<ComponentDependencyDeclaration>) component
                .getDependencies()) {
                result.append("  ")
                    .append(dependency.toString())
                    .append(" ")
                    .append(ComponentDependencyDeclaration.STATE_NAMES[dependency.getState()])
                    .append('\n');
            }
            result.append('\n');
        }
        return result.toString();
    }

    private class InternalComponentStateListener implements ComponentStateListener {

        @Override
        public void started(Component component) {
            m_startedLatch.countDown();
        }

        @Override
        public void starting(Component component) {
        }

        @Override
        public void stopped(Component component) {
        }

        @Override
        public void stopping(Component component) {
        }
    }
}
