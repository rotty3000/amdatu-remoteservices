h1. About

Amdatu Remote Services is an implementation of the OSGi Remote Services / Remote Service Admin specification. It allows you to transparently remote your OSGi services using plugable discovery and transport protocols. The implementation is partly based on the CXF RemoteServiceAdmin implementation.

* "Amdatu Website":http://www.amdatu.org/
* "Source Code":https://bitbucket.org/amdatu/amdatu-remoteservices
* "Issue Tracking":https://amdatu.atlassian.net/browse/AMDATURS
* "Development Wiki":https://amdatu.atlassian.net/wiki/display/AMDATU
* "Continuous Build":https://amdatu.atlassian.net/builds/browse/AMDATURS
