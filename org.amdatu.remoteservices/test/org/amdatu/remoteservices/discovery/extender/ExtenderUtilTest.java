/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.extender;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.amdatu.remoteservices.common.EndpointDescriptorReader;
import org.amdatu.remoteservices.common.EndpointDescriptorWriter;
import org.osgi.framework.Constants;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

//TODO extend test 
public class ExtenderUtilTest extends TestCase {

    public void testMultiple() throws Exception {

        Map<String, Object> properties1 = new HashMap<String, Object>();
        properties1.put(Constants.OBJECTCLASS, new String[] { "AInterface", "Binterface" });
        properties1.put(RemoteConstants.ENDPOINT_ID, "x1");
        properties1.put(RemoteConstants.ENDPOINT_SERVICE_ID, 999l);
        properties1.put(RemoteConstants.ENDPOINT_FRAMEWORK_UUID, "xyz");
        properties1.put(RemoteConstants.SERVICE_IMPORTED_CONFIGS, new String[] { "AConfigType" });
        EndpointDescription endpoint1 = new EndpointDescription(properties1);

        Map<String, Object> properties2 = new HashMap<String, Object>();
        properties2.put(Constants.OBJECTCLASS, new String[] { "CInterface" });
        properties2.put(RemoteConstants.ENDPOINT_ID, "x2");
        properties2.put(RemoteConstants.ENDPOINT_SERVICE_ID, 888l);
        properties2.put(RemoteConstants.ENDPOINT_FRAMEWORK_UUID, "xyz");
        properties2.put(RemoteConstants.SERVICE_IMPORTED_CONFIGS, new String[] { "AConfigType" });
        EndpointDescription endpoint2 = new EndpointDescription(properties2);

        // to xml
        StringWriter writer = new StringWriter();
        new EndpointDescriptorWriter().writeDocument(writer, endpoint1, endpoint2);

        // read back
        List<EndpointDescription> endpoints =
            new EndpointDescriptorReader().parseDocument(new StringReader(writer.toString()));

        // check
        assertNotNull(endpoints);
        assertEquals(2, endpoints.size());
        assertEquals(endpoint1, endpoints.get(0));
        assertEquals(endpoint2, endpoints.get(1));
    }

    public void testPropertyValues() throws Exception {

        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(Constants.OBJECTCLASS, new String[] { "AInterface", "Binterface" });
        properties.put(RemoteConstants.ENDPOINT_ID, "x123");
        properties.put(RemoteConstants.ENDPOINT_SERVICE_ID, 999l);
        properties.put(RemoteConstants.ENDPOINT_FRAMEWORK_UUID, "xyz");
        properties.put(RemoteConstants.SERVICE_IMPORTED_CONFIGS, new String[] { "AConfigType" });
        properties.put("longObjectArray", new Long[] { 1l, 2l });
        properties.put("longTypeArray", new long[] { 1l, 2l });
        properties.put("longObect", new Long(5l));
        properties.put("longType", 6l);
        properties.put("someXML", "<array xmlns=\"qqq\" id= \"1\" class=\"q\"> <b a= \"1\" /> </array>");
        EndpointDescription endpoint = new EndpointDescription(properties);

        StringWriter writer = new StringWriter();
        new EndpointDescriptorWriter().writeDocument(writer, endpoint);

        // read back
        List<EndpointDescription> endpoints =
            new EndpointDescriptorReader().parseDocument(new StringReader(writer.toString()));

        assertNotNull(endpoints);
        assertEquals(1, endpoints.size());
        assertEquals(endpoint, endpoints.get(0));

// System.out.println(writer.toString());
    }
}
