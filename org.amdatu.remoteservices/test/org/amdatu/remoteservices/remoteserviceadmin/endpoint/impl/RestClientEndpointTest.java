/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import junit.framework.TestCase;

import org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.TestUtil.BoundType;
import org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.TestUtil.GenericType;
import org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.TestUtil.ServiceA;
import org.osgi.framework.ServiceException;

/**
 * Test cases for {@link RestClientEndpoint}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class RestClientEndpointTest extends TestCase implements URLStreamHandlerFactory {
    private static final String ENDPOINT_LOCATION = "test://";

    private static AtomicBoolean URL_FACTORY_INSTALLED = new AtomicBoolean(false);
    private static MutableURLStreamHandler m_urlHandler = new MutableURLStreamHandler();

    @Override
    public URLStreamHandler createURLStreamHandler(String protocol) {
        if ("test".equals(protocol)) {
            return m_urlHandler;
        }
        return null;
    }

    public void testCreateWithoutInterfacesFail() {
        try {
            new RestClientEndpoint(ENDPOINT_LOCATION);
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException e) {
            // Ok; expected...
        }
    }

    /**
     * Tests that we can call the correct methods for a generic type, with unbound types.
     * 
     * <p>Note that although the generic types are bound, the methods from the *generic*
     * interface are called (due to type erasure).</p>
     */
    @SuppressWarnings("rawtypes")
    public void testGenericTypeOk() throws Exception {
        Class<GenericType> genericType = GenericType.class;

        RestClientEndpoint endpoint = new RestClientEndpoint(ENDPOINT_LOCATION, genericType);

        GenericType<Integer, String> proxy = endpoint.getServiceProxy();

        setUpURLStreamHandler(new TestURLConnection(HTTP_NO_CONTENT, ""));

        // calls "void m(X x)"
        proxy.m(Integer.valueOf(3));

        setUpURLStreamHandler(new TestURLConnection(HTTP_OK, "1"));

        // calls "int m(Y y)"
        int result1 = proxy.m("foo");

        assertEquals(1, result1);

        setUpURLStreamHandler(new TestURLConnection(HTTP_OK, "3.1"));

        // calls "X m(X x, Y y)"
        Number result2 = proxy.m(Integer.valueOf(3), "foo");

        assertEquals(3.1, result2.doubleValue(), 0.0001);
    }

    /**
     * Tests that if a remote service throws a checked exception, that this is correctly unmarshalled by the client.
     */
    public void testHandleCheckedExceptionOk() throws Exception {
        Class<ServiceA> serviceAType = ServiceA.class;

        RestClientEndpoint endpoint = new RestClientEndpoint(ENDPOINT_LOCATION, serviceAType);

        ServiceA proxy = endpoint.getServiceProxy();

        setUpURLStreamHandler(new TestURLConnection(HTTP_BAD_REQUEST,
            "{\"type\":\"java.io.IOException\",\"msg\":\"Invalid value!\",\"stacktrace\":[]}"));

        try {
            // This should fail with an IOException...
            proxy.doException();
            fail("IOException expected!");
        }
        catch (IOException e) {
            // Ok; expected...
            assertEquals("Invalid value!", e.getMessage());
        }
    }

    /**
     * Tests that if a remote service returns a valid value, that this is correctly unmarshalled by the client.
     */
    public void testHandleRemoteCallInvalidServiceLocationOk() throws Exception {
        Class<ServiceA> serviceAType = ServiceA.class;

        RestClientEndpoint endpoint = new RestClientEndpoint("does-not-exist://", serviceAType);

        ServiceA proxy = endpoint.getServiceProxy();

        try {
            proxy.returnNull();
            fail("ServiceException expected!");
        }
        catch (ServiceException e) {
            // Ok; expected...
        }
    }

    /**
     * Tests that if a remote service returns a invalid value, that this is correctly signaled by the client.
     */
    public void testHandleRemoteCallReturnsInvalidTypeOk() throws Exception {
        Class<ServiceA> serviceAType = ServiceA.class;

        RestClientEndpoint endpoint = new RestClientEndpoint(ENDPOINT_LOCATION, serviceAType);

        ServiceA proxy = endpoint.getServiceProxy();

        setUpURLStreamHandler(new TestURLConnection(HTTP_OK, "[2]"));

        try {
            proxy.doubleIt(1);
            fail("ServiceException expected!");
        }
        catch (ServiceException e) {
            // Ok; expected...
        }
    }

    /**
     * Tests that if a remote service returns a valid value, that this is correctly unmarshalled by the client.
     */
    public void testHandleRemoteCallUnknownMethodOk() throws Exception {
        Class<ServiceA> serviceAType = ServiceA.class;

        RestClientEndpoint endpoint = new RestClientEndpoint(ENDPOINT_LOCATION, serviceAType);

        ServiceA proxy = endpoint.getServiceProxy();

        setUpURLStreamHandler(new TestURLConnection(HTTP_OK, "2"));

        // Only positive numbers are allowed...
        assertEquals(ENDPOINT_LOCATION.hashCode(), proxy.hashCode());
    }

    /**
     * Tests that if a remote service returns a valid value, that this is correctly unmarshalled by the client.
     */
    public void testHandleRemoteCallWithNullParameterOk() throws Exception {
        Class<ServiceA> serviceAType = ServiceA.class;

        RestClientEndpoint endpoint = new RestClientEndpoint(ENDPOINT_LOCATION, serviceAType);

        ServiceA proxy = endpoint.getServiceProxy();

        setUpURLStreamHandler(new TestURLConnection(HTTP_OK, "0"));

        // Only positive numbers are allowed...
        assertEquals(0, proxy.tripeIt(null));
    }

    /**
     * Tests that if a remote service returns a valid value, that this is correctly unmarshalled by the client.
     */
    public void testHandleRemoteCallWithoutReturnValueOk() throws Exception {
        Class<ServiceA> serviceAType = ServiceA.class;

        RestClientEndpoint endpoint = new RestClientEndpoint(ENDPOINT_LOCATION, serviceAType);

        ServiceA proxy = endpoint.getServiceProxy();

        setUpURLStreamHandler(new TestURLConnection(HTTP_NO_CONTENT, ""));

        // Only positive numbers are allowed...
        proxy.doNothing();
    }

    /**
     * Tests that if a remote service returns a valid value, that this is correctly unmarshalled by the client.
     */
    public void testHandleRemoteCallWithReturnValueOk() throws Exception {
        Class<ServiceA> serviceAType = ServiceA.class;

        RestClientEndpoint endpoint = new RestClientEndpoint(ENDPOINT_LOCATION, serviceAType);

        ServiceA proxy = endpoint.getServiceProxy();

        setUpURLStreamHandler(new TestURLConnection(HTTP_OK, "2"));

        // Only positive numbers are allowed...
        assertEquals(2, proxy.doubleIt(1));
    }

    /**
     * Tests that if a remote service throws a runtime exception, that this is correctly unmarshalled by the client.
     */
    public void testHandleRuntimeExceptionOk() throws Exception {
        Class<ServiceA> serviceAType = ServiceA.class;

        RestClientEndpoint endpoint = new RestClientEndpoint(ENDPOINT_LOCATION, serviceAType);

        ServiceA proxy = endpoint.getServiceProxy();

        setUpURLStreamHandler(new TestURLConnection(HTTP_BAD_REQUEST,
            "{\"type\":\"java.lang.IllegalArgumentException\",\"msg\":\"Invalid value!\",\"stacktrace\":[]}"));

        try {
            // This should fail with an IllegalArgumentException...
            proxy.doubleIt(0);
            fail("IllegalArgumentException expected!");
        }
        catch (IllegalArgumentException e) {
            // Ok; expected...
            assertEquals("Invalid value!", e.getMessage());
        }
    }

    /**
     * Tests that if a remote service throws a checked exception, that this is correctly unmarshalled by the client.
     */
    public void testHandleServerExceptionOk() throws Exception {
        Class<ServiceA> serviceAType = ServiceA.class;

        RestClientEndpoint endpoint = new RestClientEndpoint(ENDPOINT_LOCATION, serviceAType);

        ServiceA proxy = endpoint.getServiceProxy();

        setUpURLStreamHandler(new TestURLConnection(HTTP_INTERNAL_ERROR, ""));

        try {
            // This should fail with an IOException...
            proxy.doNothing();
            fail("ServiceException expected!");
        }
        catch (ServiceException e) {
            // Ok; expected...
        }
    }

    /**
     * Tests that the generic object methods, like {@link Object#hashCode()} & {@link Object#equals(Object)} aren't forwarded to the server.
     */
    public void testObjectMethodsOk() throws Exception {
        Class<ServiceA> serviceAType = ServiceA.class;

        RestClientEndpoint endpoint = new RestClientEndpoint(ENDPOINT_LOCATION, serviceAType);

        ServiceA proxy = endpoint.getServiceProxy();

        // Object#hashCode, Object#equals & Object#toString should be called on the service location... 
        assertEquals(ENDPOINT_LOCATION.hashCode(), proxy.hashCode());
        assertEquals(ENDPOINT_LOCATION, proxy.toString());
        assertTrue(proxy.equals(ENDPOINT_LOCATION));
        assertFalse(proxy.equals(proxy));
    }

    /**
     * Tests that for a concrete instance of a generic type, the correct methods are called.
     * 
     * <p>Note that although the generic types are bound, the methods from the *generic*
     * interface are called (due to type erasure).</p>
     */
    public void testTypeErasureOk() throws Exception {
        Class<BoundType> type = BoundType.class;

        RestClientEndpoint endpoint = new RestClientEndpoint(ENDPOINT_LOCATION, type);

        BoundType proxy = endpoint.getServiceProxy();

        setUpURLStreamHandler(new TestURLConnection(HTTP_NO_CONTENT, ""));

        // calls "void m(Long x)"
        proxy.m(Long.valueOf(3));

        setUpURLStreamHandler(new TestURLConnection(HTTP_OK, "1"));

        // calls "int m(String y)"
        int result1 = proxy.m("foo");

        assertEquals(1, result1);

        setUpURLStreamHandler(new TestURLConnection(HTTP_OK, "100000000000000"));

        // calls "Long m(Long x, String y)"
        Number result2 = proxy.m(Long.valueOf(3), "foo");

        assertEquals(100000000000000L, result2.longValue());

        setUpURLStreamHandler(new TestURLConnection(HTTP_OK, "\"foo\""));

        // calls "String m(String y, Long x)"
        String result3 = proxy.m("foo", Long.valueOf(3));

        assertEquals("foo", result3);
    }

    @Override
    protected void setUp() throws Exception {
        if (URL_FACTORY_INSTALLED.compareAndSet(false, true)) {
            URL.setURLStreamHandlerFactory(this);
        }
    }

    private void setUpURLStreamHandler(final TestURLConnection conn) {
        m_urlHandler.setUpURLConnection(conn);
    }

    static class MutableURLStreamHandler extends URLStreamHandler {
        private final AtomicReference<URLConnection> m_urlConnRef = new AtomicReference<URLConnection>();

        public void setUpURLConnection(TestURLConnection conn) {
            m_urlConnRef.set(conn);
        }

        @Override
        protected URLConnection openConnection(URL u) throws IOException {
            return m_urlConnRef.get();
        }
    }

    static class TestURLConnection extends HttpURLConnection {
        private final ByteArrayOutputStream m_baos;
        private final ByteArrayInputStream m_bais;

        public TestURLConnection(int rc, String result) {
            super(null);

            m_baos = new ByteArrayOutputStream();
            m_bais = new ByteArrayInputStream(result.getBytes());

            responseCode = rc;
        }

        @Override
        public void connect() throws IOException {
            // Nop
        }

        @Override
        public void disconnect() {
            // Nop
        }

        @Override
        public InputStream getErrorStream() {
            if (responseCode < 400) {
                return super.getErrorStream();
            }
            return m_bais;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            if (responseCode >= 400) {
                return super.getInputStream();
            }
            return m_bais;
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            return m_baos;
        }

        @Override
        public int getResponseCode() throws IOException {
            return responseCode;
        }

        @Override
        public boolean usingProxy() {
            return false;
        }
    }
}
