/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_NOT_ACCEPTABLE;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static javax.servlet.http.HttpServletResponse.SC_NO_CONTENT;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_SERVICE_UNAVAILABLE;
import static org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.Util.hash;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.TestCase;

import org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.TestUtil.ServiceA;
import org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.TestUtil.ServiceAImpl;
import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * Test cases for {@link RestServerEndpoint}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class RestServerEndpointTest extends TestCase {
    private BundleContext m_context;
    private ServiceReference<Object> m_serviceRef;
    private HttpServletRequest m_servletRequest;
    private HttpServletResponse m_servletResponse;
    private MockServletOutputStream m_baos;

    /**
     * Tests that we can call a method that fails with a checked exception.
     */
    public void testCallMethodWithCheckedExceptionOk() throws Exception {
        Class<ServiceA> type = ServiceA.class;
        ServiceAImpl service = spy(new ServiceAImpl());

        Method m = type.getMethod("doException");
        mockServiceLookup(service, m, "[]");

        RestServerEndpoint endpoint = createEndpoint(type);
        endpoint.doPost(m_servletRequest, m_servletResponse);

        verify(m_servletResponse).setStatus(SC_BAD_REQUEST);
        verify(service).doException();

        Throwable t = m_baos.asThrowable();
        assertNotNull(t);
        assertEquals(IOException.class, t.getClass());
        assertEquals("Exception!", t.getMessage());
    }

    /**
     * Tests that we can call a void-method.
     */
    public void testCallMethodWithIncorrectArgumentsOk() throws Exception {
        Class<ServiceA> type = ServiceA.class;
        ServiceAImpl service = spy(new ServiceAImpl());

        Method m = type.getMethod("doNothing");
        mockServiceLookup(service, m, "[3]");

        RestServerEndpoint endpoint = createEndpoint(type);
        endpoint.doPost(m_servletRequest, m_servletResponse);

        verify(m_servletResponse).sendError(SC_NOT_ACCEPTABLE);
        verifyNoMoreInteractions(service);

        m_baos.assertContent("");
    }

    /**
     * Tests that we can call a void-method.
     */
    public void testCallMethodWithNullResultOk() throws Exception {
        Class<ServiceA> type = ServiceA.class;
        ServiceAImpl service = spy(new ServiceAImpl());

        Method m = type.getMethod("returnNull");
        mockServiceLookup(service, m, "[]");

        RestServerEndpoint endpoint = createEndpoint(type);
        endpoint.doPost(m_servletRequest, m_servletResponse);

        verify(m_servletResponse).setStatus(SC_OK);
        verify(service).returnNull();

        m_baos.assertContent("null");
    }

    /**
     * Tests that we can call a void-method.
     */
    public void testCallMethodWithoutServiceOk() throws Exception {
        Class<ServiceA> type = ServiceA.class;
        ServiceAImpl service = spy(new ServiceAImpl());

        Method m = type.getMethod("doNothing");
        mockServiceLookup(null, m, "[]");

        RestServerEndpoint endpoint = createEndpoint(type);
        endpoint.doPost(m_servletRequest, m_servletResponse);

        verify(m_servletResponse).sendError(SC_SERVICE_UNAVAILABLE);
        verifyNoMoreInteractions(service);

        m_baos.assertContent("");
    }

    /**
     * Tests that we can call a method successfully and process its result.
     */
    public void testCallMethodWithResultOk() throws Exception {
        Class<ServiceA> type = ServiceA.class;
        ServiceAImpl service = spy(new ServiceAImpl());

        Method m = type.getMethod("doubleIt", Integer.TYPE);
        mockServiceLookup(service, m, "[3]");

        RestServerEndpoint endpoint = createEndpoint(type);
        endpoint.doPost(m_servletRequest, m_servletResponse);

        verify(service).doubleIt(eq(3));
        verify(m_servletResponse).setStatus(SC_OK);

        m_baos.assertContent("6");
    }

    /**
     * Tests that we can call a method that fails with a runtime exception.
     */
    public void testCallMethodWithRuntimeExceptionOk() throws Exception {
        Class<ServiceA> type = ServiceA.class;
        ServiceAImpl service = spy(new ServiceAImpl());

        Method m = type.getMethod("doubleIt", Integer.TYPE);
        mockServiceLookup(service, m, "[0]");

        RestServerEndpoint endpoint = createEndpoint(type);
        endpoint.doPost(m_servletRequest, m_servletResponse);

        verify(m_servletResponse).setStatus(SC_BAD_REQUEST);
        verify(service).doubleIt(eq(0));

        Throwable t = m_baos.asThrowable();
        assertNotNull(t);
        assertEquals(IllegalArgumentException.class, t.getClass());
        assertEquals("Invalid value!", t.getMessage());
    }

    /**
     * Tests that we can call a void-method.
     */
    public void testCallUnknownMethodOk() throws Exception {
        Class<ServiceA> type = ServiceA.class;
        ServiceAImpl service = spy(new ServiceAImpl());

        Method m = service.getClass().getMethod("hashCode");
        mockServiceLookup(service, m, "[]");

        RestServerEndpoint endpoint = createEndpoint(type);
        endpoint.doPost(m_servletRequest, m_servletResponse);

        verify(m_servletResponse).sendError(SC_NOT_FOUND);
        verifyNoMoreInteractions(service);

        m_baos.assertContent("");
    }

    /**
     * Tests that we can call a void-method.
     */
    public void testCallVoidMethodOk() throws Exception {
        Class<ServiceA> type = ServiceA.class;
        ServiceAImpl service = spy(new ServiceAImpl());

        Method m = type.getMethod("doNothing");
        mockServiceLookup(service, m, "[]");

        RestServerEndpoint endpoint = createEndpoint(type);
        endpoint.doPost(m_servletRequest, m_servletResponse);

        verify(m_servletResponse).setStatus(SC_NO_CONTENT);
        verify(service).doNothing();

        m_baos.assertContent("");
    }

    /**
     * Tests that we can call a void-method.
     */
    public void testCallWithoutMethodOk() throws Exception {
        Class<ServiceA> type = ServiceA.class;
        ServiceAImpl service = spy(new ServiceAImpl());

        mockServiceLookup(service, null, "[]");

        RestServerEndpoint endpoint = createEndpoint(type);
        endpoint.doPost(m_servletRequest, m_servletResponse);

        verify(m_servletResponse).sendError(SC_NOT_FOUND);
        verifyNoMoreInteractions(service);

        m_baos.assertContent("");
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void setUp() throws Exception {
        m_baos = new MockServletOutputStream();

        m_context = mock(BundleContext.class);
        m_serviceRef = mock(ServiceReference.class);
        m_servletRequest = mock(HttpServletRequest.class);
        m_servletResponse = mock(HttpServletResponse.class);
        when(m_servletResponse.getOutputStream()).thenReturn(m_baos);
    }

    private RestServerEndpoint createEndpoint(Class<?>... interfaces) {
        return new RestServerEndpoint(m_context, m_serviceRef, interfaces);
    }

    private void mockServiceLookup(Object service, Method m, String content) throws IOException {
        String hash = "";
        if (m != null) {
            hash = hash(m);
        }
        when(m_servletRequest.getPathInfo()).thenReturn("/".concat(hash));
        when(m_servletRequest.getInputStream()).thenReturn(new MockServletInputStream(content));
        when(m_context.getService(eq(m_serviceRef))).thenReturn(service);
    }

    static class MockServletInputStream extends ServletInputStream {
        private final ByteArrayInputStream m_bais;

        public MockServletInputStream(String input) {
            m_bais = new ByteArrayInputStream(input.getBytes());
        }

        @Override
        public int read() throws IOException {
            return m_bais.read();
        }
    }

    static class MockServletOutputStream extends ServletOutputStream {
        private final ByteArrayOutputStream m_baos = new ByteArrayOutputStream();

        @Override
        public void write(int b) throws IOException {
            m_baos.write(b);
        }

        void assertContent(String content) {
            assertEquals(content, getBodyContent());
        }

        String getBodyContent() {
            return new String(m_baos.toByteArray());
        }

        Throwable asThrowable() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ExceptionWrapper cause = mapper.readValue(getBodyContent(), ExceptionWrapper.class);
            return cause.getException();
        }
    }
}
