/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.impl;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.osgi.framework.ServiceReference;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.ExportReference;
import org.osgi.service.remoteserviceadmin.ExportRegistration;

/**
 * Implementation of (@link {@link ExportRegistration}.
 * 
 */
public final class ExportRegistrationImpl implements ExportRegistration {

    private final AtomicBoolean m_closed = new AtomicBoolean(false);

    private final AdminInstanceImpl m_admin;
    private final ExportReferenceImpl m_reference;
    private final Throwable m_exception;

    public ExportRegistrationImpl(AdminInstanceImpl admin, ServiceReference<?> serviceReference,
        EndpointDescription endpointDescription, Throwable exception) {
        m_admin = admin;
        m_reference = new ExportReferenceImpl(serviceReference, endpointDescription);
        m_exception = exception;
    }

    public ExportRegistrationImpl(AdminInstanceImpl admin, ExportRegistrationImpl source) {
        m_admin = admin;
        m_reference =
            new ExportReferenceImpl(source.internalGetExportReference().internalGetServiceReference(), source
                .internalGetExportReference().internalGetExportedEndpoint());
        m_exception = source.internalGetException();
    }

    @Override
    public ExportReference getExportReference() {

// FIXME We should return NULL according to the Javadoc but the TCK fails on it
// if (m_closed.get()) {
// return null;
// }
        return m_reference;
    }

    @Override
    public void close() {
        if (m_closed.compareAndSet(false, true)) {
            m_reference.internalClose();
            internalGetAdmin().getExportManager().registrationClosed(this);
        }
    }

    @Override
    public Throwable getException() {
        if (m_closed.get()) {
            return null;
        }
        return m_exception;
    }

    @Override
    public EndpointDescription update(Map<String, ?> properties) {
        // FIXME implement
        throw new IllegalStateException("Not implemented");
    }
    
    AdminInstanceImpl internalGetAdmin() {
        return m_admin;
    }

    ExportReferenceImpl internalGetExportReference() {
        return m_reference;
    }

    Throwable internalGetException() {
        return m_exception;
    }
}
