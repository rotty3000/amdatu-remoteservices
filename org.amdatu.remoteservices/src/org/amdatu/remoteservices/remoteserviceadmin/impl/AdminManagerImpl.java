/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.impl;

import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.remoteservices.common.AbstractComponent;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdmin;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdminListener;

/**
 * Activator for the Amdatu Remote Service Admin service implementation.
 */
public final class AdminManagerImpl extends AbstractComponent implements ServiceFactory<RemoteServiceAdmin> {

    private final ConcurrentHashMap<ServiceRegistration<RemoteServiceAdmin>, AdminInstanceImpl> m_instances =
        new ConcurrentHashMap<ServiceRegistration<RemoteServiceAdmin>, AdminInstanceImpl>();

    private final EventsHandlerImpl m_eventsHandler;
    private final ImportsHandlerImpl m_importHandler;
    private final ExportsHandlerImpl m_exportHandler;
    private final ServerEndpointHandlerImpl m_endpointHandler;

    public AdminManagerImpl() {
        super("rsadmin", "default");
        m_eventsHandler = new EventsHandlerImpl(this);
        m_endpointHandler = new ServerEndpointHandlerImpl(this, m_eventsHandler);
        m_importHandler = new ImportsHandlerImpl(this, m_eventsHandler);
        m_exportHandler = new ExportsHandlerImpl(this, m_eventsHandler, m_endpointHandler);
    }

    @Override
    public void startComponent() throws Exception {

        // TODO not sure if we need this
        m_endpointHandler.start();
        m_eventsHandler.start();
        m_importHandler.start();
        m_exportHandler.start();
    }

    @Override
    public void stopComponent() throws Exception {
        // Assuming Service Factory will unget all instances

        // TODO not sure if we need this
        m_endpointHandler.stop();
        m_eventsHandler.stop();
        m_importHandler.stop();
        m_exportHandler.stop();
    }

    /*
     * Component dependency call-backs
     */

    public void listenerAdded(ServiceReference<?> reference, RemoteServiceAdminListener listener) {
        m_eventsHandler.listenerAdded(reference, listener);
    }

    public void listenerRemoved(ServiceReference<?> reference, RemoteServiceAdminListener listener) {
        m_eventsHandler.listenerRemoved(reference, listener);
    }

    public void eventAdminAdded(ServiceReference<?> reference, EventAdmin eventAdmin) {
        m_eventsHandler.eventAdminAdded(reference, eventAdmin);
    }

    public void eventAdminRemoved(ServiceReference<?> reference, EventAdmin eventAdmin) {
        m_eventsHandler.eventAdminRemoved(reference, eventAdmin);
    }

    /*
     * ServiceFactory interface
     */

    @Override
    public RemoteServiceAdmin getService(Bundle bundle, ServiceRegistration<RemoteServiceAdmin> registration) {

        AdminInstanceImpl instance = new AdminInstanceImpl(m_exportHandler, m_importHandler);
        m_instances.put(registration, instance);
        return instance;
    }

    @Override
    public void ungetService(Bundle bundle, ServiceRegistration<RemoteServiceAdmin> registration,
        RemoteServiceAdmin service) {

        AdminInstanceImpl instance = m_instances.remove(registration);
        m_exportHandler.adminClosed(instance);
        m_importHandler.adminClosed(instance);
    }
}
