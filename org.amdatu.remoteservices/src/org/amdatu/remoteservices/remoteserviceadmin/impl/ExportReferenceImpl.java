/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.impl;

import java.util.concurrent.atomic.AtomicBoolean;

import org.osgi.framework.ServiceReference;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.ExportReference;

/**
 * Implementation of (@link {@link ExportReference}.
 */
public final class ExportReferenceImpl implements ExportReference {

    private final AtomicBoolean m_closed = new AtomicBoolean(false);

    private final ServiceReference<?> m_serviceReference;
    private final EndpointDescription m_endpointDescription;

    public ExportReferenceImpl(ServiceReference<?> serviceReference, EndpointDescription endpointDescription) {
        m_serviceReference = serviceReference;
        m_endpointDescription = endpointDescription;
    }

    @Override
    public ServiceReference<?> getExportedService() {
        if (m_closed.get()) {
            return null;
        }
        return m_serviceReference;
    }

    @Override
    public EndpointDescription getExportedEndpoint() {
        if (m_closed.get()) {
            return null;
        }
        return m_endpointDescription;
    }

    void internalClose() {
        m_closed.set(true);
    }

    ServiceReference<?> internalGetServiceReference() {
        return m_serviceReference;
    }

    EndpointDescription internalGetExportedEndpoint() {
        return m_endpointDescription;
    }
}
