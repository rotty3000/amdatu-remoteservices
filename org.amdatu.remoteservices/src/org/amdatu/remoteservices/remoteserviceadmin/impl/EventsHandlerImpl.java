/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.impl;

import java.security.AccessController;
import java.security.Permission;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.remoteservices.common.ServiceUtil;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.event.TopicPermission;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.EndpointPermission;
import org.osgi.service.remoteserviceadmin.ExportReference;
import org.osgi.service.remoteserviceadmin.ImportReference;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdminEvent;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdminListener;

/**
 * RSA component that handles events delivery.
 */
public final class EventsHandlerImpl extends AbstractHandler {

    private static final String EVENT_TOPIC_BASE = "org/osgi/service/remoteserviceadmin/";

    private static final Permission EVENT_TOPIC_PERMISSION = new TopicPermission(
        "org/osgi/service/remoteserviceadmin/*", TopicPermission.PUBLISH);

    private final Map<ServiceReference<?>, RemoteServiceAdminListener> m_listeners =
        new ConcurrentHashMap<ServiceReference<?>, RemoteServiceAdminListener>();

    private final Map<ServiceReference<?>, EventAdmin> m_admins =
        new ConcurrentHashMap<ServiceReference<?>, EventAdmin>();

    public EventsHandlerImpl(AdminManagerImpl adminManager) {
        super(adminManager);
    }

    /*
     * Component dependency call-backs
     */

    void listenerAdded(ServiceReference<?> reference, RemoteServiceAdminListener listener) {
        m_listeners.put(reference, listener);
        logDebug("listener added %s", reference);
    }

    void listenerRemoved(ServiceReference<?> reference, RemoteServiceAdminListener listener) {
        m_listeners.remove(reference);
        logDebug("listener removed %s", reference);
    }

    void eventAdminAdded(ServiceReference<?> reference, EventAdmin eventAdmin) {
        m_admins.put(reference, eventAdmin);
        logDebug("EventAdmin added %s", reference);
    }

    void eventAdminRemoved(ServiceReference<?> reference, EventAdmin eventAdmin) {
        m_admins.remove(reference);
        logDebug("EventAdmin removed %s", reference);
    }

    /*
     * 'API' methods
     */

    public void emitEvent(int type, Bundle source, ExportReference exportReference, Throwable exception) {
        if (m_listeners.isEmpty() && m_admins.isEmpty()) {
            return;
        }

        RemoteServiceAdminEvent serviceEvent =
            new RemoteServiceAdminEvent(type, source, exportReference, exception);

        EndpointPermission permission =
            new EndpointPermission(exportReference.getExportedEndpoint(),
                ServiceUtil.getFrameworkUUID(getBundleContext()), EndpointPermission.READ);

        for (Entry<ServiceReference<?>, RemoteServiceAdminListener> entry : m_listeners.entrySet()) {
            if (entry.getKey().getBundle().hasPermission(permission)) {
                entry.getValue().remoteAdminEvent(serviceEvent);
            }
        }
        if (!m_admins.isEmpty()) {
            final Event adminEvent = createEventAdminEvent(serviceEvent);
            postEventAdminEvent(adminEvent);
        }
    }

    public void emitEvent(int type, Bundle source, ImportReference importReference, Throwable exception) {
        if (m_listeners.isEmpty() && m_admins.isEmpty()) {
            return;
        }

        RemoteServiceAdminEvent serviceEvent = new RemoteServiceAdminEvent(type, source, importReference, exception);
        EndpointPermission permission =
            new EndpointPermission(importReference.getImportedEndpoint(),
                ServiceUtil.getFrameworkUUID(getBundleContext()), EndpointPermission.READ);

        for (Entry<ServiceReference<?>, RemoteServiceAdminListener> entry : m_listeners.entrySet()) {
            if (entry.getKey().getBundle().hasPermission(permission)) {
                entry.getValue().remoteAdminEvent(serviceEvent);
            }
        }
        if (!m_admins.isEmpty()) {
            Event adminEvent = createEventAdminEvent(serviceEvent);
            postEventAdminEvent(adminEvent);
        }
    }

    /*
     * Private methods
     */

    private void postEventAdminEvent(final Event event) {
        try {
            AccessController.checkPermission(EVENT_TOPIC_PERMISSION);
            for (EventAdmin eventAdmin : m_admins.values()) {
                eventAdmin.postEvent(event);
            }
        }
        catch (Exception e) {
            logWarning("No permission to post events!", e);
        }
    }

    /**
     * Map a Remote Service Admin Event to an EventAdmin event according to OSGi Enterprise R5 122.7.1.
     * 
     * @param event the Remote Service Admin event
     * @return the Event Admin event
     */
    private Event createEventAdminEvent(RemoteServiceAdminEvent event) {

        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put("bundle", getBundleContext().getBundle());
        properties.put("bundle.id", getBundleContext().getBundle().getBundleId());
        properties.put("bundle.symbolicname", getBundleContext().getBundle().getSymbolicName());
        properties.put("bundle.version", getBundleContext().getBundle().getVersion());
        properties.put("bundle.signer", new String[] {}); // TODO impl
        properties.put("event", event);
        properties.put("timestamp", System.currentTimeMillis());
        putIfValueNotNull(properties, "cause", event.getException());

        EndpointDescription description = getEventDescription(event);
        if (description != null) {
            putIfValueNotNull(properties, "endpoint.service.id", description.getServiceId());
            putIfValueNotNull(properties, "endpoint.framework.uuid", description.getFrameworkUUID());
            putIfValueNotNull(properties, "endpoint.id", description.getId());
            putIfValueNotNull(properties, "endpoint.imported.configs", description.getConfigurationTypes());
        }
        return new Event(getEventTopic(event.getType()), properties);
    }

    private static String getEventTopic(int type) {
        return EVENT_TOPIC_BASE + getEventName(type);
    }

    private static String getEventName(int type) {
        switch (type) {
            case RemoteServiceAdminEvent.EXPORT_ERROR:
                return "EXPORT_ERROR";
            case RemoteServiceAdminEvent.EXPORT_REGISTRATION:
                return "EXPORT_REGISTRATION";
            case RemoteServiceAdminEvent.EXPORT_UNREGISTRATION:
                return "EXPORT_UNREGISTRATION";
            case RemoteServiceAdminEvent.EXPORT_WARNING:
                return "EXPORT_WARNING";
            case RemoteServiceAdminEvent.IMPORT_ERROR:
                return "IMPORT_ERROR";
            case RemoteServiceAdminEvent.IMPORT_REGISTRATION:
                return "IMPORT_REGISTRATION";
            case RemoteServiceAdminEvent.IMPORT_UNREGISTRATION:
                return "IMPORT_UNREGISTRATION";
            case RemoteServiceAdminEvent.IMPORT_WARNING:
                return "IMPORT_WARNING";
            default:
                throw new IllegalStateException("Unknown event type : " + type);
        }
    }

    private static EndpointDescription getEventDescription(RemoteServiceAdminEvent event) {
        if (event.getImportReference() != null) {
            return event.getImportReference().getImportedEndpoint();
        }
        if (event.getExportReference() != null) {
            return event.getExportReference().getExportedEndpoint();
        }
        return null;
    }

    private static void putIfValueNotNull(Map<String, Object> properties, String key, Object value) {
        if (value != null) {
            properties.put(key, value);
        }
    }
}
