/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.impl;

import static org.amdatu.remoteservices.common.ServiceUtil.getFrameworkUUID;
import static org.amdatu.remoteservices.common.ServiceUtil.getStringPlusValue;
import static org.amdatu.remoteservices.remoteserviceadmin.RemoteServiceAdminConstants.ARS_ALIAS;
import static org.amdatu.remoteservices.remoteserviceadmin.RemoteServiceAdminConstants.ARS_CONFIG_TYPE;
import static org.amdatu.remoteservices.remoteserviceadmin.RemoteServiceAdminConstants.ARS_PASSBYVALYE_INTENT;
import static org.amdatu.remoteservices.remoteserviceadmin.RemoteServiceAdminConstants.ARS_SUPPORTED_INTENTS;
import static org.osgi.framework.Constants.OBJECTCLASS;
import static org.osgi.framework.Constants.SERVICE_ID;
import static org.osgi.service.remoteserviceadmin.EndpointPermission.EXPORT;
import static org.osgi.service.remoteserviceadmin.EndpointPermission.READ;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.ENDPOINT_FRAMEWORK_UUID;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.ENDPOINT_ID;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.ENDPOINT_SERVICE_ID;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.SERVICE_EXPORTED_CONFIGS;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.SERVICE_EXPORTED_INTENTS;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.SERVICE_EXPORTED_INTENTS_EXTRA;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.SERVICE_EXPORTED_INTERFACES;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.SERVICE_IMPORTED_CONFIGS;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.SERVICE_INTENTS;
import static org.osgi.service.remoteserviceadmin.RemoteServiceAdminEvent.EXPORT_REGISTRATION;
import static org.osgi.service.remoteserviceadmin.RemoteServiceAdminEvent.EXPORT_UNREGISTRATION;
import static org.osgi.service.remoteserviceadmin.RemoteServiceAdminEvent.*;

import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.RestServerEndpoint;
import org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.ServerEndpointProblemListener;
import org.osgi.framework.ServiceReference;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.EndpointPermission;
import org.osgi.service.remoteserviceadmin.ExportReference;
import org.osgi.service.remoteserviceadmin.ExportRegistration;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdmin;

/**
 * RSA component that handles all service exports.
 */
public final class ExportsHandlerImpl extends AbstractHandler implements ServerEndpointProblemListener {
    private static final Set<String> MY_SUPPORTED_INTENTS_SET = new HashSet<String>(
        Arrays.asList(ARS_SUPPORTED_INTENTS));

    private final Map<EndpointDescription, List<ExportRegistrationImpl>> m_exportRegistrations;
    private final ServerEndpointHandlerImpl m_endpointHandler;
    private final EventsHandlerImpl m_eventsHandler;

    public ExportsHandlerImpl(AdminManagerImpl adminManager, EventsHandlerImpl eventsHandler,
        ServerEndpointHandlerImpl endpointHandler) {
        super(adminManager);

        m_endpointHandler = endpointHandler;
        m_eventsHandler = eventsHandler;

        m_exportRegistrations = new HashMap<EndpointDescription, List<ExportRegistrationImpl>>();
    }

    /**
     * Returns an array exported intents based on the SERVICE_EXPORTED_INTENTS and
     * SERVICE_EXPORTED_INTENTS_EXTRA property values as well as the ARS default
     * ARS_PASSBYVALYE_INTENT.
     * 
     * @param properties the properties
     * @return an array of intents
     */
    private static String[] getExportedIntents(Map<String, Object> properties) {
        Object exportedIntents = properties.get(SERVICE_EXPORTED_INTENTS);
        Object exportedIntentsExtra = properties.get(SERVICE_EXPORTED_INTENTS_EXTRA);
        if (exportedIntents == null && exportedIntentsExtra == null) {
            return new String[] { ARS_PASSBYVALYE_INTENT };
        }
        Set<String> set = new HashSet<String>();
        if (exportedIntents != null) {
            for (String exportedIntent : getStringPlusValue(exportedIntents)) {
                set.add(exportedIntent);
            }
        }
        if (exportedIntentsExtra != null) {
            for (String exportedIntent : getStringPlusValue(exportedIntentsExtra)) {
                set.add(exportedIntent);
            }
        }
        set.add(ARS_PASSBYVALYE_INTENT);
        return set.toArray(new String[set.size()]);
    }

    /**
     * Returns a list of exported interface names as declared by the SERVICE_EXPORTED_INTERFACES property
     * using the following rules.
     * <ul>
     * <li>A single value of '*' means the OBJECTCLASS must be used </li>
     * <li>Any interface must be listed in the OBJECTCLASS</li>
     * </ul>
     * 
     * @param exportProperties the map of export properties
     * @return a list of exported interfaces names
     * @throws IllegalArgumentException if an interfaces is listed as export but it is not in the OBJECTCLASS.
     */
    private static String[] getExportedInterfaces(Map<String, ?> exportProperties) {
        String[] providedInterfaces = getStringPlusValue(exportProperties.get(OBJECTCLASS));
        String[] exportedInterfaces = getStringPlusValue(exportProperties.get(SERVICE_EXPORTED_INTERFACES));
        if (exportedInterfaces.length == 1 && exportedInterfaces[0].equals("*")) {
            return providedInterfaces;
        }
        for (String exportedInterface : exportedInterfaces) {
            if ("*".equals(exportedInterface)) {
                throw new IllegalArgumentException("Cannot accept wildcard together with other exported interfaces!");
            }
            boolean contained = false;
            for (String providedInterface : providedInterfaces) {
                contained |= providedInterface.equals(exportedInterface);
            }
            if (!contained) {
                throw new IllegalArgumentException("Exported interface " + exportedInterface
                    + " not implemented by service: " + Arrays.toString(providedInterfaces));
            }
        }
        return exportedInterfaces;
    }

    /**
     * Returns a map of merged properties from the specified Service Reference and an optional map with
     * extra properties. Merging is done under the following rules:
     * <ul>
     * <li>Properties with a key starting with a '.' are private and thus ignored</li>
     * <li>Extra properties override service properties irrespective of casing</li>
     * </ul>
     * 
     * @param reference a Service Reference
     * @param extraProperties a map of extra properties, can be {@link null}
     * @return a map of merged properties
     */
    private static Map<String, Object> getMergedProperties(ServiceReference<?> reference, Map<String, ?> extraProperties) {
        Map<String, Object> serviceProperties = new HashMap<String, Object>();
        for (String propertyKey : reference.getPropertyKeys()) {
            if (propertyKey.startsWith(".")) {
                continue;
            }
            serviceProperties.put(propertyKey, reference.getProperty(propertyKey));
        }

        if (extraProperties == null) {
            return serviceProperties;
        }
        Set<String> removeServicePropertyKeys = new HashSet<String>();
        for (String extraPropertyKey : extraProperties.keySet()) {
            if (extraPropertyKey.startsWith(".") || extraPropertyKey.equalsIgnoreCase(SERVICE_ID)
                || extraPropertyKey.equalsIgnoreCase(OBJECTCLASS)) {
                continue;
            }

            for (String servicePropertyKey : serviceProperties.keySet()) {
                if (servicePropertyKey.equalsIgnoreCase(extraPropertyKey)) {
                    removeServicePropertyKeys.add(servicePropertyKey);
                }
            }
            for (String removeServicePropertyKey : removeServicePropertyKeys) {
                serviceProperties.remove(removeServicePropertyKey);
            }
            removeServicePropertyKeys.clear();
            serviceProperties.put(extraPropertyKey, extraProperties.get(extraPropertyKey));
        }
        return serviceProperties;
    }

    /**
     * Determines whether an array of intents is supported bu this implementation.
     * 
     * @param exportedIntents the intents
     * @return {@code true} if supported, {@code false} otherwise.
     */
    private static boolean isExportedIntentsSupported(String[] exportedIntents) {
        if (exportedIntents == null) {
            return false;
        }
        for (String exportedIntent : exportedIntents) {
            if (!MY_SUPPORTED_INTENTS_SET.contains(exportedIntent)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Export a service to a given Endpoint for the specified Remote Service Admin
     * instance.
     * 
     * @param instance The Admin Instance
     * @param reference The Service Reference to export.
     * @param properties The properties to create a local Endpoint.
     * @return A {@code Collection} of {@link ExportRegistration}s for the
     *         specified Service Reference and properties.
     * @throws IllegalArgumentException If any of the properties has a value
     *         that is not syntactically correct.
     * @throws UnsupportedOperationException If any of the intents expressed
     *         through the properties is not supported by the distribution
     *         provider.
     * @see RemoteServiceAdmin#exportService(ServiceReference, Map)
     */
    public Collection<ExportRegistration> exportService(final AdminInstanceImpl instance,
        final ServiceReference<?> reference, final Map<String, ?> properties) {

        final EndpointDescription description = createEndpointDescription(getMergedProperties(reference, properties));
        if (description == null) {
            return Collections.emptyList();
        }

        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            securityManager.checkPermission(new EndpointPermission(description, getFrameworkUUID(getBundleContext()),
                EXPORT));

            return AccessController.doPrivileged(new PrivilegedAction<Collection<ExportRegistration>>() {
                @Override
                public Collection<ExportRegistration> run() {
                    return internalExportService(instance, reference, description);
                }
            });
        }
        return internalExportService(instance, reference, description);
    }

    /*
     * Private methods
     */

    /**
     * Return the currently active Export References.
     * 
     * @return A {@code Collection} of {@link ExportReference}s that are
     *         currently active.
     * @see RemoteServiceAdmin#getExportedServices()
     */
    public Collection<ExportReference> getExportedServices() {
        List<ExportReference> exportedServices = new ArrayList<ExportReference>();
        SecurityManager securityManager = System.getSecurityManager();

        synchronized (m_exportRegistrations) {
            for (List<ExportRegistrationImpl> registrations : m_exportRegistrations.values()) {
                for (ExportRegistrationImpl registration : registrations) {
                    ExportReference reference = registration.getExportReference();
                    if (reference == null) {
                        continue;
                    }
                    EndpointDescription description = reference.getExportedEndpoint();
                    if (description == null) {
                        continue;
                    }
                    try {
                        if (securityManager != null) {
                            securityManager.checkPermission(new EndpointPermission(description,
                                getFrameworkUUID(getBundleContext()), READ));
                        }
                        exportedServices.add(registration.getExportReference());
                    }
                    catch (SecurityException e) {
                        logDebug("Security exception while checking READ permissions for endpoint: %s", description);
                    }
                }
            }
            return Collections.unmodifiableCollection(exportedServices);
        }
    }

    @Override
    public void handleEndpointError(ExportRegistration exportRegistration, Exception e) {
        m_eventsHandler.emitEvent(EXPORT_ERROR, getBundleContext().getBundle(),
            ((ExportRegistrationImpl) exportRegistration).internalGetExportReference(), e);
    }

    @Override
    public void handleEndpointWarning(ExportRegistration exportRegistration, Exception e) {
        m_eventsHandler.emitEvent(EXPORT_WARNING, getBundleContext().getBundle(),
            ((ExportRegistrationImpl) exportRegistration).internalGetExportReference(), e);
    }

    /**
     * Close currently active Export Registrations for the specified Remote Service Admin instance. This
     * is an internal callback for the manager.
     * 
     * @param instance the instance
     */
    void adminClosed(AdminInstanceImpl instance) {
        List<ExportRegistrationImpl> closeRegistrations = new ArrayList<ExportRegistrationImpl>();

        synchronized (m_exportRegistrations) {
            for (List<ExportRegistrationImpl> registrations : m_exportRegistrations.values()) {
                for (ExportRegistrationImpl registration : registrations) {
                    if (registration.internalGetAdmin() == instance) {
                        closeRegistrations.add(registration);
                    }
                }
            }
        }

        for (ExportRegistrationImpl registration : closeRegistrations) {
            registration.close();
        }
    }

    EventsHandlerImpl getEventsHandler() {
        return m_eventsHandler;
    }

    /**
     * Close an active Export Registration. This is an internal callback for the registration.
     * 
     * @param instance the registration
     */
    void registrationClosed(final ExportRegistrationImpl exportRegistration) {
        final ExportReferenceImpl exportReference = exportRegistration.internalGetExportReference();
        final EndpointDescription endpointDescription = exportReference.internalGetExportedEndpoint();
        final Throwable exception = exportRegistration.internalGetException();

        logDebug("Export Registration closed for endpoint: %s", endpointDescription);

        int registrationCount = 0;
        synchronized (m_exportRegistrations) {
            List<ExportRegistrationImpl> registrations = m_exportRegistrations.get(endpointDescription);
            registrations.remove(exportRegistration);
            registrationCount = registrations.size();
            if (registrationCount == 0) {
                m_exportRegistrations.remove(endpointDescription);
            }
        }
        if (registrationCount == 0) {
            logDebug("No export registrations left. Unregistering server endpoint");
            m_endpointHandler.removeEndpoint(endpointDescription);
        }
        else {
            logDebug("%d export registration(s) left. Keeping server endpoint open", registrationCount);
        }

        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            AccessController.doPrivileged(new PrivilegedAction<Void>() {
                @Override
                public Void run() {
                    getEventsHandler().emitEvent(EXPORT_UNREGISTRATION, getBundleContext().getBundle(),
                        exportReference, exception);
                    return null;
                }
            });
        }
        else {
            getEventsHandler().emitEvent(EXPORT_UNREGISTRATION, getBundleContext().getBundle(), exportReference,
                exception);

        }
    }

    /**
     * Creates an Endpoint by merging and checking the provided properties map as well
     * as adding system properties.
     * 
     * @param properties optional extra properties
     * @param interfaceClasses
     * @return
     */
    private EndpointDescription createEndpointDescription(Map<String, Object> properties) {
        String[] configurationTypes = getStringPlusValue(properties.get(SERVICE_EXPORTED_CONFIGS));
        if (configurationTypes.length > 0) {
            if (!Arrays.asList(configurationTypes).contains(ARS_CONFIG_TYPE)) {
                logDebug("Can not export service (no supported configuration type specified): %s", properties);
                return null;
            }
        }
        properties.put(SERVICE_IMPORTED_CONFIGS, new String[] { ARS_CONFIG_TYPE });

        if (properties.get(SERVICE_EXPORTED_INTERFACES) == null) {
            logWarning("Can not export service (no exported interfaces): %s", properties);
            throw new IllegalArgumentException("Can not export service (no exported interfaces)");
        }

        String[] exportedInterfaces = getExportedInterfaces(properties);
        if (exportedInterfaces.length == 0) {
            logWarning("Can not export service (no exported interfaces): %s", properties);
            throw new IllegalArgumentException("Can not export service (no exported interfaces)");
        }

        properties.put(OBJECTCLASS, exportedInterfaces);

        String[] exportedIntents = getExportedIntents(properties);
        if (!isExportedIntentsSupported(exportedIntents)) {
            logDebug("Can not export service (unsupported intent specified): %s", properties);
            throw new UnsupportedOperationException("Can not export service (unsupported intent specified)");
        }
        properties.put(SERVICE_INTENTS, exportedIntents);

        properties.put(ENDPOINT_SERVICE_ID, properties.get(SERVICE_ID));
        properties.put(ENDPOINT_FRAMEWORK_UUID, getFrameworkUUID(getBundleContext()));

        String endpointID = new EndpointIdGenerator().generateId(properties);
        properties.put(ENDPOINT_ID, endpointID);

        URL endpointURL = m_endpointHandler.getEndpointURL(endpointID);
        properties.put(ARS_ALIAS, endpointURL.toString());

        // TODO remove these superfluous properties
        properties.put(".ars.host", endpointURL.getHost());
        properties.put(".ars.path", endpointURL.getPath());
        properties.put(".ars.port", endpointURL.getPort());

        return new EndpointDescription(properties);
    }

    /**
     * Internal main export service logic.
     * 
     * @param instance The Admin Instance
     * @param reference The Service Reference to export.
     * @param endoint The Endpoint Description.
     * @return A {@code Collection} of {@link ExportRegistration}s for the
     *         specified Service Reference and properties.
     * @see #exportService(AdminInstanceImpl, ServiceReference, Map)
     */
    private Collection<ExportRegistration> internalExportService(AdminInstanceImpl instance,
        ServiceReference<?> reference, EndpointDescription description) {

        ExportRegistrationImpl exportRegistration = null;
        synchronized (m_exportRegistrations) {
            List<ExportRegistrationImpl> registrations = m_exportRegistrations.get(description);
            if (registrations != null) {
                exportRegistration = new ExportRegistrationImpl(instance, registrations.get(0));

                logInfo("Added export registration for *existing* endpoint: %s", description);
            }
            else {
                registrations = new ArrayList<ExportRegistrationImpl>();
                m_exportRegistrations.put(description, registrations);

                try {
                    RestServerEndpoint endpoint = m_endpointHandler.addEndpoint(reference, description);
                    exportRegistration = new ExportRegistrationImpl(instance, reference, description, null);

                    endpoint.setProblemListener(this);
                    endpoint.setExportRegistration(exportRegistration);
                }
                catch (Exception e) {
                    exportRegistration = new ExportRegistrationImpl(instance, reference, description, e);
                }
                logInfo("Added export registration for *new* endpoint: %s", description);
            }

            registrations.add(exportRegistration);
        }

        getEventsHandler().emitEvent(EXPORT_REGISTRATION, getBundleContext().getBundle(),
            exportRegistration.getExportReference(), exportRegistration.getException());

        return Collections.singletonList((ExportRegistration) exportRegistration);
    }
}
