/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.impl;

import static org.amdatu.remoteservices.remoteserviceadmin.RemoteServiceAdminConstants.ARS_SUPPORTED_CONFIG_TYPES;
import static org.amdatu.remoteservices.remoteserviceadmin.RemoteServiceAdminConstants.ARS_SUPPORTED_INTENTS;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.REMOTE_CONFIGS_SUPPORTED;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.REMOTE_INTENTS_SUPPORTED;

import java.util.Dictionary;
import java.util.Hashtable;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.log.LogService;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdmin;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdminListener;

/**
 * Activator for the Amdatu Remote Service Admin service implementation.
 */
public final class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {

        Dictionary<String, Object> properties = new Hashtable<String, Object>();
        properties.put(REMOTE_CONFIGS_SUPPORTED, ARS_SUPPORTED_CONFIG_TYPES);
        properties.put(REMOTE_INTENTS_SUPPORTED, ARS_SUPPORTED_INTENTS);

        Component component = createComponent()
            .setInterface(RemoteServiceAdmin.class.getName(), properties)
            .setImplementation(AdminManagerImpl.class)
            .add(
                createServiceDependency()
                    .setService(LogService.class)
                    .setRequired(false))
            .add(
                createServiceDependency()
                    .setService(RemoteServiceAdminListener.class)
                    .setCallbacks("listenerAdded", "listenerRemoved")
                    .setRequired(false))
            .add(
                createServiceDependency()
                    .setService(EventAdmin.class)
                    .setCallbacks("eventAdminAdded", "eventAdminRemoved")
                    .setRequired(false));

        manager.add(component);
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager)
        throws Exception {

        // DM will clear components
    }
}
