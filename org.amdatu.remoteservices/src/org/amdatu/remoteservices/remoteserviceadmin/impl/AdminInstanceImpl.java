/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.impl;

import java.util.Collection;
import java.util.Map;

import org.osgi.framework.ServiceReference;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.ExportReference;
import org.osgi.service.remoteserviceadmin.ExportRegistration;
import org.osgi.service.remoteserviceadmin.ImportReference;
import org.osgi.service.remoteserviceadmin.ImportRegistration;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdmin;

/**
 * Remote Service Admin instance implementation.
 */
public final class AdminInstanceImpl implements RemoteServiceAdmin {

    private final ExportsHandlerImpl m_exportManager;
    private final ImportsHandlerImpl m_importManager;

    public AdminInstanceImpl(ExportsHandlerImpl exportManager, ImportsHandlerImpl importManager) {
        m_exportManager = exportManager;
        m_importManager = importManager;
    }

    public ExportsHandlerImpl getExportManager() {
        return m_exportManager;
    }

    public ImportsHandlerImpl getImportManager() {
        return m_importManager;
    }

    @Override
    public Collection<ExportRegistration> exportService(ServiceReference<?> reference, Map<String, ?> properties) {
        return getExportManager().exportService(this, reference, properties);
    }

    @Override
    public ImportRegistration importService(EndpointDescription endpointDescription) {
        return getImportManager().importService(this, endpointDescription);
    }

    @Override
    public Collection<ExportReference> getExportedServices() {
        return getExportManager().getExportedServices();
    }

    @Override
    public Collection<ImportReference> getImportedEndpoints() {
        return getImportManager().getImportedEndpoints();
    }
}
