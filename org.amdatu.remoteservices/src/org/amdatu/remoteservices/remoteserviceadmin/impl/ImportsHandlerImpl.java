/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.impl;

import static org.amdatu.remoteservices.common.ServiceUtil.getFrameworkUUID;
import static org.amdatu.remoteservices.remoteserviceadmin.RemoteServiceAdminConstants.ARS_ALIAS;
import static org.amdatu.remoteservices.remoteserviceadmin.RemoteServiceAdminConstants.ARS_CONFIG_TYPE;
import static org.osgi.service.remoteserviceadmin.EndpointPermission.IMPORT;
import static org.osgi.service.remoteserviceadmin.EndpointPermission.READ;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.SERVICE_IMPORTED;
import static org.osgi.service.remoteserviceadmin.RemoteServiceAdminEvent.IMPORT_ERROR;
import static org.osgi.service.remoteserviceadmin.RemoteServiceAdminEvent.IMPORT_REGISTRATION;
import static org.osgi.service.remoteserviceadmin.RemoteServiceAdminEvent.IMPORT_UNREGISTRATION;
import static org.osgi.service.remoteserviceadmin.RemoteServiceAdminEvent.IMPORT_WARNING;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.ClientEndpointProblemListener;
import org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.RestClientEndpoint;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.EndpointPermission;
import org.osgi.service.remoteserviceadmin.ImportReference;
import org.osgi.service.remoteserviceadmin.ImportRegistration;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdmin;

/**
 * RSA component that handles all service imports.
 */
public final class ImportsHandlerImpl extends AbstractHandler implements ClientEndpointProblemListener {

    private final Map<EndpointDescription, List<ImportRegistrationImpl>> m_importRegistrations;
    private final Map<EndpointDescription, ServiceRegistration<?>> m_clientEndpoints;
    private final EventsHandlerImpl m_eventsHandler;

    public ImportsHandlerImpl(AdminManagerImpl adminManager, EventsHandlerImpl eventsHandler) {
        super(adminManager);

        m_eventsHandler = eventsHandler;

        m_importRegistrations = new HashMap<EndpointDescription, List<ImportRegistrationImpl>>();
        m_clientEndpoints = new HashMap<EndpointDescription, ServiceRegistration<?>>();
    }

    private static Dictionary<String, Object> createImportedServiceProperties(EndpointDescription endpointDescription) {
        Dictionary<String, Object> serviceProperties = new Hashtable<String, Object>();
        serviceProperties.put(SERVICE_IMPORTED, endpointDescription.getId());
        for (String key : endpointDescription.getProperties().keySet()) {
            serviceProperties.put(key, endpointDescription.getProperties().get(key));
        }
        return serviceProperties;
    }

    /**
     * Returns the currently active Import References.
     * 
     * @return A {@code Collection} of {@link ImportReference}s that are
     *         currently active.
     * @see RemoteServiceAdmin#getImportedEndpoints()
     */
    public Collection<ImportReference> getImportedEndpoints() {
        List<ImportReference> importedEndpoints = new ArrayList<ImportReference>();
        SecurityManager securityManager = System.getSecurityManager();

        synchronized (m_importRegistrations) {
            for (Collection<ImportRegistrationImpl> importRegistrations : m_importRegistrations.values()) {
                for (ImportRegistrationImpl importRegistration : importRegistrations) {
                    ImportReference reference = importRegistration.getImportReference();
                    if (reference == null) {
                        continue;
                    }

                    EndpointDescription description = reference.getImportedEndpoint();
                    if (description == null) {
                        continue;
                    }

                    try {
                        if (securityManager != null) {
                            securityManager.checkPermission(new EndpointPermission(description,
                                getFrameworkUUID(getBundleContext()), READ));
                        }
                        importedEndpoints.add(reference);
                    }
                    catch (SecurityException e) {
                        logDebug("Security exception while checking READ permissions for endpoint: %s", description);
                    }
                }
            }
        }
        return Collections.unmodifiableCollection(importedEndpoints);
    }

    @Override
    public void handleEndpointError(ImportRegistration importRegistration, Exception e) {
        m_eventsHandler.emitEvent(IMPORT_ERROR, getBundleContext().getBundle(), ((ImportRegistrationImpl) importRegistration).internalGetReference(), e);
    }

    @Override
    public void handleEndpointWarning(ImportRegistration importRegistration, Exception e) {
        m_eventsHandler.emitEvent(IMPORT_WARNING, getBundleContext().getBundle(), ((ImportRegistrationImpl) importRegistration).internalGetReference(), e);
    }

    /**
     * Import a service from an Endpoint for the specified Remote Service Admin instance.
     * 
     * @param endpoint The Endpoint Description to be used for import.
     * @return An Import Registration
     * @throws SecurityException If the caller does not have the appropriate import permission
     *         for the Endpoint, and the Java Runtime Environment supports permissions.
     * @see RemoteServiceAdmin#importService(EndpointDescription)
     */
    public ImportRegistration importService(final AdminInstanceImpl instance, final EndpointDescription endpointDescription) {
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            securityManager.checkPermission(new EndpointPermission(endpointDescription, getFrameworkUUID(getBundleContext()), IMPORT));

            return AccessController.doPrivileged(new PrivilegedAction<ImportRegistration>() {
                @Override
                public ImportRegistration run() {
                    return internalImportService(instance, endpointDescription);
                }
            });
        }
        return internalImportService(instance, endpointDescription);
    }

    /**
     * Close currently active Import Registrations for the specified Remote Service Admin instance. This
     * is an internal callback for the manager.
     * 
     * @param instance the instance
     */
    void adminClosed(AdminInstanceImpl instance) {
        List<ImportRegistrationImpl> removedImportHolders = new ArrayList<ImportRegistrationImpl>();
        synchronized (m_importRegistrations) {
            for (List<ImportRegistrationImpl> registrations : m_importRegistrations.values()) {
                for (ImportRegistrationImpl registration : registrations) {
                    if (registration.internalGetAdmin() == instance) {
                        removedImportHolders.add(registration);
                    }
                }
            }
            for (ImportRegistrationImpl registration : removedImportHolders) {
                registration.close();
            }
        }
    }

    EventsHandlerImpl getEventsHandler() {
        return m_eventsHandler;
    }

    /*
     * Private methods
     */

    /**
     * Close an active Import Registration. This is an internal callback for the registration.
     * 
     * @param instance the registration
     */
    void registrationClosed(final ImportRegistrationImpl importRegistration) {
        final ImportReferenceImpl importReference = importRegistration.internalGetReference();
        final EndpointDescription endpointDescription = importReference.internalGetImportedEndpoint();
        final Throwable exception = importRegistration.internalGetException();

        logDebug("Import Registration closed for endpoint: %s", endpointDescription);

        int registrationCount = 0;
        synchronized (m_importRegistrations) {
            List<ImportRegistrationImpl> registrations = m_importRegistrations.get(endpointDescription);
            registrations.remove(registrationCount);
            registrationCount = registrations.size();
            if (registrationCount == 0) {
                m_importRegistrations.remove(endpointDescription);
            }
        }

        if (registrationCount == 0) {
            logDebug("No import registrations left. Unregistering local endpoint");
            unregisterClientEndpoint(endpointDescription);
        }
        else {
            logDebug("%d import registration(s) left. Keeping local endpoint open", registrationCount);
        }

        SecurityManager securityManager = System.getSecurityManager();
        final Bundle bundle = getBundleContext().getBundle();

        if (securityManager != null) {
            AccessController.doPrivileged(new PrivilegedAction<Void>() {
                @Override
                public Void run() {
                    getEventsHandler().emitEvent(IMPORT_UNREGISTRATION, bundle, importReference, exception);
                    return null;
                }
            });
        }
        else {
            getEventsHandler().emitEvent(IMPORT_UNREGISTRATION, bundle, importReference, exception);
        }
    }

    private RestClientEndpoint createImportedServiceProxy(EndpointDescription endpointDescription) {
        Object serviceLocation = endpointDescription.getProperties().get(ARS_ALIAS);
        if (serviceLocation == null || !(serviceLocation instanceof String)) {
            logWarning("Failed to create service proxy for endpoint. Missing mandatory property " + ARS_ALIAS + ": %s",
                endpointDescription);
            return null;
        }

        Bundle bundle = getBundleContext().getBundle();
        List<String> ifaces = endpointDescription.getInterfaces();
        int ifaceCount = ifaces.size();

        Class<?>[] interfaceClasses = new Class<?>[ifaceCount];
        for (int i = 0; i < ifaceCount; i++) {
            try {
                interfaceClasses[i] = bundle.loadClass(ifaces.get(i));
            }
            catch (ClassNotFoundException e) {
                logWarning("Failed to create service proxy for endpoint. Unable to load interface class: %s", e,
                    ifaces.get(i));
                return null;
            }
        }
        return new RestClientEndpoint((String) serviceLocation, interfaceClasses);
    }

    private ImportRegistration internalImportService(AdminInstanceImpl instance, EndpointDescription endpointDescription) {
        // FIXME this code is ignoring the properties in terms of registrations. It just uses the service reference
        // as key... probably not good.

        List<String> configurationTypes = endpointDescription.getConfigurationTypes();
        if (!configurationTypes.contains(ARS_CONFIG_TYPE)) {
            logInfo("No supported configuration type found. Ignoring endpoint: %s", endpointDescription);
            return null;
        }

        ImportRegistrationImpl importRegistration = null;
        synchronized (m_importRegistrations) {
            List<ImportRegistrationImpl> registrations = m_importRegistrations.get(endpointDescription);
            if (registrations != null) {
                importRegistration = new ImportRegistrationImpl(instance, registrations.get(0));

                logInfo("Added import registration for *existing* endpoint: %s", endpointDescription);
            }
            else {
                registrations = new ArrayList<ImportRegistrationImpl>();
                m_importRegistrations.put(endpointDescription, registrations);

                importRegistration = registerClientEndpoint(instance, endpointDescription);

                logInfo("Added import registration for *new* endpoint: %s", endpointDescription);
            }

            registrations.add(importRegistration);
        }

        getEventsHandler().emitEvent(IMPORT_REGISTRATION, getBundleContext().getBundle(),
            importRegistration.internalGetReference(), importRegistration.internalGetException());

        return importRegistration;
    }

    private ImportRegistrationImpl registerClientEndpoint(AdminInstanceImpl adminInstance,
        EndpointDescription endpointDescription) {
        RestClientEndpoint endpoint = createImportedServiceProxy(endpointDescription);
        if (endpoint == null) {
            return null;
        }

        Dictionary<String, Object> serviceProperties = createImportedServiceProperties(endpointDescription);

        List<String> ifaceList = endpointDescription.getInterfaces();
        String[] ifaces = ifaceList.toArray(new String[ifaceList.size()]);

        ServiceRegistration<?> serviceRegistration =
            getBundleContext().registerService(ifaces, endpoint.getServiceProxy(), serviceProperties);

        ImportRegistrationImpl reg = new ImportRegistrationImpl(adminInstance, serviceRegistration.getReference(), endpointDescription, null);
        // Make the import registration known to the endpoint so that it
        // use it to report problems when it fails (persistently)...
        endpoint.setImportRegistration(reg);
        // We want to keep informed of any problems in this endpoint...
        endpoint.setProblemListener(this);

        ServiceRegistration<?> oldEndpoint;
        synchronized (m_clientEndpoints) {
            oldEndpoint = m_clientEndpoints.put(endpointDescription, serviceRegistration);
        }
        if (oldEndpoint != null) {
            logWarning("Overwritten endpoint: %s; unregistering old one!", endpointDescription);
            oldEndpoint.unregister();
        }

        return reg;
    }

    private void unregisterClientEndpoint(EndpointDescription endpointDescription) {
        ServiceRegistration<?> localEndpoint = null;
        synchronized (m_clientEndpoints) {
            localEndpoint = m_clientEndpoints.remove(endpointDescription);
        }
        if (localEndpoint != null) {
            localEndpoint.unregister();
        }
    }
}
