/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.impl;

import static org.amdatu.remoteservices.common.ServiceUtil.getStringPlusValue;
import static org.osgi.framework.Constants.OBJECTCLASS;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.remoteservices.common.EndpointDescriptorWriter;
import org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.RestServerEndpoint;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.remoteserviceadmin.EndpointDescription;

/**
 * RSA component that handles all server endpoints.
 */
public final class ServerEndpointHandlerImpl extends AbstractHandler {

    // TODO Refactor RESTServerEndpoint from servlet to a simple handler
    // TODO Improve caching support with last-modified adn ETags

    // TODO In theory there could be more instances... path would clash
    public static final String SERVLET_PATH = "/ars";

    private final Map<String, EndpointDescription> m_endpoints = new HashMap<String, EndpointDescription>();
    private final Map<String, RestServerEndpoint> m_handlers = new HashMap<String, RestServerEndpoint>();
    private final ReentrantReadWriteLock m_lock = new ReentrantReadWriteLock();

    private volatile EndpointDescription[] m_endpointsArray = new EndpointDescription[0];
    private volatile ServiceRegistration<?> m_registration;
    private volatile URL m_baseURL;

    // TODO evaluate if we need to Events Handler
    public ServerEndpointHandlerImpl(AdminManagerImpl adminManager, EventsHandlerImpl eventsHandler) {
        super(adminManager);
    }

    @Override
    protected void startComponentDelegate() {
        try {
            // TODO this should be (more) configurable and robust
            int port = 8080;
            String value = getBundleContext().getProperty("org.osgi.service.http.port");
            if (value != null) {
                port = Integer.parseInt(value);
            }
            String host = getBundleContext().getProperty("org.apache.felix.http.host");
            if (host == null) {
                host = "localhost";
            }
            m_baseURL = new URL("http", host, port, SERVLET_PATH + "/");

            Dictionary<String, Object> properties = new Hashtable<String, Object>();
            properties.put("alias", SERVLET_PATH);

            m_registration = getBundleContext().registerService(Servlet.class.getName(), new ServerEndpointServlet(), properties);
        }
        catch (MalformedURLException e) {
            logError("Failed to initialize due to configuration problem!", e);
            throw new IllegalStateException("Configuration problem", e);
        }
    }

    @Override
    protected void stopComponentDelegate() {
        m_registration.unregister();
    }

    /**
     * Returns the runtime URL for a specified Endpoint ID.
     * 
     * @param endpointId The Endpoint ID
     * @return The URL
     * @throws IllegalArgumentException If the Endpoint ID is not a valid URL path segment
     */
    public URL getEndpointURL(String endpointId) {
        try {
            return new URL(m_baseURL, endpointId);
        }
        catch (MalformedURLException e) {
            throw new IllegalArgumentException("Invalid endpoint id", e);
        }
    }

    /**
     * Add a Server Endpoint.
     * 
     * @param reference The local Service Reference
     * @param endpoint The Endpoint Description
     */
    public RestServerEndpoint addEndpoint(ServiceReference<?> reference, EndpointDescription endpoint) {

        // TODO sanity check and throw exception se the Export Handler can deal with it
        String[] endpointInterfaces = getStringPlusValue(endpoint.getProperties().get(OBJECTCLASS));
        Class<?>[] serviceInterfaces = getServiceInterfaces(getBundleContext(), reference);
        Class<?>[] exportedInterfaces = getExportInterfaceClasses(serviceInterfaces, endpointInterfaces);
        RestServerEndpoint serv = new RestServerEndpoint(getBundleContext(), reference, exportedInterfaces);

        m_lock.writeLock().lock();
        try {
            m_endpoints.put(endpoint.getId(), endpoint);
            m_handlers.put(endpoint.getId(), serv);
            m_endpointsArray = m_endpoints.values().toArray(new EndpointDescription[m_endpoints.size()]);
        }
        finally {
            m_lock.writeLock().unlock();
        }
        
        return serv;
    }

    /**
     * Remove a Server Endpoint.
     * 
     * @param endpoint The Endpoint Description
     */
    public RestServerEndpoint removeEndpoint(EndpointDescription endpoint) {
        RestServerEndpoint serv;
        
        m_lock.writeLock().lock();
        try {
            m_endpoints.remove(endpoint.getId());
            serv = m_handlers.remove(endpoint.getId());
            m_endpointsArray = m_endpoints.values().toArray(new EndpointDescription[m_endpoints.size()]);
        }
        finally {
            m_lock.writeLock().unlock();
        }
        
        return serv;
    }

    private EndpointDescription getEndpoint(String id) {
        m_lock.readLock().lock();
        try {
            return m_endpoints.get(id);
        }
        finally {
            m_lock.readLock().unlock();
        }
    }

    private RestServerEndpoint getHandler(String id) {
        m_lock.readLock().lock();
        try {
            return m_handlers.get(id);
        }
        finally {
            m_lock.readLock().unlock();
        }
    }

    private EndpointDescription[] getEndpoints() {
        return m_endpointsArray;
    }

    /**
     * Returns an array of interface classes implemented by the service instance for the specified Service
     * Reference.
     * 
     * @param reference the reference
     * @return an array of interface classes
     */
    private static Class<?>[] getServiceInterfaces(BundleContext context, ServiceReference<?> reference) {
        Set<Class<?>> serviceInterfaces = new HashSet<Class<?>>();
        Object serviceInstance = context.getService(reference);
        try {
            if (serviceInstance != null) {
                collectInterfaces(serviceInstance.getClass(), serviceInterfaces);
            }
        }
        finally {
            context.ungetService(reference);
        }
        return serviceInterfaces.toArray(new Class<?>[serviceInterfaces.size()]);
    }

    private static void collectInterfaces(Class<?> clazz, Set<Class<?>> accumulator) {
        for (Class<?> serviceInterface : clazz.getInterfaces()) {
            if (accumulator.add(serviceInterface)) {
                // Collect the inherited interfaces...
                collectInterfaces(serviceInterface, accumulator);
            }
        }
        // Go up in the hierarchy...
        Class<?> parent = clazz.getSuperclass();
        if (parent != null) {
            collectInterfaces(parent, accumulator);
        }
    }

    /**
     * Returns an array of interface classes by retaining the classes provided as the first argument if their
     * name is listed in the second argument.
     * 
     * @param interfaceClasses and array of classes
     * @param interfaceNames a list of class names
     * @return an array of classes
     */
    private static Class<?>[] getExportInterfaceClasses(Class<?>[] interfaceClasses, String[] interfaceNames) {
        Class<?>[] exportInterfaceClasses = new Class<?>[interfaceNames.length];
        for (int i = 0; i < interfaceNames.length; i++) {
            String interfaceName = interfaceNames[i];
            
            for (Class<?> interfaceClass : interfaceClasses) {
                if (interfaceClass.getName().equals(interfaceName)) {
                    exportInterfaceClasses[i] = interfaceClass;
                }
            }

            if (exportInterfaceClasses[i] == null) {
                throw new IllegalArgumentException("Service does not implement " + interfaceName);
            }
        }
        return exportInterfaceClasses;
    }

    private static String normalizePath(String pathInfo) {
        if (pathInfo == null) {
            return "";
        }
        if (pathInfo.startsWith("/")) {
            pathInfo = pathInfo.substring(1);
        }
        if (pathInfo.endsWith("/")) {
            pathInfo = pathInfo.substring(0, pathInfo.length() - 1);
        }
        return pathInfo;
    }

    private static void closeQuietly(Closeable... closeables) {
        for (Closeable closeable : closeables) {
            try {
                closeable.close();
            }
            catch (IOException e) {}
        }
    }

    /**
     * Internal Servlet that handles all calls.
     */
    private class ServerEndpointServlet extends HttpServlet {

        private static final long serialVersionUID = 1L;

        private final EndpointDescriptorWriter m_writer = new EndpointDescriptorWriter();

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

            String path = normalizePath(req.getPathInfo());
            if (path.equals("")) {
                writeEndpoints(req, resp, getEndpoints());
            }
            else {
                EndpointDescription endpoint = getEndpoint(path);
                if (endpoint != null) {
                    writeEndpoints(req, resp, endpoint);
                }
                else {
                    resp.sendError(HttpServletResponse.SC_NOT_FOUND);
                }
            }
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

            String path = normalizePath(req.getPathInfo());
            if (path.equals("") || !path.contains("/")) {
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }
            else {
                String id = path.substring(0, path.indexOf("/"));
                path = path.substring(path.indexOf("/"));
                RestServerEndpoint handler = getHandler(id);
                if (handler != null) {
                    try {
                        handler.service(new EndpointHttpRequestWrapper(req, path), resp);
                    }
                    catch (Exception e) {
                        logError("Server Endpoint Handler failed: %s", e, id);
                        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    }
                }
                else {
                    resp.sendError(HttpServletResponse.SC_NOT_FOUND);
                }
            }
        }

        private void writeEndpoints(HttpServletRequest req, HttpServletResponse resp, EndpointDescription... endpoints)
            throws IOException {

            Writer out = null;
            try {
                out = new OutputStreamWriter(resp.getOutputStream());
                resp.setStatus(HttpServletResponse.SC_OK);
                resp.setContentType("text/xml");
                resp.setCharacterEncoding("UTF-8");
                resp.setDateHeader("Expires", System.currentTimeMillis() + 10000);
                m_writer.writeDocument(out, endpoints);
            }
            finally {
                closeQuietly(out);
            }
        }
    }

    /**
     * Wraps the request to hide the servlet path as long as handlers are servlets..
     */
    static class EndpointHttpRequestWrapper extends HttpServletRequestWrapper {

        private final String m_pathInfo;

        public EndpointHttpRequestWrapper(HttpServletRequest request, String pathInfo) {
            super(request);
            m_pathInfo = pathInfo;
        }

        @Override
        public String getPathInfo() {
            return m_pathInfo;
        }
    }
}
