/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.impl;

import org.amdatu.remoteservices.common.AbstractComponentDelegate;

/**
 * Simple base class for Admin Manager delegates.
 */
public abstract class AbstractHandler extends AbstractComponentDelegate {

    private final AdminManagerImpl m_adminManager;

    public AbstractHandler(AdminManagerImpl adminManager) {
        super(adminManager);
        m_adminManager = adminManager;
    }

    public final AdminManagerImpl getAdminManager() {
        return m_adminManager;
    }
}
