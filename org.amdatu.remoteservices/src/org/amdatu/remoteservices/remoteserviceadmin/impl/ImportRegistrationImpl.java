/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.impl;

import java.util.concurrent.atomic.AtomicBoolean;

import org.osgi.framework.ServiceReference;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.ImportReference;
import org.osgi.service.remoteserviceadmin.ImportRegistration;

/**
 * Implementation of (@link {@link ImportRegistration}.
 */
public final class ImportRegistrationImpl implements ImportRegistration {

    private final AtomicBoolean m_closed = new AtomicBoolean(false);

    private final AdminInstanceImpl m_adminInstance;
    private final Throwable m_exception;

    private volatile ImportReferenceImpl m_importReference;

    public ImportRegistrationImpl(AdminInstanceImpl admin, ServiceReference<?> serviceReference,
        EndpointDescription endpointDescription, Throwable exception) {

        m_adminInstance = admin;
        m_importReference = new ImportReferenceImpl(serviceReference, endpointDescription);
        m_exception = exception;
    }

    public ImportRegistrationImpl(AdminInstanceImpl admin, ImportRegistrationImpl source) {
        m_adminInstance = admin;
        m_importReference =
            new ImportReferenceImpl(source.internalGetReference().internalGetImportedService(), source
                .internalGetReference().internalGetImportedEndpoint());
        m_exception = source.internalGetException();
    }

    @Override
    public ImportReference getImportReference() {
        if (m_closed.get()) {
            return null;
        }
        return m_importReference;
    }

    @Override
    public Throwable getException() {
        if (m_closed.get()) {
            return null;
        }
        return m_exception;
    }

    @Override
    public void close() {
        if (m_closed.compareAndSet(false, true)) {
            m_importReference.internalClose();
            m_adminInstance.getImportManager().registrationClosed(this);
        }
    }

    @Override
    public void update(EndpointDescription endpoint) {
        if (m_closed.get()) {
            // FIXME not covered by spec
            throw new IllegalStateException("Updating closed Import Registration not supported");
        }
        if (m_exception != null) {
            throw new IllegalStateException("Updating invalid Import Registration not allowed");
        }
        if (!endpoint.isSameService(m_importReference.internalGetImportedEndpoint())) {
            throw new IllegalArgumentException(
                "Updating Import Registation with different service instance not allowed");
        }
        m_importReference = new ImportReferenceImpl(m_importReference.internalGetImportedService(), endpoint);

        // FIXME implement proxy update
    }

    ImportReferenceImpl internalGetReference() {
        return m_importReference;
    }

    Throwable internalGetException() {
        return m_exception;

    }

    AdminInstanceImpl internalGetAdmin() {
        return m_adminInstance;
    }

}
