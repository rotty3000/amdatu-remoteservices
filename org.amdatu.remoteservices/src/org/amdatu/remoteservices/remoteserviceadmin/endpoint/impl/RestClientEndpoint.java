/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.HTTP_OK;
import static org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.Util.closeSilently;
import static org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.Util.hash;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.framework.ServiceException;
import org.osgi.service.remoteserviceadmin.ImportRegistration;

/**
 * Implementation of an {@link InvocationHandler} that represents a remoted service for one or more service interfaces.
 */
public final class RestClientEndpoint implements InvocationHandler {
    private static final int FATAL_ERROR_COUNT = 5;

    private final Map<Method, String> m_interfaceMethods;
    private final String m_serviceLocation;
    private final Object m_proxy;

    private ClientEndpointProblemListener m_problemListener;
    private ImportRegistration m_importRegistration;
    private int m_remoteErrors;

    public RestClientEndpoint(String serviceLocation, Class<?>... interfaceClasses) {
        if (interfaceClasses.length == 0) {
            throw new IllegalArgumentException("Need at least one interface to expose!");
        }

        m_serviceLocation = serviceLocation;
        m_interfaceMethods = new HashMap<Method, String>();
        m_remoteErrors = 0;

        for (Class<?> interfaceClass : interfaceClasses) {
            for (Method m : interfaceClass.getMethods()) {
                // Compute a fingerprint of the requested method, as to be able to uniquely identify it in the set
                // of interfaces we're exposing...
                m_interfaceMethods.put(m, hash(m));
            }
        }
        m_proxy = Proxy.newProxyInstance(getClass().getClassLoader(), interfaceClasses, this);
    }

    @SuppressWarnings("unchecked")
    public final <T> T getServiceProxy() {
        return (T) m_proxy;
    }

    @Override
    public final Object invoke(Object serviceProxy, Method method, Object[] args) throws Throwable {
        String hash = m_interfaceMethods.get(method);
        if (hash == null) {
            return method.invoke(m_serviceLocation, args);
        }
        return invokeRemoteMethod(method, hash, args);
    }

    /**
     * @param problemListener the problem listener to set, can be <code>null</code>.
     */
    public void setProblemListener(ClientEndpointProblemListener problemListener) {
        m_problemListener = problemListener;
    }

    /**
     * Used to report back fatal communication problems.
     * 
     * @param importRegistration the import registration for this endpoint to set, may be <code>null</code>.
     */
    public void setImportRegistration(ImportRegistration importRegistration) {
        m_importRegistration = importRegistration;
    }

    /**
     * Handles I/O exceptions by counting the number of times they occurred, and if a certain
     * threshold is exceeded closes the import registration for this endpoint.
     * 
     * @param e the exception to handle.
     */
    private void handleRemoteException(IOException e) {
        if (m_problemListener != null) {
            if (++m_remoteErrors > FATAL_ERROR_COUNT) {
                m_problemListener.handleEndpointError(m_importRegistration, e);
            }
            else {
                m_problemListener.handleEndpointWarning(m_importRegistration, e);
            }
        }
    }

    /**
     * Does the invocation of the remote method adhering to any security managers that might be installed.
     * 
     * @param method the actual method to invoke;
     * @param hash the hash of the method to invoke, as calculated by {@link Util#hash(Method)};
     * @param args the arguments of the method to invoke;
     * @return the result of the method invocation, can be <code>null</code>.
     * @throws Exception in case the invocation failed in some way.
     */
    private Object invokeRemoteMethod(final Method method, final String hash, final Object[] args) throws Throwable {
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            try {
                return AccessController.doPrivileged(new PrivilegedAction<Object>() {
                    @Override
                    public Object run() {
                        try {
                            return invokeRemoteMethodSecure(method, hash, args, method.getReturnType());
                        }
                        catch (Throwable e) {
                            throw new ServiceException("TRANSPORT WRAPPER", e);
                        }
                    }
                });
            }
            catch (ServiceException e) {
                // All exceptions are wrapped in this exception, so we need to rethrow its cause to get the actual exception back...
                throw e.getCause();
            }
        }
        else {
            return invokeRemoteMethodSecure(method, hash, args, method.getReturnType());
        }
    }

    /**
     * Does the actual invocation of the remote method.
     * <p>
     * This method assumes that all security checks (if needed) are processed!
     * </p>
     * 
     * @param method the actual method to invoke;
     * @param hash the hash of the method to invoke, as calculated by {@link Util#hash(Method)};
     * @param args the arguments of the method to invoke;
     * @param returnType the return type of the method to invoke.
     * @return the result of the method invocation, can be <code>null</code>.
     * @throws Exception in case the invocation failed in some way.
     */
    private Object invokeRemoteMethodSecure(Method method, String hash, Object[] args, Class<?> returnType)
        throws Throwable {
        HttpURLConnection connection = null;
        Object result;
        try {
            URL url = new URL(m_serviceLocation + "/" + hash);
            connection = openConnection(url);
            result = postRequest(connection, args, returnType);

            // Reset this error counter upon each successful request...
            m_remoteErrors = 0;
        }
        catch (IOException e) {
            handleRemoteException(e);
            throw new ServiceException("Remote service invocation failed: " + e.getMessage(), ServiceException.REMOTE,
                e);
        }
        finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        if (result instanceof ExceptionWrapper) {
            throw ((ExceptionWrapper) result).getException();
        }

        return result;
    }

    private HttpURLConnection openConnection(URL url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("POST");
        connection.setUseCaches(false);
        connection.setDoOutput(true);
        connection.setChunkedStreamingMode(8192);
        // XXX make this configurable somehow?!
        connection.setConnectTimeout(1500);
        // wait indefinitely for response...?
        connection.setReadTimeout(0);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.connect();

        return connection;
    }

    private Object postRequest(HttpURLConnection connection, Object[] args, Class<?> returnType) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory jsonFactory = new JsonFactory(mapper);

        OutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            outputStream = connection.getOutputStream();

            writeArgumentsAsJSON(jsonFactory.createJsonGenerator(outputStream), args);

            int rc = connection.getResponseCode();
            switch (rc) {
                case HTTP_NO_CONTENT:
                    // Void method called, we're done...
                    return null;

                case HTTP_OK:
                    inputStream = connection.getInputStream();

                    return mapper.readValue(connection.getInputStream(), returnType);

                case HTTP_BAD_REQUEST:
                    inputStream = connection.getErrorStream();

                    return mapper.readValue(inputStream, ExceptionWrapper.class);

                default:
                    // All other response codes are not expected...
                    throw new IOException("Unexpected HTTP response: " + rc + " " + connection.getResponseMessage());
            }
        }
        finally {
            closeSilently(inputStream);
            closeSilently(outputStream);
        }
    }

    private void writeArgumentsAsJSON(JsonGenerator gen, Object[] args) throws IOException {
        gen.writeStartArray();
        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                gen.writeObject(args[i]);
            }
        }
        gen.writeEndArray();
        gen.flush();
        gen.close();
    }
}
