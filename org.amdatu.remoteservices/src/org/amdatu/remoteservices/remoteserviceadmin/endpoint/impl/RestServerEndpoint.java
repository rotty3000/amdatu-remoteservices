/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl;

import static javax.servlet.http.HttpServletResponse.*;
import static org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl.Util.hash;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.remoteserviceadmin.ExportRegistration;

/**
 * Servlet that represents a remoted local service.
 */
public final class RestServerEndpoint extends HttpServlet {
    private static final int FATAL_ERROR_COUNT = 5;

    private static final long serialVersionUID = 5306914873403465207L;

    private static final String APPLICATION_JSON = "application/json";

    private final BundleContext m_bundleContext;
    private final ServiceReference<?> m_serviceReference;
    private final Map<String, Method> m_interfaceMethods;

    private ServerEndpointProblemListener m_problemListener;
    private ExportRegistration m_exportRegistration;
    private int m_localErrors;

    public RestServerEndpoint(BundleContext context, ServiceReference<?> reference, Class<?>... interfaceClasses) {
        m_bundleContext = context;
        m_serviceReference = reference;
        m_interfaceMethods = new HashMap<String, Method>();

        for (Class<?> interfaceClass : interfaceClasses) {
            for (Method m : interfaceClass.getMethods()) {
                // Compute a fingerprint of the requested method, as to be able to uniquely identify it in the set of interfaces we're exposing...
                m_interfaceMethods.put(hash(m), m);
            }
        }
    }

    /**
     * @param exportRegistration the export registration to set, is used to report in which registration an error or warning occurred.
     */
    public void setExportRegistration(ExportRegistration exportRegistration) {
        m_exportRegistration = exportRegistration;
    }

    /**
     * @param problemListener the problem listener to set, can be <code>null</code>.
     */
    public void setProblemListener(ServerEndpointProblemListener problemListener) {
        m_problemListener = problemListener;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Method method = findMethod(req.getPathInfo());
        if (method == null) {
            resp.sendError(SC_NOT_FOUND);
            return;
        }

        int argCount = method.getParameterTypes().length;

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode args = mapper.readValue(req.getInputStream(), ArrayNode.class);
        if (args.size() == argCount) {
            Object[] arguments = new Object[argCount];
            for (int i = 0; i < argCount; i++) {
                Class<?> argumentType = method.getParameterTypes()[i];
                JsonNode argument = args.get(i);

                arguments[i] = mapper.readValue(argument, argumentType);
            }

            Object service = m_bundleContext.getService(m_serviceReference);
            if (service == null) {
                // This is actually quite bad...
                handleLocalException(null);

                resp.sendError(SC_SERVICE_UNAVAILABLE);
                return;
            }

            try {
                invokeMethod(resp, service, method, arguments);
                
                // Reset our error counter upon successful invocations...
                m_localErrors = 0;
            }
            finally {
                m_bundleContext.ungetService(m_serviceReference);
            }
        }
        else {
            resp.sendError(SC_NOT_ACCEPTABLE);
        }
    }

    protected void invokeMethod(HttpServletResponse resp, Object service, Method method, Object... args) throws IOException {
        Object result;

        try {
            result = method.invoke(service, args);
        }
        catch (Exception e) {
            processException(resp, e);
            // Exception handled, we're done...
            return;
        }

        // Make it explicit to the caller what to expect...
        if (Void.TYPE.equals(method.getReturnType())) {
            processVoidResult(resp);
        }
        else {
            processResult(resp, result);
        }
    }

    /**
     * Unwraps a given {@link Throwable} into a more concrete exception.
     * 
     * @param t the throwable to unwrap, cannot be <code>null</code>.
     * @return the unwrapped throwable, never <code>null</code>.
     */
    protected Throwable unwrapException(Exception t) {
        if (t instanceof InvocationTargetException) {
            return ((InvocationTargetException) t).getTargetException();
        }
        return t;
    }

    private Method findMethod(String requestPath) {
        if ((requestPath != null) && requestPath.length() > 1) {
            String methodHash = requestPath.substring(1);
            return m_interfaceMethods.get(methodHash);
        }
        return null;
    }

    /**
     * Handles I/O exceptions by counting the number of times they occurred, and if a certain
     * threshold is exceeded closes the import registration for this endpoint.
     * 
     * @param e the exception to handle.
     */
    private void handleLocalException(IOException e) {
        if (m_problemListener != null) {
            if (++m_localErrors > FATAL_ERROR_COUNT) {
                m_problemListener.handleEndpointError(m_exportRegistration, e);
            }
            else {
                m_problemListener.handleEndpointWarning(m_exportRegistration, e);
            }
        }
    }

    private void processException(HttpServletResponse resp, Exception e) throws IOException {
        ServletOutputStream outputStream = resp.getOutputStream();
        resp.setContentType(APPLICATION_JSON);
        resp.setStatus(SC_BAD_REQUEST);

        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(outputStream, new ExceptionWrapper(unwrapException(e)));

        outputStream.flush();
    }

    private void processResult(HttpServletResponse resp, Object result) throws IOException {
        ServletOutputStream outputStream = resp.getOutputStream();
        resp.setContentType(APPLICATION_JSON);
        resp.setStatus(SC_OK);

        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(outputStream, result);
        outputStream.flush();
    }

    private void processVoidResult(HttpServletResponse resp) throws IOException {
        resp.setStatus(SC_NO_CONTENT);
        resp.setContentType(APPLICATION_JSON);
        resp.setContentLength(0);
    }
}
