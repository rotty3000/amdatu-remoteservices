/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.base;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.remoteservices.common.AbstractEndpointPublishingComponent;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.EndpointEvent;
import org.osgi.service.remoteserviceadmin.EndpointEventListener;
import org.osgi.service.remoteserviceadmin.EndpointListener;

/**
 * Base implementation of a Discovery service that handles end-point registration as well as listener tracking
 * and invocation.
 */
@SuppressWarnings("deprecation")
public abstract class AbstractDiscovery extends AbstractEndpointPublishingComponent implements EndpointEventListener,
    EndpointListener {

    // FIXME modified logic not strictly thread safe... but do we actually need to keep this set?
    private final Set<EndpointDescription> m_publishedEndpoints = Collections
        .newSetFromMap(new ConcurrentHashMap<EndpointDescription, Boolean>());

    public AbstractDiscovery(String name) {
        super("discovery", name);
    }

    @Override
    public void endpointChanged(EndpointEvent event, String matchedFilter) {

        switch (event.getType()) {
            case EndpointEvent.ADDED:
                m_publishedEndpoints.add(event.getEndpoint());
                addPublishedEndpoint(event.getEndpoint(), matchedFilter);
                logInfo("Added local endpoint: %s", event.getEndpoint());
                break;
            case EndpointEvent.REMOVED:
                m_publishedEndpoints.remove(event.getEndpoint());
                removePublishedEndpoint(event.getEndpoint(), matchedFilter);
                logInfo("Removed local endpoint: %s", event.getEndpoint());
                break;
            case EndpointEvent.MODIFIED:
                m_publishedEndpoints.remove(event.getEndpoint());
                m_publishedEndpoints.add(event.getEndpoint());
                modifyPublishedEndpoint(event.getEndpoint(), matchedFilter);
                logInfo("Modified local endpoint: %s", event.getEndpoint());
                break;
            case EndpointEvent.MODIFIED_ENDMATCH:
                m_publishedEndpoints.remove(event.getEndpoint());
                removePublishedEndpoint(event.getEndpoint(), matchedFilter);
                logInfo("Endmatched local endpoint: %s", event.getEndpoint());
                break;
            default:
                throw new IllegalStateException("Recieved event with unknown type " + event.getType());
        }
    }

    @Override
    public void endpointAdded(EndpointDescription endpoint, String matchedFilter) {
        m_publishedEndpoints.add(endpoint);
        addPublishedEndpoint(endpoint, matchedFilter);
        logInfo("Added local endpoint: %s", endpoint);
    }

    @Override
    public void endpointRemoved(EndpointDescription endpoint, String matchedFilter) {
        m_publishedEndpoints.remove(endpoint);
        removePublishedEndpoint(endpoint, matchedFilter);
        logInfo("Removed local endpoint: %s", endpoint);
    }

    /**
     * Register a newly discovered remote service and invoke relevant listeners. Concrete implementations must
     * call this method for every applicable remote registration they discover.
     * 
     * @param endpoint The service Endpoint Description
     */
    protected final void addDiscoveredEndpoint(EndpointDescription endpoint) {

        endpointAdded(endpoint);
        logInfo("Added remote endpoint: %s", endpoint);
    }

    /**
     * Unregister a previously discovered remote service endPoint and invoke relevant listeners. Concrete
     * implementations must call this method for every applicable remote registration that disappears.
     * 
     * @param endpoint The service Endpoint Description
     */
    protected final void removeDiscoveredEndpoint(EndpointDescription endpoint) {

        endpointRemoved(endpoint);
        logInfo("Removed remote endpoint: %s", endpoint);
    }

    /**
     * Modifies a previously discovered remote service endPoint and invoke relevant listeners. Concrete
     * implementations must call this method for every applicable remote registration that disappears.
     * 
     * @param endpoint The service Endpoint Description
     */
    protected final void modifyDiscoveredEndpoint(EndpointDescription endpoint) {

        endpointModified(endpoint);
        logInfo("Removed remote endpoint: %s", endpoint);
    }

    /**
     * Called when an exported service is published. The concrete implementation is responsible for registering
     * the service in its service registry.
     * 
     * @param endpoint The service Endpoint Description
     * @param matchedFilter The matched filter
     */
    protected abstract void addPublishedEndpoint(EndpointDescription endpoint, String matchedFilter);

    /**
     * Called when an exported service is depublished. The concrete implementation is responsible for unregistering
     * the service in its service registry.
     * 
     * @param endpoint The service Endpoint Description
     * @param matchedFilter the matched filter
     */
    protected abstract void removePublishedEndpoint(EndpointDescription endpoint, String matchedFilter);

    /**
     * Called when an exported service is modified. The concrete implementation is responsible for updating
     * the service in its service registry.
     * 
     * @param endpoint The service Endpoint Description
     * @param matchedFilter The matched filter
     */
    protected abstract void modifyPublishedEndpoint(EndpointDescription endpoint, String matchedFilter);
}
