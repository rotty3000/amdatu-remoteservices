/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.base;

import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.EndpointEvent;
import org.osgi.service.remoteserviceadmin.EndpointEventListener;
import org.osgi.service.remoteserviceadmin.EndpointListener;

/**
 * Provides a service adapter for registering {@link EndpointEventListener} implementations with DM without any lifecycle methods (which might cause infinite loops).
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@SuppressWarnings("deprecation")
public class EndpointListenerAdapter implements EndpointEventListener, EndpointListener {
    private final AbstractDiscovery m_delegate;

    /**
     * Creates a new {@link EndpointListenerAdapter} instance.
     */
    public EndpointListenerAdapter(AbstractDiscovery delegate) {
        m_delegate = delegate;
    }

    @Override
    public void endpointChanged(EndpointEvent event, String matchedFilter) {
        m_delegate.endpointChanged(event, matchedFilter);
    }

    @Override
    public void endpointAdded(EndpointDescription endpoint, String matchedFilter) {
        m_delegate.endpointAdded(endpoint, matchedFilter);
    }

    @Override
    public void endpointRemoved(EndpointDescription endpoint, String matchedFilter) {
        m_delegate.endpointRemoved(endpoint, matchedFilter);
    }
}
