/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.slp.impl;

import static org.amdatu.remoteservices.discovery.base.DiscoveryUtil.createEndpointListenerServiceProperties;
import static org.amdatu.remoteservices.discovery.slp.SlpConstants.DISCOVERY_TYPE_SLP;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;
import org.osgi.service.remoteserviceadmin.EndpointEventListener;
import org.osgi.service.remoteserviceadmin.EndpointListener;

@SuppressWarnings("deprecation")
public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties properties = createEndpointListenerServiceProperties(context, DISCOVERY_TYPE_SLP);

        manager.add(createComponent()
            .setImplementation(SlpDiscovery.class)
            .setInterface(new String[] { EndpointEventListener.class.getName(), EndpointListener.class.getName() },
                properties)
            .add(createServiceDependency()
                .setService(EndpointEventListener.class, "(!(discovery=true))")
                .setCallbacks("eventListenerAdded", "eventListenerModified", "eventListenerRemoved")
                .setRequired(false))
            .add(createServiceDependency()
                .setService(EndpointListener.class, "(!(discovery=true))")
                .setCallbacks("listenerAdded", "listenerModified", "listenerRemoved")
                .setRequired(false))
            .add(createServiceDependency()
                .setService(LogService.class)
                .setRequired(false))
            .add(createConfigurationDependency()
                .setPid("org.amdatu.remoteservices.discovery.slp"))
            );
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {

    }
}
