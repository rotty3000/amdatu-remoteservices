/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.slp.impl;

import static org.amdatu.remoteservices.discovery.slp.SlpConstants.ARS_SLP_POLL_INTERVAL;
import static org.amdatu.remoteservices.discovery.slp.SlpUtil.createEndpointDescription;
import static org.amdatu.remoteservices.discovery.slp.SlpUtil.createServiceAgent;
import static org.amdatu.remoteservices.discovery.slp.SlpUtil.createServiceInfo;
import static org.amdatu.remoteservices.discovery.slp.SlpUtil.createUserAgentClient;
import static org.amdatu.remoteservices.discovery.slp.SlpUtil.getInet4Addresses;

import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.amdatu.remoteservices.discovery.base.AbstractDiscovery;
import org.livetribe.slp.Scopes;
import org.livetribe.slp.ServiceInfo;
import org.livetribe.slp.ServiceType;
import org.livetribe.slp.ServiceURL;
import org.livetribe.slp.sa.ServiceAgent;
import org.livetribe.slp.ua.UserAgentClient;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.remoteserviceadmin.EndpointDescription;

/**
 * SLP based discovery implementation for use with OSGi Remote Service Admin.
 * This implementation is based on SLP and uses the Livetribe implementation.
 * 
 */
public class SlpDiscovery extends AbstractDiscovery {

    private final ConcurrentHashMap<EndpointDescription, List<ServiceInfo>> m_publishedEndpoints =
        new ConcurrentHashMap<EndpointDescription, List<ServiceInfo>>();

    private final Map<ServiceInfo, EndpointDescription> m_discoveredEndpoints =
        new HashMap<ServiceInfo, EndpointDescription>();

    private volatile ServiceAgent m_serviceAgent;
    private volatile UserAgentClient m_userAgentClient;
    private volatile ScheduledExecutorService m_executorService;
    private volatile int m_pollInterval = 30;
    private volatile Dictionary<String, Object> m_properties;

    public SlpDiscovery() {
        super("slp");
    }

    @Override
    public void stopComponent() {
        logInfo("Stopping Livetribe/SLP discovery: " + m_properties);
        m_executorService.shutdown();
        m_executorService = null;
        m_serviceAgent.stop();
    }

    public void updated(Dictionary<String, Object> properties) throws ConfigurationException {
        if (properties == null) {
            return;
        }
        logInfo("Received configuration %s", properties);
        m_properties = properties;
        if (m_serviceAgent != null && m_serviceAgent.isRunning()) {
            m_serviceAgent.stop();
        }
        m_serviceAgent = createServiceAgent(this.getClass().getClassLoader(), properties);
        m_serviceAgent.start();

        m_userAgentClient = createUserAgentClient(this.getClass().getClassLoader(), properties);

        m_pollInterval = Integer.parseInt((String) properties.get(ARS_SLP_POLL_INTERVAL));
        if (m_executorService != null) {
            m_executorService.shutdownNow();
        }
        m_executorService = Executors.newSingleThreadScheduledExecutor();
        m_executorService.scheduleWithFixedDelay(new ServicePollRunnable(), 0, m_pollInterval, TimeUnit.SECONDS);
    }

    @Override
    protected void addPublishedEndpoint(EndpointDescription endpoint, String matchedFilter) {

        // FIXME should not be here
        String port = getBundleContext().getProperty("org.osgi.service.http.port");
        if (port == null) {
            port = "8080";
        }

        List<ServiceInfo> serviceInfos = new ArrayList<ServiceInfo>();
        try {
            for (Inet4Address address : getInet4Addresses()) {
                ServiceInfo serviceInfo =
                    createServiceInfo(endpoint, address.getHostAddress(), Integer.parseInt(port));
                serviceInfos.add(serviceInfo);
                m_serviceAgent.register(serviceInfo);
            }
        }
        catch (Exception e) {
            logWarning("Exception during service registration", e);
        }
        m_publishedEndpoints.put(endpoint, serviceInfos);
    }

    @Override
    protected void removePublishedEndpoint(EndpointDescription endpoint, String matchedFilter) {

        List<ServiceInfo> serviceInfos = m_publishedEndpoints.remove(endpoint);
        if (serviceInfos != null) {
            for (ServiceInfo serviceInfo : serviceInfos) {
                m_serviceAgent.deregister(serviceInfo.getServiceURL(), serviceInfo.getLanguage());
            }
        }
    }

    @Override
    protected void modifyPublishedEndpoint(EndpointDescription endpoint, String matchedFilter) {
        // TODO can we be smarter?
        removePublishedEndpoint(endpoint, matchedFilter);
        addPublishedEndpoint(endpoint, matchedFilter);
    }

    class ServicePollRunnable implements Runnable {

        @Override
        public void run() {

            UserAgentClient uac = m_userAgentClient;
            // FIXME literal
            ServiceType serviceType = new ServiceType("service:osgi.remote:http");
            String language = null;
            Scopes scopes = Scopes.NONE;
            String filter = null;

            List<ServiceInfo> availableServices = uac.findServices(serviceType, language, scopes, filter);
            Map<ServiceInfo, EndpointDescription> availableEndpoints = new HashMap<ServiceInfo, EndpointDescription>();
            try {
                for (ServiceInfo serviceInfo : availableServices) {
                    EndpointDescription endpointDescription = createEndpointDescription(serviceInfo);
                    availableEndpoints.put(serviceInfo, endpointDescription);
                }
                synchronized (m_discoveredEndpoints) {
                    removeUnavailableEndpoints(availableEndpoints);
                    updateAvailableEndpoints(availableEndpoints);
                }
            }
            catch (Exception e) {
                logWarning("Failed to update available services", e);
                e.printStackTrace();
            }
        }

        private void removeUnavailableEndpoints(Map<ServiceInfo, EndpointDescription> availableEndpoints) {
            List<ServiceURL> availableUrls = new ArrayList<ServiceURL>();
            for (ServiceInfo serviceInfo : availableEndpoints.keySet()) {
                availableUrls.add(serviceInfo.getServiceURL());
            }
            List<ServiceInfo> removeEndpointUrls = new ArrayList<ServiceInfo>();
            for (ServiceInfo serviceInfo : m_discoveredEndpoints.keySet()) {
                if (!availableUrls.contains(serviceInfo.getServiceURL())) {
                    removeEndpointUrls.add(serviceInfo);
                }
            }
            for (ServiceInfo removeEndpoint : removeEndpointUrls) {
                EndpointDescription endpointDescription = m_discoveredEndpoints.remove(removeEndpoint);
                logInfo("DISCOVERY: Remove old: %s %s", removeEndpoint.toString(), endpointDescription);
                removeDiscoveredEndpoint(endpointDescription);
            }
        }

        private void updateAvailableEndpoints(Map<ServiceInfo, EndpointDescription> availableEndpoints) {
            List<ServiceURL> discoveredUrls = new ArrayList<ServiceURL>();
            for (ServiceInfo serviceInfo : m_discoveredEndpoints.keySet()) {
                discoveredUrls.add(serviceInfo.getServiceURL());
            }
            for (ServiceInfo availableEndpoint : availableEndpoints.keySet()) {
                if (discoveredUrls.contains(availableEndpoint.getServiceURL())) {
                    // FIXME we should check for changes
                    continue;
                }
                EndpointDescription endpointDescription = availableEndpoints.get(availableEndpoint);
                logInfo("DISCOVERY: Add new: %s %s", availableEndpoint.toString(), endpointDescription);

                m_discoveredEndpoints.put(availableEndpoint, endpointDescription);
                addDiscoveredEndpoint(endpointDescription);
            }
        }
    }
}
