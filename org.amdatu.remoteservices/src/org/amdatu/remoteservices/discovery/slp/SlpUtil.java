/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.slp;

import static org.amdatu.remoteservices.discovery.base.DiscoveryUtil.join;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.livetribe.slp.Attributes;
import org.livetribe.slp.Attributes.Value;
import org.livetribe.slp.SLP;
import org.livetribe.slp.Scopes;
import org.livetribe.slp.ServiceInfo;
import org.livetribe.slp.ServiceURL;
import org.livetribe.slp.sa.ServiceAgent;
import org.livetribe.slp.settings.Keys;
import org.livetribe.slp.settings.MapSettings;
import org.livetribe.slp.ua.UserAgentClient;
import org.osgi.framework.Constants;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

/**
 * Collection of SLP/Livetribe utility methods.
 * 
 */
public class SlpUtil {

    private SlpUtil() {
    }

    /**
     * Create a {@link ServiceAgent} using the specified arguments.
     * 
     * @param cl the classLoader
     * @param port the port
     * @return the agent
     */
    public static ServiceAgent createServiceAgent(ClassLoader cl, Dictionary<String, Object> properties) {
        MapSettings settings = getSettings(properties);
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(cl);
        try {
            return SLP.newServiceAgent(settings);
        }
        finally {
            Thread.currentThread().setContextClassLoader(classLoader);
        }
    }

    /**
     * Create a {@link UserAgentClient} using the specified arguments.
     * 
     * @param cl
     * @param properties
     * @return the client
     */
    public static UserAgentClient createUserAgentClient(ClassLoader cl, Dictionary<String, Object> properties) {

        MapSettings settings = getSettings(properties);
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(cl);
        try {
            return SLP.newUserAgentClient(settings);
        }
        finally {
            Thread.currentThread().setContextClassLoader(classLoader);
        }
    }

    /**
     * Construct an SLP serviceinfo object from an endpoint for a specific address and port.
     * 
     * @param endpoint the endpoint
     * @param address an ip-address of hostname
     * @param port a port number
     * @return a serviceinfo object
     */
    public static ServiceInfo createServiceInfo(EndpointDescription endpoint, String address, int port)
        throws Exception {

        String service = endpoint.getId().substring(endpoint.getFrameworkUUID().length());
        String url = "service:osgi.remote:http://" + address + ":" + port + service;
        ServiceURL serviceURL = new ServiceURL(url, 10);
        String language = Locale.ENGLISH.getLanguage();
        Scopes scopes = Scopes.DEFAULT;

        String scopesS = (String) endpoint.getProperties().get(SlpConstants.SLP_SCOPES);
        if (scopesS != null) {
            scopes = Scopes.from(scopesS.split(","));
        }

        Map<String, String> props = new HashMap<String, String>();
        for (String key : endpoint.getProperties().keySet()) {
            if (RemoteConstants.SERVICE_IMPORTED_CONFIGS.equals(key)) {
                String value = join(endpoint.getConfigurationTypes(), ",");
                props.put(key, value);
            }
            else if (RemoteConstants.ENDPOINT_FRAMEWORK_UUID.equals(key)) {
                props.put(key, endpoint.getFrameworkUUID());
            }
            else if (RemoteConstants.ENDPOINT_ID.equals(key)) {
                props.put(key, endpoint.getId());
            }
            else if (RemoteConstants.ENDPOINT_SERVICE_ID.equals(key)) {
                props.put(key, String.valueOf(endpoint.getServiceId()));
            }
            else if (Constants.OBJECTCLASS.equals(key)) {
                props.put(key, join(endpoint.getInterfaces(), ","));
            }
            else if (key.equals(Constants.SERVICE_ID)) {
                props.put(key, String.valueOf(endpoint.getServiceId()));
            }
            else {
                Object obj = endpoint.getProperties().get(key);
                String configurationSetting = Attributes.bytesToOpaque(toByteArray(obj));
                props.put(key, configurationSetting);
            }
        }
        Attributes attributes = Attributes.from(props);
        return new ServiceInfo(serviceURL, language, scopes, attributes);
    }

    public static EndpointDescription createEndpointDescription(ServiceInfo serviceInfo) throws Exception {

        Map<String, Object> descriptionProps = new Hashtable<String, Object>();
        Attributes attributes = serviceInfo.getAttributes();

        for (String key : attributes) {
            Value value = attributes.valueFor(key);
            if (RemoteConstants.SERVICE_IMPORTED_CONFIGS.equals(key)) {
                Object[] values = value.getValues();
                String[] stringArray = Arrays.copyOf(values, values.length, String[].class);
                descriptionProps.put(RemoteConstants.SERVICE_IMPORTED_CONFIGS, stringArray);
            }
            else if (RemoteConstants.ENDPOINT_FRAMEWORK_UUID.equals(key)) {
                String uuid = (String) value.getValue();
                descriptionProps.put(RemoteConstants.ENDPOINT_FRAMEWORK_UUID, uuid);
            }
            else if (RemoteConstants.ENDPOINT_ID.equals(key)) {
                // Ignore the ID tag, use the information from the service url itself.
                // This includes the hostname/address, the ID doesn't
                // String id = (String) value.getValue();

                String prefix = "service:osgi.remote:";
                String url = serviceInfo.getServiceURL().getURL().substring(prefix.length());

                descriptionProps.put(RemoteConstants.ENDPOINT_ID, url);
            }
            else if (RemoteConstants.ENDPOINT_SERVICE_ID.equals(key)) {
                Long serviceId = new Long((Integer) value.getValue());
                if (serviceId != null) {
                    descriptionProps.put(RemoteConstants.ENDPOINT_SERVICE_ID, serviceId);
                }
            }
            else if (Constants.OBJECTCLASS.equals(key)) {
                String[] stringArray =
                    Arrays.copyOf(value.getValues(), value.getValues().length, String[].class);
                descriptionProps.put(Constants.OBJECTCLASS, stringArray);
            }
            else if (key.equals(Constants.SERVICE_ID)) {
                Long serviceId = new Long((Integer) value.getValue());
                if (serviceId != null) {
                    descriptionProps.put(Constants.SERVICE_ID, serviceId);
                }
            }
            else {
                // All other entries can be of any type, so just get the bytes and convert to an Object.
                // This includes the import configuration specific settings and the import property itself
                // as well as any other entry added to the service registration.
                Object configurationValue = attributes.valueFor(key).getValue();
                Object object = toObject((byte[]) configurationValue);
                descriptionProps.put(key, object);
            }
        }

        return new EndpointDescription(descriptionProps);
    }

    public static byte[] toByteArray(Object obj) throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(obj);
        oos.flush();
        oos.close();
        bos.close();
        return bos.toByteArray();
    }

    public static Object toObject(byte[] bytes) throws Exception {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        ObjectInputStream ois = new ObjectInputStream(bis);
        return ois.readObject();
    }

    public static List<Inet4Address> getInet4Addresses() throws SocketException {
        List<Inet4Address> addresses = new ArrayList<Inet4Address>();
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface ni = (NetworkInterface) interfaces.nextElement();

            if (!ni.isLoopback()) {
                Enumeration<InetAddress> e2 = ni.getInetAddresses();
                while (e2.hasMoreElements()) {
                    InetAddress ip = (InetAddress) e2.nextElement();
                    if (ip instanceof Inet4Address) {
                        addresses.add((Inet4Address) ip);
                    }
                }
            }
        }
        return addresses;
    }

    private static MapSettings getSettings(Dictionary<String, ?> properties) {
        MapSettings settings = new MapSettings();
        Object slpPort = properties.get(Keys.PORT_KEY.getKey());
        if (slpPort != null) {
            settings.put(Keys.PORT_KEY, Keys.PORT_KEY.convert(slpPort));
        }
        Object slpScopes = properties.get(Keys.SCOPES_KEY.getKey());
        if (slpScopes != null) {
            settings.put(Keys.SCOPES_KEY, Keys.SCOPES_KEY.convert(slpScopes));
        }
        Object slpAddress = properties.get(Keys.ADDRESSES_KEY.getKey());
        if (slpAddress != null) {
            settings.put(Keys.ADDRESSES_KEY, Keys.ADDRESSES_KEY.convert(slpAddress));
        }
        Object slpMtu = properties.get(Keys.MAX_TRANSMISSION_UNIT_KEY.getKey());
        if (slpMtu != null) {
            settings.put(Keys.MAX_TRANSMISSION_UNIT_KEY, Keys.MAX_TRANSMISSION_UNIT_KEY.convert(slpMtu));
        }
        Object tcp = properties.get(Keys.UA_UNICAST_PREFER_TCP.getKey());
        if (tcp != null) {
            settings.put(Keys.UA_UNICAST_PREFER_TCP, Keys.UA_UNICAST_PREFER_TCP.convert(tcp));
        }
        return settings;
    }
}
