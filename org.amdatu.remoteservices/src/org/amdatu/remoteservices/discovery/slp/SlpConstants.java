/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.slp;

/**
 * SLP Configuration properties used for service registrations.
 * 
 * The OSGi RSA specification details how configuration types can be used to decide what type of Endpoint is created,
 * but there is no default way to tailor the discovery registration.
 * These properties can be used to influence how a service is registered using SLP.
 * The SLPDiscovery implementation takes the settings from the Endpoint properties, so it is possible by the user to add
 * the properties to the service registration, since these are added to the Endpoint properties.
 */
public interface SlpConstants {

    String ARS_SLP_QUERY_SCOPES = "ars.slp.query.scopes";
    String ARS_SLP_POLL_INTERVAL = "ars.slp.poll.interval";

    String SLP_CONFIG_TYPE = ".slp"; // SLP discovery configuration type
    String SLP_SCOPES = SLP_CONFIG_TYPE + ".scopes";
    String SLP_LANGUAGUE = SLP_CONFIG_TYPE + ".language";
    String SLP_LIFETIME = SLP_CONFIG_TYPE + ".lifetime";

    /** Name used for the 'discovery.type' service property. */
    String DISCOVERY_TYPE_SLP = "slp";
}
