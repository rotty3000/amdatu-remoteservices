/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.bonjour.impl;

import static org.amdatu.remoteservices.common.ServiceUtil.getFrameworkUUID;
import static org.amdatu.remoteservices.discovery.base.DiscoveryUtil.createEndpointListenerServiceProperties;
import static org.amdatu.remoteservices.discovery.bonjour.BonjourConstants.DISCOVERY_TYPE_BONJOUR;
import static org.amdatu.remoteservices.discovery.bonjour.BonjourConstants.JMDNS_NAME;
import static org.amdatu.remoteservices.discovery.bonjour.BonjourConstants.SERVICE_TYPE;
import static org.amdatu.remoteservices.discovery.bonjour.BonjourUtil.createEndpointDescription;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;

import org.amdatu.remoteservices.discovery.base.AbstractDiscovery;
import org.amdatu.remoteservices.discovery.base.EndpointListenerAdapter;
import org.amdatu.remoteservices.discovery.bonjour.BonjourUtil;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.EndpointEventListener;
import org.osgi.service.remoteserviceadmin.EndpointListener;

/**
 * OSGi Remote Services Discovery service implementation using jmDNS/Bonjour.
 */
public class BonjourDiscovery extends AbstractDiscovery implements ServiceListener {

    private final ConcurrentHashMap<EndpointDescription, ServiceInfo> m_publishedEndpoints =
        new ConcurrentHashMap<EndpointDescription, ServiceInfo>();

    private final ConcurrentHashMap<ServiceInfo, EndpointDescription> m_discoveredEndpoints =
        new ConcurrentHashMap<ServiceInfo, EndpointDescription>();

    // Injected by Felix DM...
    private volatile DependencyManager m_dm;
    private volatile BundleContext m_context;
    // Local services...
    private volatile Component m_comp;
    private volatile JmDNS m_jmDNS;

    public BonjourDiscovery() {
        super("bonjour");
    }

    /**
     * Component life-cycle for start
     * 
     * @throws Exception If initialization of the jmDNS fails
     */
    @Override
    public void startComponent() throws Exception {
        String host = getBundleContext().getProperty("host");

        InetAddress ip = null;
        if (host != null) {
            ip = InetAddress.getByName(host);
        }

        logDebug("Starting Bonjour Discovery on %s", (ip == null ? "localhost" : ip.getHostAddress()));

        m_jmDNS = JmDNS.create(ip, JMDNS_NAME);
        m_jmDNS.addServiceListener(SERVICE_TYPE, this);

        Properties properties = createEndpointListenerServiceProperties(m_context, DISCOVERY_TYPE_BONJOUR);

        // TODO bramk: can we not solve this in a different way?
        
        // Register a *new* component for our endpoint listener, as we only should register an
        // endpoint listener in case of a *succesful* creation of our JmDNS service. We cannot
        // register *this* as implementation, as this would cause an endless loop...
        m_comp = m_dm.createComponent()
            .setInterface(new String[] { EndpointEventListener.class.getName(), EndpointListener.class.getName() }, properties)
            .setImplementation(new EndpointListenerAdapter(this));
        m_dm.add(m_comp);
    }

    /**
     * Component life-cycle for stop
     */
    @Override
    public void stopComponent() throws Exception {
        logDebug("Stopping Bonjour Discovery");

        if (m_comp != null) {
            DependencyManager dm = m_comp.getDependencyManager();
            dm.remove(m_comp);
        }

        if (m_jmDNS != null) {
            m_jmDNS.removeServiceListener(SERVICE_TYPE, this);
            m_jmDNS.unregisterAllServices();
            try {
                m_jmDNS.close();
            }
            catch (IOException e) {
                logWarning("Ignored exception while closing jmDNS instance", e);
            }
        }

        logDebug("Stopped Bonjour Discovery");
    }

    @Override
    protected void addPublishedEndpoint(EndpointDescription endpoint, String matchedFilter) {
        try {
            ServiceInfo serviceInfo = BonjourUtil.createServiceInfo(endpoint);
            m_jmDNS.registerService(serviceInfo);
            m_publishedEndpoints.put(endpoint, serviceInfo);
        }
        catch (IOException e) {
            logWarning("Failed to register service with jmDNS. Endpoint not added!", e);
        }
    }

    @Override
    protected void removePublishedEndpoint(EndpointDescription endpoint, String matchedFilter) {
        try {
            ServiceInfo serviceInfo = m_publishedEndpoints.remove(endpoint);
            if (serviceInfo != null) {
                m_jmDNS.unregisterService(serviceInfo);
            }
        }
        catch (Exception e) {
            logWarning("Failed to unregister service with jmDNS.", e);
        }
    }

    @Override
    protected void modifyPublishedEndpoint(EndpointDescription endpoint, String matchedFilter) {
        // TODO can we be smarter?
        removePublishedEndpoint(endpoint, matchedFilter);
        addPublishedEndpoint(endpoint, matchedFilter);
    }

    /*
     * ServiceListener API
     */

    @Override
    public void serviceAdded(ServiceEvent serviceEvent) {
        // see serviceResolved
    }

    @Override
    public void serviceRemoved(ServiceEvent serviceEvent) {

        ServiceInfo serviceInfo = serviceEvent.getInfo();
        EndpointDescription endpointDescription = m_discoveredEndpoints.remove(serviceInfo);
        if (endpointDescription != null) {
            removeDiscoveredEndpoint(endpointDescription);
        }
    }

    @Override
    public void serviceResolved(ServiceEvent serviceEvent) {

        ServiceInfo serviceInfo = serviceEvent.getInfo();
        // FIXME can we not filter on this?
        // FIXME literal
        if (!"osgi".equals(serviceInfo.getSubtype())) {
            return;
        }

        // FIXME can we prevent discovering own services?
        EndpointDescription endpointDescription = createEndpointDescription(serviceInfo);
        if (endpointDescription.getFrameworkUUID().equals(getFrameworkUUID(getBundleContext()))) {
            return;
        }

        m_discoveredEndpoints.put(serviceInfo, endpointDescription);
        addDiscoveredEndpoint(endpointDescription);
    }
}
