/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.bonjour;

import static org.amdatu.remoteservices.discovery.base.DiscoveryUtil.join;
import static org.amdatu.remoteservices.discovery.bonjour.BonjourConstants.SERVICE_TYPE;
import static org.osgi.framework.Constants.OBJECTCLASS;
import static org.osgi.framework.Constants.SERVICE_ID;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.ENDPOINT_FRAMEWORK_UUID;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.ENDPOINT_ID;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.ENDPOINT_SERVICE_ID;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.SERVICE_IMPORTED_CONFIGS;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.jmdns.ServiceInfo;

import org.osgi.framework.Constants;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

/**
 * Collection of jmDNS specific utility methods.
 */
public final class BonjourUtil {

    /**
     * Construct an EndpointDescription from a jmDNS ServiceInfo.
     * 
     * @param serviceInfo the ServiceInfo
     * @return an EndpointDescription
     */
    public static EndpointDescription createEndpointDescription(ServiceInfo serviceInfo) {

        Map<String, Object> descriptionProps = new HashMap<String, Object>();

        Enumeration<String> propertyNames = serviceInfo.getPropertyNames();
        while (propertyNames.hasMoreElements()) {
            String key = (String) propertyNames.nextElement();
            String value = serviceInfo.getPropertyString(key);

            if (SERVICE_IMPORTED_CONFIGS.equals(key)) {
                String[] configurations = value.split(",");
                descriptionProps.put(SERVICE_IMPORTED_CONFIGS, configurations);
            }
            else if (ENDPOINT_FRAMEWORK_UUID.equals(key)) {
                descriptionProps.put(ENDPOINT_FRAMEWORK_UUID, value);
            }
            else if (ENDPOINT_ID.equals(key)) {
                String endpointId = value;
                descriptionProps.put(ENDPOINT_ID, endpointId);
            }
            else if (ENDPOINT_SERVICE_ID.equals(key)) {
                Long serviceId = Long.valueOf(value);
                if (serviceId != null) {
                    descriptionProps.put(RemoteConstants.ENDPOINT_SERVICE_ID, serviceId);
                }
            }
            else if (Constants.OBJECTCLASS.equals(key)) {
                String[] objectClasses = value.split(",");
                descriptionProps.put(Constants.OBJECTCLASS, objectClasses);
            }
            else if (key.equals(Constants.SERVICE_ID)) {
                Long serviceId = Long.valueOf(value);
                if (serviceId != null) {
                    descriptionProps.put(Constants.SERVICE_ID, serviceId);
                }
            }
            else {
                descriptionProps.put(key, value);
            }
        }
        return new EndpointDescription(descriptionProps);
    }

    /**
     * Construct a jmDNS ServiceInfo from an EndpointDescription.
     * 
     * @param endpointDescription the EndpointDescription
     * @param port a port number
     * @return a ServiceInfo object
     */
    public static ServiceInfo createServiceInfo(EndpointDescription endpointDescription) {

        Map<String, Object> props = new HashMap<String, Object>();
        for (String key : endpointDescription.getProperties().keySet()) {
            if (RemoteConstants.SERVICE_IMPORTED_CONFIGS.equals(key)) {
                props.put(key, join(endpointDescription.getConfigurationTypes(), ","));
            }
            else if (RemoteConstants.ENDPOINT_FRAMEWORK_UUID.equals(key)) {
                props.put(key, endpointDescription.getFrameworkUUID());
            }
            else if (RemoteConstants.ENDPOINT_ID.equals(key)) {
                props.put(key, endpointDescription.getId());
            }
            else if (RemoteConstants.ENDPOINT_SERVICE_ID.equals(key)) {
                props.put(key, String.valueOf(endpointDescription.getServiceId()));
            }
            else if (OBJECTCLASS.equals(key)) {
                props.put(key, join(endpointDescription.getInterfaces(), ","));
            }
            else if (key.equals(SERVICE_ID)) {
                props.put(key, String.valueOf(endpointDescription.getServiceId()));
            }
            else {
                Object obj = endpointDescription.getProperties().get(key);
                if (obj instanceof Integer) {
                    props.put(key, String.valueOf(obj));
                }
                else if (obj instanceof String) {
                    props.put(key, String.valueOf(obj));
                }
                else if (obj instanceof Integer) {
                    props.put(key, String.valueOf(obj));
                }
                else if (obj instanceof Boolean) {
                    props.put(key, String.valueOf(obj));
                }
                else if (obj instanceof Character) {
                    props.put(key, String.valueOf(obj));
                }
                else if (obj instanceof Short) {
                    props.put(key, String.valueOf(obj));
                }
                else if (obj instanceof Long) {
                    props.put(key, String.valueOf(obj));
                }
                else if (obj instanceof Float) {
                    props.put(key, String.valueOf(obj));
                }
                else if (obj instanceof Double) {
                    props.put(key, String.valueOf(obj));
                }
                else {
                    // System.out.println("Ignore non-primitive types due to java-c mapping.");
                }
            }
        }

        // FIXME literal
        // FIXME what to do with port
        return ServiceInfo.create(SERVICE_TYPE, "OSGi Remote Service Admin Endpoint", "_osgi", 9999,
            0, 0, props);
    }

    private BonjourUtil() {
    }

}
