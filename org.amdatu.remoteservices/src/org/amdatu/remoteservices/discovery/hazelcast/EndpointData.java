/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.hazelcast;

import static org.osgi.service.remoteserviceadmin.RemoteConstants.ENDPOINT_ID;

import java.io.Serializable;
import java.util.Map;

import com.hazelcast.core.Member;

public final class EndpointData implements Serializable {

    private static final long serialVersionUID = -589019249454265936L;

    private final String m_id;
    private final Map<String, Object> m_properties;
    private final Member m_owner;

    public EndpointData(Map<String, Object> properties, Member owner) {
        m_properties = properties;
        m_owner = owner;
        m_id = (String) properties.get(ENDPOINT_ID);
    }

    public String getId() {
        return m_id;
    }

    public Map<String, Object> getProperties() {
        return m_properties;
    }

    public Member getOwner() {
        return m_owner;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((m_id == null) ? 0 : m_id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EndpointData other = (EndpointData) obj;
        if (m_id == null) {
            if (other.m_id != null)
                return false;
        }
        else if (!m_id.equals(other.m_id))
            return false;
        return true;
    }
}
