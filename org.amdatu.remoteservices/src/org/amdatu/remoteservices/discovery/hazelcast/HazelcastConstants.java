/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.hazelcast;

public interface HazelcastConstants {

    // FIXME move to generic
    String ARS_CONFIG_TYPE = ".ars"; // Amdatu Remote Service service type
    String ARS_ALIAS = ARS_CONFIG_TYPE + ".alias"; // ARS alias
    
    /** Name used for the 'discovery.type' service property. */
    String DISCOVERY_TYPE_HAZELCAST = "hazelcast";

    String LEASES_MAP = "leases.map";

    String ENDPOINTS_MAP = "endpoints.map";

    String LEASE_TOPIC = "lease.topic";
}
