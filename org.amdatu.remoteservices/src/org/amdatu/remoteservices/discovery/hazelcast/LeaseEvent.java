/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.hazelcast;

import java.io.Serializable;

public class LeaseEvent implements Serializable {

    private static final long serialVersionUID = -6463082608696433348L;

    private final String m_id;
    private final long m_duration;

    public LeaseEvent(String id, long duration) {
        m_id = id;
        m_duration = duration;
    }

    public String getId() {
        return m_id;
    }

    public long getDuration() {
        return m_duration;
    }
}
