/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.hazelcast.impl;

import static org.amdatu.remoteservices.discovery.hazelcast.HazelcastConstants.ENDPOINTS_MAP;
import static org.amdatu.remoteservices.discovery.hazelcast.HazelcastConstants.LEASES_MAP;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.amdatu.remoteservices.discovery.base.AbstractDiscovery;
import org.amdatu.remoteservices.discovery.hazelcast.EndpointData;
import org.amdatu.remoteservices.discovery.hazelcast.Lease;
import org.amdatu.remoteservices.discovery.hazelcast.LeaseData;
import org.osgi.service.remoteserviceadmin.EndpointDescription;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.TcpIpConfig;
import com.hazelcast.core.EntryEvent;
import com.hazelcast.core.EntryListener;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.Member;
import com.hazelcast.logging.LogEvent;
import com.hazelcast.logging.LogListener;

/**
 * HazelCast based discovery for OSGi RemoteServiceAdmin with lease support.
 * 
 * This implementation uses HazelCast to publish and discover remote services. To handle remote services a distributed map is used.
 * Leases are also stored in a distributed map. They are handled by two threads, one renews leases, the other expires leases.
 * Renewal of a lease is handled via a Topic and messages.
 * 
 */
public class HazelcastDiscovery extends AbstractDiscovery implements EntryListener<String, EndpointData>, LogListener {

    private final Map<EndpointDescription, EndpointData> m_publishedEndpoints =
        new HashMap<EndpointDescription, EndpointData>();

    private final Map<EndpointData, EndpointDescription> m_discoveredEndpoints =
        new HashMap<EndpointData, EndpointDescription>();

    private final Map<String, Lease> m_publishedImportLeases = new HashMap<String, Lease>();
    private final Map<String, Lease> m_publishedExportLeases = new HashMap<String, Lease>();

    private volatile HazelcastInstance m_hazelcastInstance;
    private volatile Member m_localMember;
    private volatile LeaseRenewalWorker m_leaseRenewalWorker;
    private volatile LeaseExpirationWorker m_leaseExpirationWorker;
    private volatile String m_entrylistenerId;

    private volatile IMap<String, EndpointData> m_endpointDataRegistry;
    private volatile IMap<String, LeaseData> m_endpointLeasesRegistry;

    public HazelcastDiscovery() {
        super("hazelcast");
    }

    /**
     * Set up the required hazelcast instance, lease handling threads etc.
     * During startup all current entries in the distributed map are also handled.
     */
    public void startComponent() throws Exception {

        setupHazelcast();
        m_leaseRenewalWorker = new LeaseRenewalWorker(m_hazelcastInstance, m_publishedExportLeases);
        m_leaseExpirationWorker = new LeaseExpirationWorker(m_hazelcastInstance, m_publishedImportLeases);

        // FIXME Is this needed? Don't we get callbacks?
        for (EndpointData endpointData : m_endpointDataRegistry.values()) {
            endpointAdded(endpointData);
        }
    }

    public void stopComponent() {
        m_leaseRenewalWorker.stop();
        m_leaseExpirationWorker.stop();

        // FIXME Is this needed? Can HZ not take care of this?
        synchronized (m_publishedEndpoints) {
            for (EndpointData endpointData : m_publishedEndpoints.values()) {
                m_endpointDataRegistry.remove(endpointData.getId());
                m_publishedImportLeases.remove(endpointData.getId());
                m_endpointLeasesRegistry.remove(endpointData.getId());
            }
        }

        destroyHazelcast();
    }

    /**
     * When an endpoint is added a new EndpointData object is created. This object is stored in the distributed endpoint map.
     * Also a new LeaseData object is created, leasetime is set to the leasetime specified in endpoint, or to the default 60 seconds.
     */
    @Override
    protected void addPublishedEndpoint(EndpointDescription endpoint, String matchedFilter) {

        EndpointData endpointData = new EndpointData(endpoint.getProperties(), m_localMember);
        synchronized (m_publishedEndpoints) {
            m_publishedEndpoints.put(endpoint, endpointData);

            Object leaseTime = endpoint.getProperties().get("HAZELCAST_LEASE_TIME");
            if (leaseTime == null) {
                leaseTime = new Long(60000);
            }
            LeaseData leaseData = new LeaseData(endpointData.getId(), (Long) leaseTime);
            m_endpointLeasesRegistry.put(endpointData.getId(), leaseData);
            Lease lease = new Lease(endpointData.getId(), 10);
            m_publishedExportLeases.put(endpointData.getId(), lease);
            m_endpointDataRegistry.put(endpointData.getId(), endpointData);
        }
    }

    @Override
    protected void removePublishedEndpoint(EndpointDescription endpoint, String matchedFilter) {

        synchronized (m_publishedEndpoints) {
            EndpointData endpointData = m_publishedEndpoints.remove(endpoint);
            if (endpointData != null) {
                m_endpointDataRegistry.remove(endpointData.getId(), endpointData);
                m_publishedExportLeases.remove(endpointData.getId());
                m_endpointLeasesRegistry.remove(endpointData.getId());
            }
        }
    }

    @Override
    protected void modifyPublishedEndpoint(EndpointDescription endpoint, String matchedFilter) {
        // TODO can we be smarter?
        removePublishedEndpoint(endpoint, matchedFilter);
        addPublishedEndpoint(endpoint, matchedFilter);
    }

    /**
     * Callback method used by hazelcast to inform of additions to the EndpointData map.
     * This method is used to trigger the creation for a new endpoint for the discovered remote service.
     * The LeaseData is used to update the imported leases so that the expiration thread can handle it.
     */
    @Override
    public void entryAdded(EntryEvent<String, EndpointData> entryEvent) {

        if (entryEvent.getValue().getOwner().equals(m_localMember)) {
            // FIXME can we safely ignore this?
            return;
        }
        endpointAdded(entryEvent.getValue());
    }

    private void endpointAdded(EndpointData endpointData) {

        EndpointDescription endpointDescription = new EndpointDescription(endpointData.getProperties());
        synchronized (this) {
            m_discoveredEndpoints.put(endpointData, endpointDescription);
            LeaseData leaseData = m_endpointLeasesRegistry.get(endpointData.getId());
            Lease lease = new Lease(endpointData.getId(), leaseData.getDuration());
            m_publishedImportLeases.put(endpointData.getId(), lease);
        }
        addDiscoveredEndpoint(endpointDescription);
    }

    /**
     * Callback method used by hazelcast to inform of removal from the EndpointData map.
     * This method is used to trigger the removal of an endpoint for the removed remote service.
     * The LeaseData is removed from the imported leases.
     * 
     * If the system recovers from a network failure, Endpoints might have been removed by other nodes (eg via LeaseExpiration).
     * In this case they need to be added again.
     */
    @Override
    public void entryRemoved(EntryEvent<String, EndpointData> entryEvent) {

        EndpointData endpointData = null;
        EndpointDescription endpointDescription = null;
        synchronized (this) {
            // FIXME this is less then optimal, but upgrade to 3.1.3 results in no
            // longer receiving the removed value so we match by key as a quick fix.
            for (EndpointData data : m_discoveredEndpoints.keySet()) {
                if (data.getId().equals(entryEvent.getKey())) {
                    endpointData = data;
                    break;
                }
            }
            if (endpointData != null) {
                endpointDescription = m_discoveredEndpoints.remove(endpointData);
                m_publishedImportLeases.remove(endpointData.getId());
                m_endpointLeasesRegistry.remove(endpointData.getId());

            }
        }
        if (endpointData == null) {
            logWarning("Unable to find  endpointdata for removed event: %s ", entryEvent.getKey());
        }
        else {
            if (endpointDescription != null) {
                removeDiscoveredEndpoint(endpointDescription);
            }
        }
    }

    @Override
    public void entryEvicted(EntryEvent<String, EndpointData> endpointData) {
        // FIXME now what?
    }

    @Override
    public void entryUpdated(EntryEvent<String, EndpointData> endpointData) {
        // FIXME now what?
    }

    @Override
    public void log(LogEvent event) {
        if (event.getMember().equals(m_localMember)) {
            if (event.getLogRecord().getLevel() == Level.SEVERE) {
                logError(event.getLogRecord().getMessage());
            }
            else if (event.getLogRecord().getLevel() == Level.WARNING) {
                logWarning(event.getLogRecord().getMessage());
            }
            else if (event.getLogRecord().getLevel() == Level.INFO) {
                logInfo(event.getLogRecord().getMessage());
            }
            else {
                logDebug(event.getLogRecord().getMessage());
            }
        }
    }

    /*
     * Private methods
     */

    private void setupHazelcast() {

        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(getClass().getClassLoader());

        // FIXME removed setting port during upgrade to 3.1.3 due to API change. This
        // does not change much as HZ uses address picker, but we need to review the
        // entire HZ config anyway.
        Config config = new Config();
        config.setClassLoader(this.getClass().getClassLoader());
        config.setProperty("hazelcast.logging.type", "none");

        // The config property is too late to capture initial messages
        System.setProperty("hazelcast.logging.type", "none");

        NetworkConfig network = config.getNetworkConfig();
        JoinConfig join = network.getJoin();
        String tcpipProperty = getBundleContext().getProperty("discovery.tcpip");
        if (tcpipProperty != null) {
            join.getMulticastConfig().setEnabled(false);
            TcpIpConfig tcpip = join.getTcpIpConfig();
            String[] members = tcpipProperty.split(" ");
            for (String member : members) {
                tcpip.addMember(member);
            }
            tcpip.setEnabled(true);
        }

        m_hazelcastInstance = Hazelcast.newHazelcastInstance(config);
        m_localMember = m_hazelcastInstance.getCluster().getLocalMember();
        m_hazelcastInstance.getLoggingService().addLogListener(Level.FINE, this);

        m_endpointLeasesRegistry = m_hazelcastInstance.getMap(LEASES_MAP);
        m_endpointDataRegistry = m_hazelcastInstance.getMap(ENDPOINTS_MAP);
        m_entrylistenerId = m_endpointDataRegistry.addEntryListener(this, true);

        Thread.currentThread().setContextClassLoader(contextClassLoader);
    }

    private void destroyHazelcast() {

        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(getClass().getClassLoader());

        m_endpointDataRegistry.removeEntryListener(m_entrylistenerId);
        m_endpointDataRegistry = null;
        m_endpointLeasesRegistry = null;

        m_localMember = null;
        m_hazelcastInstance.getLifecycleService().shutdown();

        Thread.currentThread().setContextClassLoader(contextClassLoader);
    }
}
