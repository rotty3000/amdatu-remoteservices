/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.hazelcast.impl;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.amdatu.remoteservices.discovery.hazelcast.HazelcastConstants;
import org.amdatu.remoteservices.discovery.hazelcast.Lease;
import org.amdatu.remoteservices.discovery.hazelcast.LeaseData;
import org.amdatu.remoteservices.discovery.hazelcast.LeaseEvent;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ITopic;

public class LeaseRenewalWorker {

    private final ScheduledExecutorService m_executor = Executors.newSingleThreadScheduledExecutor();
    private final HazelcastInstance m_hazelcast;
    private final Map<String, Lease> m_exportLeases;
    private final IMap<String, LeaseData> m_leases;
    private final ITopic<LeaseEvent> m_topic;

    public LeaseRenewalWorker(HazelcastInstance hazelcast, Map<String, Lease> exportLeases) {
        m_hazelcast = hazelcast;
        m_exportLeases = exportLeases;
        m_leases = m_hazelcast.getMap(HazelcastConstants.LEASES_MAP);
        m_topic = m_hazelcast.getTopic(HazelcastConstants.LEASE_TOPIC);
        m_executor.scheduleWithFixedDelay(createRenewalThread(), 0, 5, TimeUnit.SECONDS);
    }

    public void stop() {
        m_executor.shutdown();
    }

    public Runnable createRenewalThread() {
        return new Runnable() {
            @Override
            public void run() {
                // FIXME why is this in a ccl switch
                ClassLoader ccl = Thread.currentThread().getContextClassLoader();
                Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
                try {
                    for (String key : m_exportLeases.keySet()) {
                        Lease lease = m_exportLeases.get(key);
                        LeaseData data = m_leases.get(key);
                        lease.renewLease(data.getDuration());

                        m_topic.publish(new LeaseEvent(key, data.getDuration()));
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                Thread.currentThread().setContextClassLoader(ccl);
            }
        };
    }
}
