/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.discovery.hazelcast.impl;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.amdatu.remoteservices.discovery.hazelcast.HazelcastConstants;
import org.amdatu.remoteservices.discovery.hazelcast.Lease;
import org.amdatu.remoteservices.discovery.hazelcast.LeaseEvent;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ITopic;
import com.hazelcast.core.Message;
import com.hazelcast.core.MessageListener;

public class LeaseExpirationWorker implements MessageListener<LeaseEvent> {

    private final ScheduledExecutorService m_executor = Executors.newSingleThreadScheduledExecutor();
    private final HazelcastInstance m_hazelcast;
    private final Map<String, Lease> m_importLeases;
    private final ITopic<LeaseEvent> m_topic;
    private final IMap<Object, Object> m_endpointdata;

    public LeaseExpirationWorker(HazelcastInstance hazelcast, Map<String, Lease> importLeases) {
        m_hazelcast = hazelcast;
        m_importLeases = importLeases;
        m_topic = m_hazelcast.getTopic(HazelcastConstants.LEASE_TOPIC);
        m_topic.addMessageListener(this);
        m_endpointdata = m_hazelcast.getMap(HazelcastConstants.ENDPOINTS_MAP);
        m_executor.scheduleWithFixedDelay(createExpirationThread(), 0, 1, TimeUnit.SECONDS);
    }

    public void stop() {
        m_executor.shutdown();
    }

    @Override
    public void onMessage(Message<LeaseEvent> message) {
        // FIXME why is this in a ccl switch
        ClassLoader ccl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());

        Lease lease = m_importLeases.get(message.getMessageObject().getId());
        if (lease != null) {
            synchronized (lease) {
                lease.renewLease(message.getMessageObject().getDuration());
            }
        }
        Thread.currentThread().setContextClassLoader(ccl);
    }

    public Runnable createExpirationThread() {
        // FIXME why is this in a ccl switch
        return new Runnable() {
            @Override
            public void run() {
                ClassLoader ccl = Thread.currentThread().getContextClassLoader();
                Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
                for (String key : m_importLeases.keySet()) {
                    Lease lease = m_importLeases.get(key);
                    if (lease.getExpiration() < System.currentTimeMillis()) {
                        m_endpointdata.remove(key);
                    }
                }
                Thread.currentThread().setContextClassLoader(ccl);
            }
        };
    }
}
