/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.common;

import static org.amdatu.remoteservices.common.ServiceUtil.getFrameworkUUID;

import java.lang.reflect.Array;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.log.LogService;

/**
 * Generic base class for service components that provides easy access to the Bundle Context
 * and methods for logging.
 * 
 */
public abstract class AbstractComponent {

    private final String m_identifier;

    private int m_logLevel = LogService.LOG_INFO;
    private int m_conLevel = LogService.LOG_ERROR - 1;

    private volatile BundleContext m_bundleContext;
    private volatile LogService m_logService;

    public AbstractComponent(String type, String name) {
        m_identifier = type + "/" + name + "(" + (System.currentTimeMillis() % 1000) + ")";
    }

    protected final void start() throws Exception {
        m_logLevel = getLevelProperty(Constants.LOGGING_PROP, LogService.LOG_INFO);
        m_conLevel = getLevelProperty(Constants.CONSOLE_PROP, LogService.LOG_ERROR + 1);
        startComponent();
        logDebug("started (frameworkUUID=%s)", getFrameworkUUID(m_bundleContext));
    }

    protected final void stop() throws Exception {
        stopComponent();
        logDebug("stopped (frameworkUUID=%s)", getFrameworkUUID(m_bundleContext));
    }

    protected void startComponent() throws Exception {
    }

    protected void stopComponent() throws Exception {
    }

    /**
     * Returns the BundleContext
     * 
     * @return the BundleContext
     */
    public final BundleContext getBundleContext() {
        return m_bundleContext;
    }

    public final void logDebug(String message, Object... args) {
        log(LogService.LOG_DEBUG, message, null, args);
    }

    public final void logDebug(String message, Throwable cause, Object... args) {
        log(LogService.LOG_DEBUG, message, cause, args);
    }

    public final void logInfo(String message, Object... args) {
        log(LogService.LOG_INFO, message, null, args);
    }

    public final void logInfo(String message, Throwable cause, Object... args) {
        log(LogService.LOG_INFO, message, cause, args);
    }

    public final void logWarning(String message, Object... args) {
        log(LogService.LOG_WARNING, message, null, args);
    }

    public final void logWarning(String message, Throwable cause, Object... args) {
        log(LogService.LOG_WARNING, message, cause, args);
    }

    public final void logError(String message, Object... args) {
        log(LogService.LOG_ERROR, message, null, args);
    }

    public final void logError(String message, Throwable cause, Object... args) {
        log(LogService.LOG_ERROR, message, cause, args);
    }

    private final void log(int level, String message, Throwable cause, Object... args) {
        if (level <= m_logLevel || level <= m_conLevel) {
            if (args.length > 0) {
                message = String.format(message, processArgs(args));
            }
            message = m_identifier + " " + message;

            if (level <= m_logLevel && m_logService != null) {
                m_logService.log(level, message);
            }
            if (level <= m_conLevel) {
                System.out.println("[CONSOLE] " + getLevelName(level) + " " + message);
                if (cause != null) {
                    cause.printStackTrace(System.out);
                }
            }
        }
    }

    private static final Object[] processArgs(Object... args) {
        for (int i = 0; i < args.length; i++) {
            if (args[i] instanceof ServiceReference<?>) {
                args[i] = toString((ServiceReference<?>) args[i]);
            }
            else if (args[i] instanceof ServiceRegistration<?>) {
                args[i] = toString(((ServiceRegistration<?>) args[i]).getReference());
            }
            else if (args[i] instanceof Bundle) {
                args[i] = toString((Bundle) args[i]);
            }
        }
        return args;
    }

    private static String toString(ServiceReference<?> reference) {
        StringBuilder builder = new StringBuilder().append("{");
        for (String propertyKey : reference.getPropertyKeys()) {
            Object propertyValue = reference.getProperty(propertyKey);
            builder.append(propertyKey).append("=");
            if (propertyValue.getClass().isArray()) {
                builder.append("[");
                for (int i = 0; i < Array.getLength(propertyValue); i++) {
                    builder.append(Array.get(propertyValue, i));
                    if (i < Array.getLength(propertyValue) - 1) {
                        builder.append(", ");
                    }
                }
                builder.append("]");
            }
            else {
                builder.append(propertyValue.toString());
            }
            builder.append(", ");
        }
        builder.setLength(builder.length() - 2);
        return builder.toString();
    }

    private static String toString(Bundle bundle) {
        return "bundle(" + bundle.getBundleId() + ") " + bundle.getSymbolicName() + "/" + bundle.getVersion();
    }

    private final int getLevelProperty(String key, int def) {
        int result = def;
        String value = m_bundleContext.getProperty(key);
        if (value != null && !value.equals("")) {
            try {
                result = Integer.parseInt(value);
            }
            catch (Exception e) {
                // ignore
            }
        }
        return result;
    }

    private final String getLevelName(int level) {
        switch (level) {
            case 1:
                return "[ERROR  ]";
            case 2:
                return "[WARNING]";
            case 3:
                return "[INFO   ]";
            case 4:
                return "[DEBUG  ]";
            default:
                return "[?????]";
        }
    }
}
