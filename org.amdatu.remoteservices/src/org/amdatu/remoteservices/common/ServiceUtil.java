/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.common;

import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;

import org.osgi.framework.BundleContext;

/**
 * Generic service utilities.
 */
public final class ServiceUtil {

    /**
     * Return the framework UUID associated with the provided Bundle Context. If
     * no framework UUID is set it will be assigned.
     * 
     * @param bundleContext the context
     * @return the UUID
     */
    public static String getFrameworkUUID(BundleContext bundleContext) {
        String uuid = bundleContext.getProperty("org.osgi.framework.uuid");
        if (uuid != null) {
            return uuid;
        }
        synchronized ("org.osgi.framework.uuid") {
            uuid = bundleContext.getProperty("org.osgi.framework.uuid");
            if (uuid == null) {
                uuid = UUID.randomUUID().toString();
                System.setProperty("org.osgi.framework.uuid", uuid);
            }
            return uuid;
        }
    }

    /**
     * Returns String[] for a String+ service property value. The value must be of
     * type String, String[] or Collection&gt;String&lt;.
     * 
     * @param value an object of a valid type, can be {@code null}
     * @return a String[] containing the String+ entries
     * @throws IllegalArgumentException if the value type is invalid
     */
    public static String[] getStringPlusValue(Object value) {
        if (value == null) {
            return new String[] {};
        }
        if (value instanceof String) {
            return new String[] { (String) value };
        }
        if (value instanceof String[]) {
            return (String[]) value;
        }
        if (value instanceof Collection<?>) {
            Collection<?> col = (Collection<?>) value;
            Iterator<?> iter = col.iterator();
            while (iter.hasNext()) {
                if (!(iter.next() instanceof String)) {
                    throw new IllegalArgumentException("Not a valid String+ property value: " + value);
                }
            }
            return col.toArray(new String[col.size()]);
        }
        throw new IllegalArgumentException("Not a valid String+ property value: " + value);
    }
}
