package org.amdatu.remoteservices.common;

import org.osgi.service.log.LogService;

public class Constants {

    /**
     * Property key for the log level. Default is {@link LogService#LOG_INFO}.
     */
    public final static String LOGGING_PROP = "amdatu.logging.level";

    /**
     * Property key for the console level. Default is {@link LogService#LOG_ERROR} - 1.
     */
    public final static String CONSOLE_PROP = "amdatu.console.level";

    /**
     * Manifest header key
     */
    public final static String MANIFEST_REMOTE_SERVICE_HEADER = "Remote-Service";

}
