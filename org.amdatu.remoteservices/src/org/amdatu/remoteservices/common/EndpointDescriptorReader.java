/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.common;

import java.io.Reader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParserFactory;

import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.RemoteConstants;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Parses an XML representation according to {@code http://www.osgi.org/xmlns/rsa/v1.0.0/rsa.xsd}
 */
// TODO validation
public class EndpointDescriptorReader {

    private final SAXParserFactory m_saxParserFactory = SAXParserFactory.newInstance();

    public List<EndpointDescription> parseDocument(Reader reader) {
        EndpointDescriptorParserHandler handler = new EndpointDescriptorParserHandler("EXTENDER");
        InputSource source = null;
        try {
            source = new InputSource(reader);
            m_saxParserFactory.newSAXParser().parse(source, handler);
            return handler.getEndpointDescriptions();
        }
        catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    static class EndpointDescriptorParserHandler extends DefaultHandler {

        private final List<EndpointDescription> m_endpointDesciptions = new ArrayList<EndpointDescription>();
        private final Map<String, Object> m_endpointProperties = new HashMap<String, Object>();
        private final StringBuilder m_valueBuffer = new StringBuilder();

        private boolean m_inProperty = false;
        private boolean m_inArray = false;
        private boolean m_inList = false;
        private boolean m_inSet = false;
        private boolean m_inXml = false;
        private boolean m_inValue = false;
        private String m_propertyName;
        private String m_propertyValue;
        private ValueTypes m_propertyType;
        private final List<Object> m_propertyValues = new ArrayList<Object>();

        private final String m_frameworkUUID;

        public EndpointDescriptorParserHandler(String frameworkUUID) {
            m_frameworkUUID = frameworkUUID;
        }

        public List<EndpointDescription> getEndpointDescriptions() {
            return m_endpointDesciptions;
        }

        void fail(boolean val) {
            if (val)
                throw new IllegalStateException("Invalid XML");
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (qName.equals("property")) {
                m_inProperty = true;
                m_propertyName = attributes.getValue(uri, "name");
                String valueType = attributes.getValue(uri, "value-type");
                if (valueType == null || valueType.equals("")) {
                    m_propertyType = ValueTypes.STRING_CLASS;
                }
                else {
                    m_propertyType = ValueTypes.get(valueType);
                }
                m_propertyValue = attributes.getValue(uri, "value");
                m_propertyValues.clear();
            }
            else if (qName.equals("array")) {
                m_inArray = true;
            }
            else if (qName.equals("list")) {
                m_inList = true;
            }
            else if (qName.equals("set")) {
                m_inSet = true;
            }
            else if (qName.equals("value")) {
                m_valueBuffer.setLength(0);
                m_inValue = true;
            }
            else if (m_inProperty) {
                m_inXml = true;
                m_valueBuffer.setLength(0);
                m_valueBuffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                m_valueBuffer.append("<" + qName);
                for (int i = 0; i < attributes.getLength(); i++) {
                    m_valueBuffer.append(" ")
                        .append(attributes.getQName(i))
                        .append("=\"")
                        .append(attributes.getValue(i))
                        .append("\"");
                }
                m_valueBuffer.append(">");
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (qName.equals("endpoint-description")) {
                m_endpointProperties.put(RemoteConstants.ENDPOINT_FRAMEWORK_UUID, m_frameworkUUID);
                m_endpointDesciptions.add(new EndpointDescription(m_endpointProperties));
            }
            else if (qName.equals("property")) {
                m_inProperty = false;
                if (m_inXml) {
                    m_endpointProperties.put(m_propertyName, m_valueBuffer.toString());
                }
                else if (m_inArray) {
                    m_endpointProperties.put(m_propertyName, getPropertyValuesArray());
                }
                else if (m_inList) {
                    m_endpointProperties.put(m_propertyName, Arrays.asList(getPropertyValuesArray()));
                }
                else if (m_inSet) {
                    m_endpointProperties.put(m_propertyName,
                        new HashSet<Object>(Arrays.asList(getPropertyValuesArray())));
                }
                else if (m_propertyValue != null) {
                    m_endpointProperties.put(m_propertyName, m_propertyType.parse(m_propertyValue));
                }
                else {
                    throw new IllegalArgumentException("Property " + m_propertyName + " has no value");
                }
                m_inArray = false;
                m_inList = false;
                m_inSet = false;
                m_inXml = false;
            }
            else if (qName.equals("value")) {
                m_propertyValues.add(m_propertyType.parse(m_valueBuffer.toString()));
                m_inValue = false;
                return;
            }
            else if (m_inXml) {
                m_valueBuffer.append("</" + qName + ">");
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (m_inValue || m_inXml) {
                m_valueBuffer.append(ch, start, length);
            }
        }

        private Object getPropertyValuesArray() {
            // using reflection because component type may be primitive
            Object valuesArray = Array.newInstance(m_propertyType.getType(), m_propertyValues.size());
            for (int i = 0; i < m_propertyValues.size(); i++) {
                Array.set(valuesArray, i, m_propertyValues.get(i));
            }
            return valuesArray;
        }
    }
}