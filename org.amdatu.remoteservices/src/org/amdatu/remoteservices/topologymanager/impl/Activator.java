/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.topologymanager.impl;

import static org.osgi.service.remoteserviceadmin.RemoteConstants.SERVICE_EXPORTED_INTERFACES;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;
import org.osgi.service.remoteserviceadmin.EndpointEventListener;
import org.osgi.service.remoteserviceadmin.EndpointListener;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdmin;

/**
 * Activator for the Amdatu Topology Manager service implementation.
 */
@SuppressWarnings("deprecation")
public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        manager.add(
            createComponent()
                .setImplementation(TopologyManagerImpl.class)
                .add(createServiceDependency()
                    .setService(LogService.class)
                    .setRequired(false))
                .add(createServiceDependency()
                    .setService(RemoteServiceAdmin.class)
                    .setCallbacks("remoteServiceAdminAdded", "remoteServiceAdminRemoved")
                    .setRequired(false))
                .add(createServiceDependency()
                    .setService(null, "(" + SERVICE_EXPORTED_INTERFACES + "=*)")
                    .setCallbacks("addExportedInterfaces", "removeExportedInterfaces")
                    .setRequired(false))
                .add(createServiceDependency()
                    .setService(EndpointEventListener.class, "(!(topology=xxx))")
                    .setCallbacks("eventListenerAdded", "eventListenerModified", "eventListenerRemoved")
                    .setRequired(false))
                .add(createServiceDependency()
                    .setService(EndpointListener.class, "(!(topology=xxx))")
                    .setCallbacks("listenerAdded", "listenerModified", "listenerRemoved")
                    .setRequired(false))
            );
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager)
        throws Exception {
    }
}
