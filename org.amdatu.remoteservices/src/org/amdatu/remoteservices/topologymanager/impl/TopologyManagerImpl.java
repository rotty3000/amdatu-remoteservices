/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.topologymanager.impl;

import static org.osgi.service.remoteserviceadmin.RemoteServiceAdminEvent.EXPORT_ERROR;
import static org.osgi.service.remoteserviceadmin.RemoteServiceAdminEvent.IMPORT_ERROR;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.amdatu.remoteservices.common.AbstractEndpointPublishingComponent;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.ExportReference;
import org.osgi.service.remoteserviceadmin.ExportRegistration;
import org.osgi.service.remoteserviceadmin.ImportReference;
import org.osgi.service.remoteserviceadmin.ImportRegistration;
import org.osgi.service.remoteserviceadmin.RemoteConstants;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdmin;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdminEvent;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdminListener;

/**
 * Topology Manager implementation for Amdatu Remote Services..
 */
public final class TopologyManagerImpl extends AbstractEndpointPublishingComponent implements
    RemoteServiceAdminListener {

    private final List<RemoteServiceAdmin> m_remoteServiceAdmins;
    private final Map<ServiceReference<?>, Map<RemoteServiceAdmin, Collection<ExportRegistration>>> m_exportedServices;
    private final Map<String, Map<ImportRegistration, EndpointDescription>> m_importedServices;
    private final Map<String, List<EndpointDescription>> m_importPossibilities;

    // Dependency Manager injected fields
    private volatile AbstractRemoteEndpointListener m_remoteEndpointListener;

    public TopologyManagerImpl() {
        super("topology", "default");

        m_remoteServiceAdmins = new ArrayList<RemoteServiceAdmin>();
        m_exportedServices =
            new LinkedHashMap<ServiceReference<?>, Map<RemoteServiceAdmin, Collection<ExportRegistration>>>();
        m_importPossibilities = new HashMap<String, List<EndpointDescription>>();
        m_importedServices = new HashMap<String, Map<ImportRegistration, EndpointDescription>>();
    }

    @Override
    public void startComponent() throws Exception {
        if (!shouldUseServiceHook(getBundleContext())) {
            m_remoteEndpointListener = new AnyRemoteEndpointListener(this);
        }
        else {
            m_remoteEndpointListener = new InterestBasedRemoteEndpointListener(this);
        }

        getBundleContext().registerService(RemoteServiceAdminListener.class, this, null);
    }

    @Override
    public void stopComponent() throws Exception {
        m_remoteEndpointListener.unregister();
        m_remoteEndpointListener = null;
    }

    @Override
    public void remoteAdminEvent(RemoteServiceAdminEvent event) {
        int type = event.getType();
        if (type == IMPORT_ERROR) {
            ImportReference importRef = event.getImportReference();

            EndpointDescription endpointDesc = importRef.getImportedEndpoint();
            ServiceReference<?> serviceRef = importRef.getImportedService();

            removeImportedInterfaces(serviceRef, endpointDesc);
        }
        else if (type == EXPORT_ERROR) {
            ExportReference exportRef = event.getExportReference();

            ServiceReference<?> serviceRef = exportRef.getExportedService();

            removeExportedService(serviceRef);
        }
    }

    /*
     * Component call-backs
     */

    void remoteServiceAdminAdded(ServiceReference<RemoteServiceAdmin> reference, RemoteServiceAdmin remoteServiceAdmin) {
        logInfo("Adding Remote Service Admin: %s", reference);
        synchronized (m_remoteServiceAdmins) {
            m_remoteServiceAdmins.add(remoteServiceAdmin);
        }
        triggerExport(remoteServiceAdmin);
        triggerImport(remoteServiceAdmin);
    }

    void remoteServiceAdminRemoved(ServiceReference<RemoteServiceAdmin> reference, RemoteServiceAdmin remoteServiceAdmin) {
        logInfo("Removing Remote Service Admin: %s", reference);
        synchronized (m_remoteServiceAdmins) {
            m_remoteServiceAdmins.remove(remoteServiceAdmin);
        }
        synchronized (m_exportedServices) {
            for (Entry<ServiceReference<?>, Map<RemoteServiceAdmin, Collection<ExportRegistration>>> entry : m_exportedServices
                .entrySet()) {
                Collection<ExportRegistration> exportRegistrations = entry.getValue().remove(remoteServiceAdmin);
                if (exportRegistrations != null) {
                    logInfo("Closing Export Registartion for: %s", reference);
                    closeRegistrations(exportRegistrations);
                }
            }
        }
    }

    void addExportedInterfaces(ServiceReference<?> reference, Object service) {
        triggerExport(reference);
        logInfo("Exported service added: %s", reference);
    }

    void removeExportedInterfaces(ServiceReference<?> reference, Object service) {
        removeExportedService(reference);
        logInfo("Exported service removed: %s", reference);
    }

    void removeImportedInterfaces(ServiceReference<?> reference, EndpointDescription endpointDesc) {
        removeImportedService(reference, endpointDesc);
        logInfo("Imported service removed: %s", reference);
    }

    /**
     * Update all exported service to include this RemoteServiceAdmin.
     * 
     * @param remoteServiceAdmin The RemoteServiceAdmin used to export the services.
     */
    private void triggerExport(RemoteServiceAdmin remoteServiceAdmin) {
        synchronized (m_exportedServices) {
            for (Entry<ServiceReference<?>, Map<RemoteServiceAdmin, Collection<ExportRegistration>>> entry : m_exportedServices
                .entrySet()) {
                if (entry.getValue().containsKey(remoteServiceAdmin)) {
                    logInfo("Service already handled by this RemoteServiceAdmin");
                }
                else {
                    logInfo("Service needs to be exported by this RemoteServiceAdmin");
                    triggerExport(entry.getKey());
                }
            }
        }
    }

    /**
     * Update the given RemoteServiceAdmin with all ImportPossibilities
     * 
     * @param remoteServiceAdmin The RemoteServiceAdmin used to import the services.
     */
    private void triggerImport(RemoteServiceAdmin remoteServiceAdmin) {
        synchronized (m_importPossibilities) {
            Set<Entry<String, List<EndpointDescription>>> entries = m_importPossibilities.entrySet();
            for (Entry<String, List<EndpointDescription>> entry : entries) {
                triggerImport(entry.getKey());
            }
        }
    }

    /**
     * Exports all marked interfaces of the given reference, using the {@link RemoteConstants#SERVICE_EXPORTED_INTERFACES} property.
     * 
     * @param reference The reference to export the interfaces from.
     */
    private void triggerExport(final ServiceReference<?> reference) {
        synchronized (m_exportedServices) {
            Map<RemoteServiceAdmin, Collection<ExportRegistration>> exports = m_exportedServices.get(reference);
            if (exports == null) {
                exports = new LinkedHashMap<RemoteServiceAdmin, Collection<ExportRegistration>>();
                m_exportedServices.put(reference, exports);
            }

            synchronized (m_remoteServiceAdmins) {

                for (RemoteServiceAdmin remoteServiceAdmin : m_remoteServiceAdmins) {

                    Collection<ExportRegistration> exportRegistrations =
                        remoteServiceAdmin.exportService(reference, null);
                    exports.put(remoteServiceAdmin, exportRegistrations);
                    callEndpointsAdded(exportRegistrations);
                }
            }
        }
    }

    /**
     * Remove all exports of the given reference, each export is closed and the EndpointListeners are notified.
     * 
     * @param reference The reference for which all exports must be removed.
     */
    private void removeExportedService(ServiceReference<?> reference) {
        synchronized (m_exportedServices) {
            Map<RemoteServiceAdmin, Collection<ExportRegistration>> exports = m_exportedServices.remove(reference);
            if (exports != null) {
                for (Entry<RemoteServiceAdmin, Collection<ExportRegistration>> entry : exports.entrySet()) {
                    closeRegistrations(entry.getValue());
                }
            }
        }
    }

    /**
     * Remove all exports of the given reference, each export is closed and the EndpointListeners are notified.
     * 
     * @param reference The reference for which all exports must be removed.
     */
    private void removeImportedService(ServiceReference<?> reference, EndpointDescription desc) {
        synchronized (m_importPossibilities) {
            List<String> toBeRemovedFilters = new ArrayList<String>();
            for (Entry<String, List<EndpointDescription>> entry : m_importPossibilities.entrySet()) {
                List<EndpointDescription> list = entry.getValue();
                while (list.remove(desc)) {
                    synchronized (m_importedServices) {
                        Map<ImportRegistration, EndpointDescription> regs = m_importedServices.get(entry.getKey());

                        List<ImportRegistration> toBeRemovedImportRegs = new ArrayList<ImportRegistration>();
                        for (Entry<ImportRegistration, EndpointDescription> innerEntry : regs.entrySet()) {
                            if (desc.equals(innerEntry.getValue())) {
                                toBeRemovedImportRegs.add(innerEntry.getKey());
                            }
                        }

                        for (ImportRegistration reg : toBeRemovedImportRegs) {
                            regs.remove(reg);
                            reg.close();
                        }

                        if (regs.isEmpty()) {
                            // Remove mapping completely...
                            m_importedServices.remove(entry.getKey());
                        }
                    }
                }
                if (list.isEmpty()) {
                    toBeRemovedFilters.add(entry.getKey());
                }
            }

            for (String filter : toBeRemovedFilters) {
                // Remove mapping completely...
                m_importPossibilities.remove(filter);
            }
        }
    }

    private void closeRegistrations(Collection<ExportRegistration> exportRegistrations) {
        for (ExportRegistration exportRegistration : exportRegistrations) {
            closeRegistration(exportRegistration);
        }
    }

    private void closeRegistration(ExportRegistration exportRegistration) {
        ExportReference exportReference = exportRegistration.getExportReference();
        EndpointDescription description = null;
        if (exportReference != null) {
            description = exportReference.getExportedEndpoint();
        }
        exportRegistration.close();
        if (exportReference != null) {
            endpointRemoved(description);
        }
    }

    /**
     * Adds the given endpoint and filter to the import possibilities.
     * After adding an import is triggered for the matched filter.
     * 
     * @param endpoint the endpoint to add.
     * @param matchedFilter the filter from which the original match has been done.
     */
    void addImportableService(EndpointDescription endpoint, String matchedFilter) {
        synchronized (m_importPossibilities) {
            List<EndpointDescription> possibilities = m_importPossibilities.get(matchedFilter);
            if (possibilities == null) {
                possibilities = new ArrayList<EndpointDescription>();
                m_importPossibilities.put(matchedFilter, possibilities);
            }

            if (!possibilities.contains(endpoint)) {
                possibilities.add(endpoint);
            }
        }

        synchronized (m_importedServices) {
            Map<ImportRegistration, EndpointDescription> importRegistrations = m_importedServices.get(matchedFilter);
            if (importRegistrations == null) {
                importRegistrations = new HashMap<ImportRegistration, EndpointDescription>();
                m_importedServices.put(matchedFilter, importRegistrations);
            }
        }

        triggerImport(matchedFilter);
    }

    /**
     * Removed the given endpoint and filter from the import possibilities.
     * After removing an import is triggered for the matched filter to remove imported services.
     * 
     * @param endpoint the endpoint to remove.
     * @param matchedFilter the filter from which the original match has been done.
     */
    void removeImportableService(EndpointDescription endpoint, String matchedFilter) {
        synchronized (m_importPossibilities) {
            List<EndpointDescription> possibilities = m_importPossibilities.get(matchedFilter);
            if (possibilities != null) {
                while (possibilities.remove(endpoint))
                    ;
            }
        }

        triggerImport(matchedFilter);
    }

    void removeImportedServices(String filter) {
        Map<ImportRegistration, EndpointDescription> importRegistrations = m_importedServices.remove(filter);
        if (importRegistrations != null) {
            for (ImportRegistration importRegistration : importRegistrations.keySet()) {
                if (importRegistration != null) {
                    importRegistration.close();
                }
            }
        }

    }

    /**
     * Triggers an import of the given filter.
     * During an import existing imports are checked for availability, and removed if needed.
     * New imports are created and added for new {@link EndpointDescription}s.
     * 
     * @param filter The filter of the matched Endpoints.
     */
    private void triggerImport(final String filter) {
        synchronized (m_importedServices) {
            synchronized (m_importPossibilities) {
                Map<ImportRegistration, EndpointDescription> importRegistrations = m_importedServices.get(filter);
                List<EndpointDescription> endpoints = m_importPossibilities.get(filter);

                if (importRegistrations != null && !importRegistrations.isEmpty()) {
                    for (Iterator<ImportRegistration> iterator = importRegistrations.keySet().iterator(); iterator.hasNext();) {
                        ImportRegistration importRegistration = iterator.next();

                        ImportReference importReference = importRegistration.getImportReference();
                        // can be null in case the registration is closed...
                        if (importReference != null) {
                            EndpointDescription importedEndpoint = importReference.getImportedEndpoint();

                            if ((endpoints != null && !endpoints.contains(importedEndpoint)) || endpoints == null) {
                                importRegistration.close();
                                iterator.remove();
                            }
                        }
                    }
                }

                if (endpoints != null) {
                    for (EndpointDescription endpointDescription : endpoints) {
                        if (!importRegistrations.containsValue(endpointDescription)) {
                            synchronized (m_remoteServiceAdmins) {
                                for (RemoteServiceAdmin rsa : m_remoteServiceAdmins) {
                                    ImportRegistration importRegistration = rsa.importService(endpointDescription);
                                    if (importRegistration != null && importRegistration.getException() == null) {
                                        importRegistrations.put(importRegistration, endpointDescription);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void callEndpointsAdded(Collection<ExportRegistration> exportRegistrations) {
        if (exportRegistrations == null) {
            return;
        }
        for (ExportRegistration registration : exportRegistrations) {
            ExportReference exportReference = registration.getExportReference();
            if (exportReference != null) {
                EndpointDescription description = exportReference.getExportedEndpoint();
                if (description != null) {
                    endpointAdded(description);
                }
            }
        }
    }

    private static boolean shouldUseServiceHook(BundleContext bundleContext) {
        String dontUserServiceHook = bundleContext.getProperty("org.amdatu.remoteservices.dontuseservicehook");
        return !"true".equals(dontUserServiceHook);
    }
}
