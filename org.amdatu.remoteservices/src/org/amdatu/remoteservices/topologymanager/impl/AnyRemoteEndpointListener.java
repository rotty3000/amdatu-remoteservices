/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.topologymanager.impl;

import static org.amdatu.remoteservices.common.ServiceUtil.getFrameworkUUID;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.ENDPOINT_FRAMEWORK_UUID;

import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.framework.ServiceRegistration;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.EndpointEvent;
import org.osgi.service.remoteserviceadmin.EndpointEventListener;
import org.osgi.service.remoteserviceadmin.EndpointListener;

/**
 * Listener strategy that triggers an import for any remote endpoint.
 */
@SuppressWarnings("deprecation")
public class AnyRemoteEndpointListener extends AbstractRemoteEndpointListener implements EndpointEventListener,
    EndpointListener {

    private final ServiceRegistration<?> m_registration;

    AnyRemoteEndpointListener(TopologyManagerImpl topologyManager) {
        super(topologyManager);
        Dictionary<String, Object> serviceProperties = new Hashtable<String, Object>();
        serviceProperties.put(EndpointEventListener.ENDPOINT_LISTENER_SCOPE,
            "(!(" + ENDPOINT_FRAMEWORK_UUID + "=" + getFrameworkUUID(getBundleContext()) + "))");
        serviceProperties.put(EndpointListener.ENDPOINT_LISTENER_SCOPE,
            "(!(" + ENDPOINT_FRAMEWORK_UUID + "=" + getFrameworkUUID(getBundleContext()) + "))");
        m_registration =
            getBundleContext().registerService(
                new String[] { EndpointEventListener.class.getName(), EndpointListener.class.getName() }, this,
                serviceProperties);
    }

    @Override
    void unregister() {
        m_registration.unregister();
    }

    @Override
    public void endpointChanged(EndpointEvent event, String matchedFilter) {
        switch (event.getType()) {
            case EndpointEvent.ADDED:
                getTopologyManager().addImportableService(event.getEndpoint(), matchedFilter);
                logInfo("Endpoint added with filter: %s", matchedFilter);
                break;
            case EndpointEvent.REMOVED:
                getTopologyManager().removeImportableService(event.getEndpoint(), matchedFilter);
                logInfo("Endpoint removed with filter: " + matchedFilter);
                break;
            case EndpointEvent.MODIFIED:
                // TODO change to actual modified
                getTopologyManager().removeImportableService(event.getEndpoint(), matchedFilter);
                getTopologyManager().addImportableService(event.getEndpoint(), matchedFilter);
                logInfo("Endpoint modified with filter: " + matchedFilter);
                break;
            case EndpointEvent.MODIFIED_ENDMATCH:
                getTopologyManager().removeImportableService(event.getEndpoint(), matchedFilter);
                logInfo("Endpoint endmatched with filter: " + matchedFilter);
                break;
            default:
                throw new IllegalStateException("Recieved event with unknown type " + event.getType());
        }
    }

    @Override
    public void endpointAdded(EndpointDescription endpoint, String matchedFilter) {
        getTopologyManager().addImportableService(endpoint, matchedFilter);
        logInfo("Endpoint added with filter: %s", matchedFilter);
    }

    @Override
    public void endpointRemoved(EndpointDescription endpoint, String matchedFilter) {
        getTopologyManager().removeImportableService(endpoint, matchedFilter);
        logInfo("Endpoint removed with filter: " + matchedFilter);
    }
}
