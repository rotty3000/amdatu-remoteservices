/*
 * Copyright (c) 2010-2013 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.remoteservices.topologymanager.impl;

import static org.amdatu.remoteservices.common.ServiceUtil.getFrameworkUUID;
import static org.osgi.service.remoteserviceadmin.RemoteConstants.ENDPOINT_FRAMEWORK_UUID;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.osgi.framework.Constants;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.hooks.service.ListenerHook;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.EndpointEvent;
import org.osgi.service.remoteserviceadmin.EndpointEventListener;
import org.osgi.service.remoteserviceadmin.EndpointListener;

/**
 * Listener strategy that triggers an import for remote endpoints that match an interest.
 */
@SuppressWarnings("deprecation")
public class InterestBasedRemoteEndpointListener extends AbstractRemoteEndpointListener implements
    EndpointEventListener, EndpointListener, ListenerHook {

    private static class ImportInterest {
        int m_refs;

        public ImportInterest(String filter) {
            m_refs = 1;
        }

        public int addReference() {
            return ++m_refs;
        }

        public int removeReference() {
            return --m_refs;
        }
    }

    // #TODO Taken (and updated) from CXF implementation, what should be done here?
    private final static Set<String> SYSTEM_PACKAGES;
    static {
        SYSTEM_PACKAGES = new HashSet<String>();
        SYSTEM_PACKAGES.add("org.osgi.service");
        SYSTEM_PACKAGES.add("org.apache.felix");
    }

    private final static String CLASS_NAME_EXPRESSION = ".*\\(" + Constants.OBJECTCLASS + "=([a-zA-Z_0-9.]+)\\).*";
    private final static Pattern CLASS_NAME_PATTERN = Pattern.compile(CLASS_NAME_EXPRESSION);

    private final Map<String, ImportInterest> m_importInterests = new HashMap<String, ImportInterest>();
    private final List<String> m_endpointListenerScope = new ArrayList<String>();

    private volatile ServiceRegistration<?> m_listenerRegistration;
    private volatile ServiceRegistration<?> m_hookRegistration;

    InterestBasedRemoteEndpointListener(TopologyManagerImpl topologyManager) {
        super(topologyManager);

        // FIXME Why do we add xxx?
        Dictionary<String, Object> properties = new Hashtable<String, Object>();
        properties.put("topology", "xxx");
        m_listenerRegistration =
            getBundleContext().registerService(
                new String[] { EndpointEventListener.class.getName(), EndpointListener.class.getName() }, this,
                properties);
        m_hookRegistration =
            getBundleContext().registerService(ListenerHook.class.getName(), this, null);
    }

    @Override
    void unregister() {
        m_hookRegistration.unregister();
        m_listenerRegistration.unregister();
    }

    @Override
    public void endpointChanged(EndpointEvent event, String matchedFilter) {
        switch (event.getType()) {
            case EndpointEvent.ADDED:
                getTopologyManager().addImportableService(event.getEndpoint(), matchedFilter);
                logInfo("Endpoint added with filter: %s", matchedFilter);
                break;
            case EndpointEvent.REMOVED:
                getTopologyManager().removeImportableService(event.getEndpoint(), matchedFilter);
                logInfo("Endpoint removed with filter: " + matchedFilter);
                break;
            case EndpointEvent.MODIFIED:
                // TODO change to actual modified
                getTopologyManager().removeImportableService(event.getEndpoint(), matchedFilter);
                getTopologyManager().addImportableService(event.getEndpoint(), matchedFilter);
                logInfo("Endpoint mofified with filter: " + matchedFilter);
                break;
            case EndpointEvent.MODIFIED_ENDMATCH:
                getTopologyManager().removeImportableService(event.getEndpoint(), matchedFilter);
                logInfo("Endpoint endmatched with filter: " + matchedFilter);
                break;
            default:
                throw new IllegalStateException("Recieved event with unknown type " + event.getType());
        }
    }

    @Override
    public void endpointAdded(EndpointDescription endpoint, String matchedFilter) {
        getTopologyManager().addImportableService(endpoint, matchedFilter);
        logInfo("Endpoint added with filter: %s", matchedFilter);
    }

    @Override
    public void endpointRemoved(EndpointDescription endpoint, String matchedFilter) {
        getTopologyManager().removeImportableService(endpoint, matchedFilter);
        logInfo("Endpoint removed with filter: " + matchedFilter);
    }

    @Override
    public void added(@SuppressWarnings("rawtypes") Collection listeners) {
        for (Object listener : listeners) {
            ListenerInfo info = (ListenerInfo) listener;
            if (listener == this) {
                continue;
            }
            if (info.getFilter() == null) {
                continue;
            }
            String className = getClassNameFromFilter(info.getFilter());
            if (info.getFilter() != null && !isClassExcluded(className)) {
                logInfo("Adding interest: %s", info.getFilter());
                addServiceInterest(info.getFilter());
            }
        }
    }

    @Override
    public void removed(@SuppressWarnings("rawtypes") Collection listeners) {
        for (Object listener : listeners) {
            ListenerInfo info = (ListenerInfo) listener;
            if (listener == this) {
                logInfo("Ignore requests from myself");
                continue;
            }
            // Exclude system packages?
            if (info.getFilter() != null) {
                removeServiceInterest(info.getFilter());
            }
        }
    }

    private void extendEndpointListenerScope(String filter) {
        List<String> endpointListenerScope = new ArrayList<String>();
        synchronized (m_endpointListenerScope) {
            m_endpointListenerScope.add(filter);
            endpointListenerScope.addAll(m_endpointListenerScope);
        }
        updateEndpointListenerRegistration(endpointListenerScope);
    }

    private void reduceEndpointListenerScope(String filter) {
        List<String> endpointListenerScope = new ArrayList<String>();
        synchronized (m_endpointListenerScope) {
            m_endpointListenerScope.remove(filter);
            endpointListenerScope.addAll(m_endpointListenerScope);
        }
        updateEndpointListenerRegistration(endpointListenerScope);
    }

    private void updateEndpointListenerRegistration(List<String> endpointListenerScope) {

        // FIXME Why do we add xxx?
        Dictionary<String, Object> serviceProperties = new Hashtable<String, Object>();
        serviceProperties.put("topology", "xxx");
        serviceProperties.put(EndpointEventListener.ENDPOINT_LISTENER_SCOPE, endpointListenerScope);
        serviceProperties.put(EndpointListener.ENDPOINT_LISTENER_SCOPE, endpointListenerScope);
        m_listenerRegistration.setProperties(serviceProperties);
    }

    private void addServiceInterest(String filter) {
        String extendedFilter = extendFilter(filter, getFrameworkUUID(getBundleContext()));
        synchronized (m_importInterests) {
            ImportInterest interest = m_importInterests.get(extendedFilter);
            if (interest != null) {
                interest.addReference();
            }
            else {
                m_importInterests.put(extendedFilter, new ImportInterest(extendedFilter));
                extendEndpointListenerScope(extendedFilter);
            }
        }
    }

    private void removeServiceInterest(String filter) {
        String extendedFilter = extendFilter(filter, getFrameworkUUID(getBundleContext()));
        synchronized (m_importInterests) {
            ImportInterest interest = m_importInterests.get(extendedFilter);
            if (interest != null) {
                if (interest.removeReference() <= 0) {
                    reduceEndpointListenerScope(extendedFilter);
                    m_importInterests.remove(extendedFilter);
                    getTopologyManager().removeImportedServices(extendedFilter);
                }
            }
        }
    }

    private static String getClassNameFromFilter(String filter) {
        Matcher matcher = CLASS_NAME_PATTERN.matcher(filter);
        if (matcher.matches() && matcher.groupCount() >= 1) {
            return matcher.group(1);
        }
        return null;
    }

    private static boolean isClassExcluded(String className) {
        if (className == null || "".equals(className)) {
            return true;
        }
        for (String p : SYSTEM_PACKAGES) {
            if (className.startsWith(p)) {
                return true;
            }
        }
        return false;
    }

    private static String extendFilter(String filter, String frameworkUUID) {
        return "(&" + filter + "(!(" + ENDPOINT_FRAMEWORK_UUID + "=" + frameworkUUID + ")))";
    }
}
